{
  description = "Carya: linear maps in Agda";
  nixConfig = {
    bash-prompt = "λ ";
  };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      devShells.default =  pkgs.mkShell {
        buildInputs = [
          # Adga
          # Info on Agda/nix: 
          # https://nixos.org/manual/nixpkgs/stable/#how-to-use-agda
          (import ./agda.nix {pkgs = pkgs;} )
          
          # Documentation/writing tools
          pkgs.pandoc
        ];
      }; 
    });
}
