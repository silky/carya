/*
Set the Agda configuration for the project by (e.g.):

* using a newer version of agda-stdlib than is available in nixpkgs

NOTE (BS - 2023-02-24):
I tried to include the agda.withPackages... expression
directly in the shell buildInputs in flake.nix,
but I got errors about concatenating functions not strings 
that I couldn't track down.
For whatever reason, simply putting the expression in a separate file
then importing works. 
*/
{ pkgs }:
with pkgs;

agda.withPackages (p: [
  (p.standard-library.overrideAttrs (oldAttrs: {
    version = "2.0";
    src = fetchFromGitHub {
      repo = "agda-stdlib";
      owner = "agda";
      rev = "7f64664c872560700d8ea732e40fad1e9b7feae4";
      # sha256 = lib.fakeSha256;
      sha256 = "7DWxFgN0jRqLrL9QR2NZqI+4pM2GJNGc7aBJl4OV+pM=";
    };
  }))
])
