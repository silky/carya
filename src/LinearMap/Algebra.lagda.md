---
title: Algebra Constructs on Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles

module LinearMap.Algebra {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

open import Algebra.Structures
open import Algebra.Module.Bundles using (LeftSemimodule)
open import LinearMap.Core 𝓡
open import LinearMap.Base 𝓡
open import LinearMap.Properties 𝓡
open import Data.Product as × using (_,_)
```

</details>

## Algebraic Structures/Bundles with `_+_`

```agda
module _  {a b ℓᵃ ℓᵇ : Level} 
         (𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ) 
         (𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ) where

  open LeftSemimodule 𝓜ᴮ

  +-isMagma : IsMagma (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ}) _+_
  +-isMagma = record 
    { isEquivalence = isEquivalence 
    ; ∙-cong = λ f g → +ᴹ-cong f g
    }

  +-isSemigroup : IsSemigroup (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ}) _+_
  +-isSemigroup = record 
    { isMagma = +-isMagma 
    ; assoc = λ f g h {a} → +ᴹ-assoc (f ·ᵥ a) (g ·ᵥ a) (h ·ᵥ a) 
    }

  0-+-isMonoid : IsMonoid (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ}) _+_ zero
  0-+-isMonoid  = record 
    { isSemigroup = +-isSemigroup
    ; identity = (λ f {a} → +ᴹ-identityˡ (f ·ᵥ a)) 
                , λ f {a} → +ᴹ-identityʳ (f ·ᵥ a) 
    }

  0-+-isCommutativeMonoid : IsCommutativeMonoid (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ}) _+_ zero
  0-+-isCommutativeMonoid  = record 
    { isMonoid = 0-+-isMonoid
    ; comm = λ f g {a} → +ᴹ-comm (f ·ᵥ a) (g ·ᵥ a)
    }

  +-magma : Magma (a ⊔ b ⊔ r ⊔ ℓᵃ ⊔ ℓᵇ ⊔ ℓʳ) (a ⊔ ℓᵇ)
  +-magma = record { isMagma = +-isMagma }

  +-semigroup : Semigroup (a ⊔ b ⊔ r ⊔ ℓᵃ ⊔ ℓᵇ ⊔ ℓʳ) (a ⊔ ℓᵇ) 
  +-semigroup = record { isSemigroup = +-isSemigroup }

  0-+-monoid : Monoid (a ⊔ b ⊔ r ⊔ ℓᵃ ⊔ ℓᵇ ⊔ ℓʳ) (a ⊔ ℓᵇ)
  0-+-monoid = record { isMonoid = 0-+-isMonoid }

  0-+-commutativeMonoid : CommutativeMonoid (a ⊔ b ⊔ r ⊔ ℓᵃ ⊔ ℓᵇ ⊔ ℓʳ) (a ⊔ ℓᵇ)
  0-+-commutativeMonoid = 
    record { isCommutativeMonoid = 0-+-isCommutativeMonoid }
```

## Algebraic Structures/Bundles with `_∘_`

```agda
module _ {a ℓᵃ : Level} (𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ) where

  open LeftSemimodule 𝓜ᴬ

  .∘-isMagma : IsMagma _≈_ (_∘_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ})
  ∘-isMagma = record 
    { isEquivalence = isEquivalence
    ; ∙-cong = λ {x} {y = y} f g → ≈ᴹ-trans f (_⊸_.⊸-cong y g)
    } 

  .∘-isSemigroup : IsSemigroup _≈_ (_∘_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ})
  ∘-isSemigroup = record { isMagma = ∘-isMagma ; assoc = λ _ _ _ → ≈ᴹ-refl }

  .∘-id-isMonoid : IsMonoid _≈_ (_∘_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ}) id
  ∘-id-isMonoid = record 
    { isSemigroup = ∘-isSemigroup ;
      identity = (λ _ → ≈ᴹ-refl) , (λ _ → ≈ᴹ-refl) 
    }

  .∘-magma : Magma (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (a ⊔ ℓᵃ)
  ∘-magma = record { isMagma = ∘-isMagma }

  .∘-semigroup : Semigroup (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (a ⊔ ℓᵃ)
  ∘-semigroup = record { isSemigroup = ∘-isSemigroup }

  .∘-id-monoid : Monoid (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (a ⊔ ℓᵃ)
  ∘-id-monoid = record { isMonoid = ∘-id-isMonoid }
```

## Algebraic Structures/Bundles with `_∘_` and _+_`

```agda
module _ {a ℓᵃ : Level} (𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ) where

  open LeftSemimodule 𝓜ᴬ

  .+-∘-isSemiringWithoutAnnihilatingZero : 
     IsSemiringWithoutAnnihilatingZero _≈_ _+_ (_∘_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ}) zero id
  +-∘-isSemiringWithoutAnnihilatingZero = record
    { +-isCommutativeMonoid = 0-+-isCommutativeMonoid 𝓜ᴬ 𝓜ᴬ
    ; *-cong = λ {x} {y} f g → ≈ᴹ-trans f (_⊸_.⊸-cong y g)
    ; *-assoc = λ _ _ _ → ≈ᴹ-refl
    ; *-identity = (λ _ → ≈ᴹ-refl) , (λ _ → ≈ᴹ-refl)
    ; distrib = (λ x y z → distribˡ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ} {𝓜ᴬ} x {y} {z}) 
               , λ x y z → distribʳ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ} {𝓜ᴬ} {x} {y} {z}
    } 

  .+-∘-isSemiring : IsSemiring _≈_ _+_ (_∘_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ}) zero id
  +-∘-isSemiring = record 
    { isSemiringWithoutAnnihilatingZero = +-∘-isSemiringWithoutAnnihilatingZero 
    ; zero = (λ T {x} → ∘-zeroˡ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ} {T} {x})
           , (λ T {x} → ∘-zeroʳ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ} {T} {x}) 
    }

  .+-∘-SemiringWithoutAnnihilatingZero : SemiringWithoutAnnihilatingZero (r ⊔ ℓʳ ⊔ a ⊔ ℓᵃ) (a ⊔ ℓᵃ)
  +-∘-SemiringWithoutAnnihilatingZero = record 
    { isSemiringWithoutAnnihilatingZero = +-∘-isSemiringWithoutAnnihilatingZero}

  .+-∘-Semiring : Semiring (r ⊔ ℓʳ ⊔ a ⊔ ℓᵃ) (a ⊔ ℓᵃ)
  +-∘-Semiring = record { isSemiring = +-∘-isSemiring }
```
