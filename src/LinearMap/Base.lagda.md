---
title: Operations on Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; lift)
open import Algebra.Bundles using (Semiring)

module LinearMap.Base {r ℓ : Level} (𝓡 : Semiring r ℓ) where

open import Algebra.Structures using (IsCommutativeSemiring)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.DirectProduct 
  renaming (leftSemimodule to _⊕ₗ_) using ()
open import Algebra.Module.Construct.Zero 
  renaming (leftSemimodule to 𝟘) using ()
open import Algebra.Module.Morphism.Structures 
  using (module LeftSemimoduleMorphisms)
open import Data.Product as × using (_,_; ∃)
open import Data.Unit using (tt)
open import Function 
  renaming (_∘_ to _∘ᶠ_ ; id to idᶠ) 
  using (const; Inverse; flip )
open import Relation.Binary.PropositionalEquality 
   using (_≡_; refl; module ≡-Reasoning)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open import LinearMap.Core 𝓡

private
  variable
    a b c d ℓᵃ ℓᵇ ℓᶜ ℓᵈ : Level

open Semiring 𝓡
  renaming 
  ( refl to reflᴿ 
  ; Carrier to R
  ; _≈_ to _≈ᴿ_
  ; _+_ to _+ᴿ_
  ; _*_ to _*ᴿ_
  )
  using 
  (0# ; 1#)
```

</details>


## Special Maps

### Zero

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where

  open LeftSemimodule 𝓜ᴮ 
    using (
      0ᴹ
    ; ≈ᴹ-sym
    ; +ᴹ-identityˡ
    ; ≈ᴹ-refl
    ; *ₗ-zeroʳ)

  zero : 𝓜ᴬ ⊸ 𝓜ᴮ
  zero = mk 
       (λ _ → 0ᴹ)  
       (λ _ → ≈ᴹ-refl) 
       (≈ᴹ-sym (+ᴹ-identityˡ 0ᴹ)) 
       λ {r} → *ₗ-zeroʳ r
```

### Identity

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where

  open LeftSemimodule 𝓜ᴬ using (+ᴹ-cong ; ≈ᴹ-refl)

  id : 𝓜ᴬ ⊸ 𝓜ᴬ
  id = mk idᶠ idᶠ (+ᴹ-cong ≈ᴹ-refl ≈ᴹ-refl) ≈ᴹ-refl 

  ⟦id⟧ : ⟦ id ⟧ ≡ idᶠ
  ⟦id⟧ = refl
```

### Point

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where
  private 
    _⊸!_ = _⊸_ {a = a} {b = r} {ℓᵃ = ℓᵃ} {ℓᵇ = ℓ}
    
  ! : 𝓜ᴬ ⊸! 𝟘
  ! = mk (λ _ → lift tt) (λ _ → lift tt) (lift tt) (lift tt)
```

### Co-Point (TODO: what is name of this?)

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where
  private
    _⊸¡_ = _⊸_ {a = r} {b = a} {ℓᵃ = ℓ}
  open LeftSemimodule 𝓜ᴬ

  ¡ : 𝟘 ⊸¡ 𝓜ᴬ
  ¡ = zero
```

### Diagonal

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {C𝓡 : IsCommutativeSemiring _≈ᴿ_ _+ᴿ_ _*ᴿ_ 0# 1#} where

  open LeftSemimodule 𝓜ᴬ renaming (_*ₗ_ to _*ᴬ_)
  diag : R → 𝓜ᴬ ⊸ 𝓜ᴬ
  diag r = 
      mk 
        (r *ᴬ_) 
        *ₗ-congˡ 
        (λ {a₁} {a₂} → *ₗ-distribˡ r a₁ a₂) 
        λ {r₁} {a} → 
          begin 
            r₁ *ᴬ r *ᴬ a
          ≈˘⟨ *ₗ-assoc r₁ r a ⟩
            (r₁ *ᴿ r) *ᴬ a
          ≈⟨ *ₗ-congʳ (IsCommutativeSemiring.*-comm C𝓡 r₁ r) ⟩
            (r *ᴿ r₁) *ᴬ a 
          ≈⟨ *ₗ-assoc r r₁ a ⟩
            r *ᴬ r₁ *ᴬ a
          ∎
        where open ≈-Reasoning ≈ᴹ-setoid
```

## Addition

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where

  open LeftSemimodule 𝓜ᴬ
    renaming 
      ( Carrierᴹ to A
      ; _+ᴹ_ to _+ᴬ_
      ; _*ₗ_ to _*ᴬ_
      )
    using ()
  open LeftSemimodule 𝓜ᴮ
    renaming 
      ( _+ᴹ_ to _+ᴮ_
      ; _*ₗ_ to _*ᴮ_
      ; +ᴹ-cong to +ᴮ-cong
      ; +ᴹ-congˡ to +ᴮ-congˡ
      ; +ᴹ-congʳ to +ᴮ-congʳ
      ; +ᴹ-comm to +ᴮ-comm
      ; +ᴹ-assoc to +ᴮ-assoc
      ; ≈ᴹ-sym to symᴮ
      ; *ₗ-distribˡ to *ᴮ-distribˡ
      ; ≈ᴹ-setoid to setoidᴮ
      )
  
  _+_ : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ
  (mk f congᶠ +₁ *₁) + (mk g congᵍ +₂ *₂) = 
      mk (λ a → f a +ᴮ g a) 
        (λ x → +ᴮ-cong (congᶠ x) (congᵍ x) )
        (λ {a₁} {a₂} → 
            begin 
              f (a₁ +ᴬ a₂) +ᴮ g (a₁ +ᴬ a₂)
            ≈⟨ +ᴮ-cong +₁ +₂ ⟩ 
              f a₁ +ᴮ f a₂ +ᴮ (g a₁ +ᴮ g a₂)
            ≈⟨ +ᴮ-assoc (f a₁) (f a₂) (g a₁ +ᴮ g a₂) ⟩  
              f a₁ +ᴮ (f a₂ +ᴮ (g a₁ +ᴮ g a₂))
            ≈⟨ +ᴮ-congˡ (symᴮ (+ᴮ-assoc (f a₂) (g a₁) (g a₂))) ⟩ 
              f a₁ +ᴮ ((f a₂ +ᴮ g a₁) +ᴮ g a₂)
            ≈⟨ +ᴮ-congˡ (+ᴮ-congʳ (+ᴮ-comm (f a₂) (g a₁) )) ⟩
              f a₁ +ᴮ ((g a₁ +ᴮ f a₂) +ᴮ g a₂)
            ≈⟨ +ᴮ-congˡ (+ᴮ-assoc (g a₁) (f a₂) (g a₂)) ⟩
              f a₁ +ᴮ (g a₁ +ᴮ (f a₂ +ᴮ g a₂))
            ≈˘⟨ +ᴮ-assoc (f a₁) (g a₁) (f a₂ +ᴮ g a₂) ⟩ 
              f a₁ +ᴮ g a₁ +ᴮ (f a₂ +ᴮ g a₂)
            ∎)
        λ {r} {a} → 
            begin 
              r *ᴮ (f a +ᴮ g a)
            ≈⟨ *ᴮ-distribˡ r (f a) (g a) ⟩
              r *ᴮ f a +ᴮ r *ᴮ g a
            ≈⟨ +ᴮ-congˡ *₂ ⟩
              r *ᴮ f a +ᴮ g (r *ᴬ a)
            ≈⟨ +ᴮ-congʳ *₁ ⟩
              f (r *ᴬ a) +ᴮ g (r *ᴬ a)
            ∎
        where open ≈-Reasoning setoidᴮ
```

## Composition

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} 
         {𝓜ᶜ : LeftSemimodule 𝓡 c ℓᶜ}  where

  open LeftSemimodule 𝓜ᴬ renaming
      (Carrierᴹ to A; _+ᴹ_ to _+ᴬ_; _*ₗ_ to _*ᴬ_)
       using ()
  open LeftSemimodule 𝓜ᴮ renaming 
      (Carrierᴹ to B; _+ᴹ_ to _+ᴮ_; _*ₗ_ to _*ᴮ_ )
      using ()
  open LeftSemimodule 𝓜ᶜ renaming 
      (Carrierᴹ to C; _+ᴹ_ to _+ᶜ_; _*ₗ_ to _*ᶜ_;
       ≈ᴹ-setoid to setoidᶜ)
       using ()

  infixr 9 _∘_

  _∘_ : 𝓜ᴮ ⊸ 𝓜ᶜ → 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᶜ
  (mk f congᶠ +₁ *₁) ∘ (mk g congᵍ +₂ *₂) =
      mk (f ∘ᶠ g)
         (congᶠ ∘ᶠ congᵍ)
         (λ {a₁} {a₂}  → 
            begin
               f (g (a₁ +ᴬ a₂)) 
            ≈⟨ congᶠ +₂ ⟩
              f (g a₁ +ᴮ g a₂) 
            ≈⟨ +₁ ⟩ 
              (f (g a₁) +ᶜ f (g a₂))
            ∎)
          λ {r} {a} → 
            (begin
              (r *ᶜ f (g a))
            ≈⟨ *₁ ⟩
              f (r *ᴮ g a)
            ≈⟨ congᶠ *₂ ⟩
              f (g (r *ᴬ a))
            ∎)
          where open ≈-Reasoning setoidᶜ  

  ⟦_∘_⟧ : ∀ (f : 𝓜ᴮ ⊸ 𝓜ᶜ) (g : 𝓜ᴬ ⊸ 𝓜ᴮ) → ⟦ f ∘ g ⟧ ≡ ⟦ f ⟧ ∘ᶠ ⟦ g ⟧ 
  ⟦ f ∘ g ⟧ = refl

  -- Synonym for composition that reverses the order
  infixr -1 _·_
  _·_ = flip _∘_
```

## Scaling 

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ}
         {C𝓡 : IsCommutativeSemiring _≈ᴿ_ _+ᴿ_ _*ᴿ_ 0# 1#} where

  _*ₗ_ : R → 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ
  r *ₗ T = diag {C𝓡 = C𝓡} r ∘ T
```

## Direct Sum

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} 
         {𝓜ᶜ : LeftSemimodule 𝓡 c ℓᶜ}
         {𝓜ᴰ : LeftSemimodule 𝓡 d ℓᵈ} where

  open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A)
  open LeftSemimodule 𝓜ᴮ renaming (Carrierᴹ to B)
  open LeftSemimodule 𝓜ᶜ renaming (Carrierᴹ to C)
  open LeftSemimodule 𝓜ᴰ renaming (Carrierᴹ to D)
  private
    𝓜A×B = 𝓜ᴬ ⊕ₗ 𝓜ᴮ
    𝓜C×D = 𝓜ᶜ ⊕ₗ 𝓜ᴰ
  open LeftSemimodule 𝓜A×B renaming (Carrierᴹ to A×B)
  open LeftSemimodule 𝓜C×D renaming (Carrierᴹ to C×D)

  _⊕ᶠ_ : (A → C) → (B → D) → A×B → C×D
  f ⊕ᶠ g = ×.map f g

  infixr 6 _⊕_

  _⊕_ : 𝓜ᴬ ⊸ 𝓜ᶜ → 𝓜ᴮ ⊸ 𝓜ᴰ → (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸ (𝓜ᶜ ⊕ₗ 𝓜ᴰ)
  (mk f congᶠ +₁ *₁) ⊕ (mk g congᵍ +₂ *₂) = 
      mk
        (λ (a , b) → f a , g b) 
        (λ (a , b) → congᶠ a , congᵍ b) 
        ( +₁ , +₂) 
        (*₁ , *₂)
        
  ⟦_⊕_⟧ : ∀ (f : 𝓜ᴬ ⊸ 𝓜ᶜ) (g : 𝓜ᴮ ⊸ 𝓜ᴰ) → ⟦ f ⊕ g ⟧ ≡ ⟦ f ⟧ ⊕ᶠ ⟦ g ⟧ 
  ⟦ f ⊕ g ⟧ = refl
```
 
## Copy

```agda
module _  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ}  where
  
  open LeftSemimodule 𝓜ᴬ using (≈ᴹ-refl)
  copy : 𝓜ᴬ ⊸ (𝓜ᴬ ⊕ₗ 𝓜ᴬ)
  copy = 
    mk 
      (λ a → a , a) 
      (λ a → a , a) 
      (≈ᴹ-refl , ≈ᴹ-refl) 
      (≈ᴹ-refl , ≈ᴹ-refl)

  ⟦copy⟧ : ⟦ copy ⟧ ≡ λ x → x , x
  ⟦copy⟧ = refl
``` 

## Reduce ("jam")

```agda
module _  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ}  where

  open LeftSemimodule 𝓜ᴬ

  reduce : (𝓜ᴬ ⊕ₗ 𝓜ᴬ) ⊸ 𝓜ᴬ
  reduce = 
     mk 
       (λ (a₁ , a₂) → a₁ +ᴹ a₂) 
       (λ (a₁ , a₂) → +ᴹ-cong a₁ a₂)
       (λ { {(a₁ , a₁')} {(a₂ , a₂')} → 
        begin 
          a₁ +ᴹ a₂ +ᴹ (a₁' +ᴹ a₂')
        ≈⟨ +ᴹ-assoc a₁ a₂ (a₁' +ᴹ a₂') ⟩
          a₁ +ᴹ (a₂ +ᴹ (a₁' +ᴹ a₂'))
        ≈⟨ +ᴹ-congˡ (+ᴹ-comm a₂ (a₁' +ᴹ a₂')) ⟩
          a₁ +ᴹ ((a₁' +ᴹ a₂') +ᴹ a₂)
        ≈⟨ +ᴹ-congˡ (+ᴹ-assoc a₁' a₂' a₂) ⟩
          a₁ +ᴹ (a₁' +ᴹ (a₂' +ᴹ a₂))
        ≈˘⟨ +ᴹ-assoc a₁  a₁' (a₂' +ᴹ a₂) ⟩
          a₁ +ᴹ a₁' +ᴹ (a₂' +ᴹ a₂)
        ≈⟨ +ᴹ-congˡ (+ᴹ-comm a₂' a₂) ⟩
          a₁ +ᴹ a₁' +ᴹ (a₂ +ᴹ a₂')
        ∎ 
       })
      λ {r} {(a₁ , a₂)} → *ₗ-distribˡ r a₁ a₂
       where open ≈-Reasoning ≈ᴹ-setoid        

  ⟦reduce⟧ : ⟦ reduce ⟧ ≡ ×.uncurry _+ᴹ_
  ⟦reduce⟧ = refl
```

## Fork (split)

Horizontal stacking: 

```md
  A | B 
```

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} 
         {𝓜ᶜ : LeftSemimodule 𝓡 c ℓᶜ} where
  
  infixr 6 _▵_ _∣_
  _▵_ : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᶜ → 𝓜ᴬ ⊸ (𝓜ᴮ ⊕ₗ 𝓜ᶜ)
  f ▵ g = (f ⊕ g) ∘ copy

  _∣_ = _▵_
```

## Join (junc)

Vertical stacking: 

```md
  A 
 --- 
  B 
```

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} 
         {𝓜ᶜ : LeftSemimodule 𝓡 c ℓᶜ} where

  infixr 6 _▿_ _—_
  _▿_ : 𝓜ᴬ ⊸ 𝓜ᶜ → 𝓜ᴮ ⊸ 𝓜ᶜ → (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸ 𝓜ᶜ
  f ▿ g = reduce ∘ (f ⊕ g)

  _—_ = _▿_
```

## Swap 

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where

  open LeftSemimodule 𝓜ᴬ renaming (≈ᴹ-refl to reflᴬ)
  open LeftSemimodule 𝓜ᴮ renaming (≈ᴹ-refl to reflᴮ)

  swap : (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸ (𝓜ᴮ ⊕ₗ 𝓜ᴬ)
  swap =
    mk 
      (λ (a , b) → b , a)
      (λ (a , b) → b , a)
      (reflᴮ , reflᴬ)
      (reflᴮ , reflᴬ)

  ⟦swap⟧ : ⟦ swap ⟧ ≡ ×.swap
  ⟦swap⟧ = refl
```

## Projection/Injection

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where

  open LeftSemimodule 𝓜ᴬ renaming (0ᴹ to 0ᴬ; _+ᴹ_ to _+ᴬ_) 
  open LeftSemimodule 𝓜ᴮ renaming (0ᴹ to 0ᴮ; _+ᴹ_ to _+ᵤ_)
  
  proj₁ : (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸ 𝓜ᴬ
  proj₁ = id ▿ zero

  ⟦proj₁⟧ : ⟦ proj₁ ⟧ ≡ λ (x , _) → x +ᴬ 0ᴬ
  ⟦proj₁⟧ = refl
  
  proj₂ : (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸ 𝓜ᴮ
  proj₂ = zero ▿ id

  ⟦proj₂⟧ : ⟦ proj₂ ⟧ ≡  λ (_ , y) → 0ᴮ +ᵤ y
  ⟦proj₂⟧ = refl

  inj₁ : 𝓜ᴬ ⊸ (𝓜ᴬ ⊕ₗ 𝓜ᴮ)
  inj₁ = id ▵ zero
  
  ⟦inj₁⟧ : ⟦ inj₁ ⟧ ≡ λ x → x , 0ᴮ
  ⟦inj₁⟧ = refl

  inj₂ : 𝓜ᴮ ⊸ (𝓜ᴬ ⊕ₗ 𝓜ᴮ)
  inj₂ = zero ▵ id

  ⟦inj₂⟧ : ⟦ inj₂ ⟧ ≡ λ x → 0ᴬ , x
  ⟦inj₂⟧ = refl
```

## Strong 

* [Profunctor strength](https://hackage.haskell.org/package/profunctors-5.6.2/docs/Data-Profunctor.html#g:2)
* [*Arrows are Strong Monads*](https://www.riec.tohoku.ac.jp/~asada/papers/arrStrMnd.pdf)

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} 
         {𝓜ᶜ : LeftSemimodule 𝓡 c ℓᶜ} where
  
  first : 𝓜ᴬ ⊸ 𝓜ᴮ → (𝓜ᴬ ⊕ₗ 𝓜ᶜ) ⊸ (𝓜ᴮ ⊕ₗ 𝓜ᶜ)
  first T = T ⊕ id

  second : 𝓜ᴬ ⊸ 𝓜ᴮ → (𝓜ᶜ ⊕ₗ 𝓜ᴬ) ⊸ (𝓜ᶜ ⊕ₗ 𝓜ᴮ)
  second T = id ⊕ T
```
