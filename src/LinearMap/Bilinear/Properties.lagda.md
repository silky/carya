---
title: Properties of Bilinear Forms
---

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (_⊔_ ; suc )
open import Algebra.Bundles using (Semiring; CommutativeSemiring)

module LinearMap.Bilinear.Properties {r ℓʳ} (C𝓡 : CommutativeSemiring r ℓʳ) where

open import Algebra.Module.Bundles using (LeftSemimodule)
open import Data.Product as × using (_×_ ; _,_)
open import Function using (_∘_)

private
  𝓡 = CommutativeSemiring.semiring C𝓡

open import LinearMap.Bilinear.Core C𝓡
open import LinearMap.Core 𝓡 hiding (_≈_)
open import LinearMap.Dual C𝓡

open CommutativeSemiring C𝓡
```

A Bilinear form can project out two linear forms (functionals):

* S - a linear map between the Dual
* T - a linear map from M to a Double Dual

See 
[generalization to modules](https://en.wikipedia.org/wiki/Bilinear_form#Generalization_to_modules).

```agda 
module _ {a ℓᵃ} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where
    
    S : Bilinear (𝓜ᴬ *) 𝓜ᴬ → (𝓜ᴬ *) ⊸ (𝓜ᴬ *)
    S B = Bilinear.proj₁ B
   
    T : Bilinear (𝓜ᴬ *) 𝓜ᴬ → 𝓜ᴬ ⊸ (𝓜ᴬ **)
    T B = Bilinear.proj₂ B
```

Some linear maps also induce bilinear forms:

```agda
module _ {a ℓᵃ} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where
    
    open LeftSemimodule 𝓜ᴬ 
      renaming (≈ᴹ-refl to reflᴬ ; ≈ᴹ-sym to symᴬ;  ≈ᴹ-trans to transᴬ)

    Sᴮ : (𝓜ᴬ *) ⊸ (𝓜ᴬ *) → Bilinear (𝓜ᴬ *) 𝓜ᴬ
    Sᴮ T = 
       mk 
         (λ (T₁ , a) → _⊸_.f (g T₁) a)
         (λ {(T₁ , _)}  (x , y) → trans (_⊸_.⊸-cong (g T₁) x) (⊸-cong y)) 
         (λ {a} → _⊸_.⊸-additive (g a) , ⊸-additive )
         λ {r} {T₁} {a} → 
            (sym (_⊸_.⊸-homogenenous (g T₁))) 
          , trans (sym ⊸-homogenenous ) (*-comm r (_⊸_.f (g T₁) a))
      where open _⊸_ T renaming (f to g)

    Tᴮ : 𝓜ᴬ ⊸ 𝓜ᴬ → Bilinear (𝓜ᴬ *) 𝓜ᴬ
    Tᴮ T = 
      mk 
        (λ (T₁ , a) → _⊸_.f T₁ (g a))
        (λ {(T₁ , _)}  (x , y) → trans (_⊸_.⊸-cong T₁ (⊸-cong x)) y)
        (λ {T₁} {T₂} → 
              trans (_⊸_.⊸-cong T₁ ⊸-additive) (_⊸_.⊸-additive T₁) 
            , +-cong (_⊸_.⊸-cong T₁ reflᴬ) (_⊸_.⊸-cong T₂ reflᴬ))
        λ {r} {T₁} {a} → 
              trans 
                (_⊸_.⊸-cong T₁ (symᴬ (⊸-homogenenous))) 
                (sym (_⊸_.⊸-homogenenous T₁ {r}))
            , trans 
                (_⊸_.⊸-homogenenous T₁)
                (trans 
                    (sym ( _⊸_.⊸-homogenenous T₁))
                    (*-comm r (_⊸_.f T₁ (g a))))
      where open _⊸_ T renaming (f to g)
```

## Predicates on Bilinear Forms

```agda
module _ {a ℓᵃ} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ}  where

  open LeftSemimodule 𝓜ᴬ 
    renaming (Carrierᴹ to A; 0ᴹ to 0ᴬ; _≈ᴹ_ to _≈ᴬ_) 
    using ()

  NonDegenerate : Bilinear 𝓜ᴬ 𝓜ᴬ → Set (ℓʳ ⊔ a ⊔ ℓᵃ)
  NonDegenerate B = ∀ (a₁ a₂ : A) → ⟨ a₁ , a₂ ⟩ ≈ 0# → a₂ ≈ᴬ 0ᴬ
    where open Bilinear B

  Symmetric : Bilinear 𝓜ᴬ 𝓜ᴬ → Set (ℓʳ ⊔ a)
  Symmetric B = ∀ (a₁ a₂ : A) → ⟨ a₁ , a₂ ⟩ ≈ ⟨ a₂ , a₁ ⟩
    where open Bilinear B

  Alternating : Bilinear 𝓜ᴬ 𝓜ᴬ → Set (ℓʳ ⊔ a)
  Alternating B = ∀ (a : A) → ⟨ a , a ⟩ ≈ 0#
    where open Bilinear B
    
  Reflexive : Bilinear 𝓜ᴬ 𝓜ᴬ → Set (ℓʳ ⊔ a)
  Reflexive B = ∀ (a₁ a₂ : A) → ⟨ a₁ , a₂ ⟩ ≈ 0# → ⟨ a₂ , a₁ ⟩ ≈ 0#
    where open Bilinear B
  
  Orthogonal : {B : Bilinear 𝓜ᴬ 𝓜ᴬ} → { _ : Reflexive B } → (A → A → Set ℓʳ)
  Orthogonal {B} a₁ a₂ = ⟨ a₁ , a₂ ⟩ ≈ 0#
      where open Bilinear B
```
 