---
title: Bilinear Form
---

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (_⊔_ ; suc )
open import Algebra.Bundles using (CommutativeSemiring)

module LinearMap.Bilinear.Core {r ℓʳ} (C𝓡 : CommutativeSemiring r ℓʳ) where

open import Algebra.Module.Bundles using (LeftSemimodule)
open import Data.Product as × using (_×_ ; _,_)
open import Function using (_∘_)
open import Function.Definitions using (Congruent)

private
  𝓡 = CommutativeSemiring.semiring C𝓡

open import LinearMap.Core 𝓡
open import LinearMap.Dual C𝓡
```

## Bilinear Form

A [*bilinear* map](https://en.wikipedia.org/wiki/Bilinear_form)
is a relation between LeftSemimodules, 
namely a map $M^B \times M^A \to R$, 
which is linear in both arguments.
In particular, see the section on generalization to modules
[generalization to modules](https://en.wikipedia.org/wiki/Bilinear_form#Generalization_to_modules).
Here, bilinear forms are defined over 
[different spaces](https://en.wikipedia.org/wiki/Bilinear_form#Different_spaces).

```agda
record Bilinear 
         {a b ℓᵃ ℓᵇ} 
         (𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ) 
         (𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ)
         : Set (a ⊔ b ⊔ r ⊔ ℓᵃ ⊔ ℓᵇ ⊔ ℓʳ) where
    
    constructor mk 

    open CommutativeSemiring C𝓡
      renaming (Carrier to R; _≈_ to _≈ᴿ_)
      using (_+_ ; _*_ ; sym; trans ; *-comm)
    open LeftSemimodule 𝓜ᴬ
      renaming (
          Carrierᴹ to A
        ; _≈ᴹ_ to _≈ᵃ_
        ; _+ᴹ_ to _+ᴬ_
        ; _*ₗ_ to _*ᴬ_
        ; ≈ᴹ-refl to reflᴬ
        )
    open LeftSemimodule 𝓜ᴮ
      renaming (
          Carrierᴹ to B
        ; _≈ᴹ_ to _≈ᵇ_
        ; _+ᴹ_ to _+ᴮ_
        ; _*ₗ_ to _*ᴮ_
        ; ≈ᴹ-refl to reflᴮ
        )
    
    field
      f : A × B → R
      .isCongruent : Congruent (λ (a₁ , b₁) (a₂ , b₂) → b₁ ≈ᵇ b₂ × a₁ ≈ᵃ a₂) _≈ᴿ_ f
      .isAdditive : ∀ {a₁ a₂ : A} {b₁ b₂ : B} 
                     → f (a₁ , b₁ +ᴮ b₂) ≈ᴿ f (a₁ , b₁) + f (a₁ , b₂)
                     × f (a₁ +ᴬ a₂ , b₁) ≈ᴿ f (a₁ , b₁) + f (a₂ , b₁)      
      .isScalable : ∀ {r : R} {a : A} {b : B}  
                  → f ( a , r *ᴮ b ) ≈ᴿ r * f ( a , b )
                  × f ( r *ᴬ a , b ) ≈ᴿ f ( a , b ) * r

    .cong : Congruent (λ (a₁ , b₁) (a₂ , b₂) → b₁ ≈ᵇ b₂ × a₁ ≈ᵃ a₂) _≈ᴿ_ f
    cong = isCongruent
    
    -- Convenient (and common) syntax for uncurried form of f function.
    ⟨_,_⟩ : A → B → R
    ⟨ x , y ⟩ = f ( x , y )
    
    -- TODO
    -- .bilinear : ∀ {r₁ r₂ : R} {a₁ a₂ : A} {b₁ b₂ : B} 
        -- → f (r₁ *ᴬ (a₁ +ᴬ a₂) , r₂ *ᴮ (b₁ +ᴮ b₂)) 
        -- ≈ᴿ r₁ * f (a₁ , b₁) * r₂ + r₁ * f (a₂ , b₂) * r₂
    -- bilinear {r₁} {r₂} {a₁} {a₂} {b₁} {b₂} = {!  !}

    .additiveˡ : ∀ {a₁ a₂ : A} {b : B} 
       → ⟨ a₁ +ᴬ a₂ , b ⟩ ≈ᴿ ⟨ a₁ , b ⟩ + ⟨ a₂ , b ⟩
    additiveˡ {a₁} {a₂} {b} = ×.proj₂ (isAdditive {a₁} {a₂} {b} {b})

    .additiveʳ : ∀ {a : A} {b₁ b₂ : B} 
       → ⟨ a , b₁ +ᴮ b₂ ⟩ ≈ᴿ ⟨ a , b₁ ⟩ + ⟨ a , b₂ ⟩ 
    additiveʳ {a} {b₁} {b₂} = ×.proj₁ (isAdditive {a} {a} {b₁} {b₂})

    .scaleˡ : ∀ {r : R} {a : A} {b : B} 
       → r * ⟨ a , b ⟩ ≈ᴿ ⟨ r *ᴬ a , b ⟩
    scaleˡ {r} {a} {b} = 
      trans (*-comm r ⟨ a , b ⟩) (sym (×.proj₂ (isScalable {r} {a} {b})))
    
    .scaleʳ :  ∀ {r : R} {a : A} {b : B}
       → ⟨ a , r *ᴮ b ⟩ ≈ᴿ ⟨ a , b ⟩ * r
    scaleʳ {r} {a} {b} = 
      trans (×.proj₁ (isScalable {r} {a} {b})) (*-comm r (f (a , b)))

    -- A Bilinear form can project out two linear maps
    proj₁ : 𝓜ᴬ ⊸ (𝓜ᴮ *)
    proj₁ = 
      mk 
        (λ x → mk 
                ⟨ x ,_⟩ 
                (cong ∘ (_, reflᴬ))
                additiveʳ
                λ {r} {a} → sym (×.proj₁ (isScalable {r} {x} {a}))
        )
        (λ x → cong (reflᴮ , x))
        additiveˡ
        scaleˡ

    proj₂ : 𝓜ᴮ ⊸ (𝓜ᴬ *)
    proj₂ = 
      mk 
        (λ x → mk   
                ⟨_, x ⟩ 
                (cong ∘ ( reflᴮ ,_))
                additiveˡ
                scaleˡ
        )
        (λ x → cong (x , reflᴬ))
        additiveʳ
        λ {r} {a} {x} → sym (×.proj₁ (isScalable {r} {x} {a}))
```
