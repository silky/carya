---
title: Transpose of Linear Maps of a Dual Spaces
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Algebra.Bundles using (CommutativeSemiring; Semiring)

module LinearMap.Transpose {r ℓʳ} (C𝓡 : CommutativeSemiring r ℓʳ) where

open import Level using (Level; _⊔_ )
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Function renaming (_∘_ to _∘ᶠ_) using ()

private
   𝓡 = CommutativeSemiring.semiring C𝓡

open import LinearMap.Core 𝓡
open import LinearMap.Base 𝓡 using (id ; _∘_; _+_)
open import LinearMap.Dual C𝓡

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

private 
   variable 
      a b c ℓᵃ ℓᵇ ℓᶜ : Level

open Semiring 𝓡
   renaming (
      _≈_ to _≈ᴿ_
   ; _+_ to _+ᴿ_
   ; setoid to setoidᴿ
  ) 
   using (
     refl
   ; _*_
   )
```

</details>

# Transpose

The `transpose` of a linear map
takes the domain and codomain to their respective `Dual`
while swapping. 

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where

   open LeftSemimodule 𝓜ᴬ renaming 
      ( _*ₗ_ to _*ᴬ_
      ; _+ᴹ_ to _+ᴬ_)
   open LeftSemimodule 𝓜ᴮ renaming
      ( _*ₗ_ to _*ᴮ_
      ; _+ᴹ_ to _+ᴮ_
      )
      
   transpose : 𝓜ᴬ ⊸ 𝓜ᴮ → (𝓜ᴮ *) ⊸ (𝓜ᴬ *)
   transpose (mk f congᶠ addᶠ scaleᶠ) =  
      mk 
         (λ (mk g congᵍ addᵍ scaleᵍ) 
            → mk 
               ( g ∘ᶠ f )
               (λ x → congᵍ (congᶠ x)) 
               (λ {a₁} {a₂} → 
                  begin 
                    g (f (a₁ +ᴬ a₂))
                  ≈⟨ congᵍ addᶠ ⟩
                    g ((f a₁) +ᴮ (f a₂))
                  ≈⟨ addᵍ ⟩
                    g (f a₁) +ᴿ g (f a₂)
                  ∎
               ) 
               λ {r} {a} → 
                  begin 
                    r * g (f a) 
                  ≈⟨ scaleᵍ ⟩
                    g (r *ᴮ f a)
                  ≈⟨ congᵍ scaleᶠ ⟩ 
                    g (f (r *ᴬ a)) 
                  ∎) 
         (λ x → x) 
         refl
         refl
         where open ≈-Reasoning setoidᴿ
```

## Properties of `transpose`

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ}  where

   private
      _ᵀ = transpose

   ᵀ-identity : (id {𝓜ᴬ = 𝓜ᴬ}) ᵀ ≈ id {𝓜ᴬ = 𝓜ᴬ *}
   ᵀ-identity = refl
```

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ}  
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} 
         {𝓜ᶜ : LeftSemimodule 𝓡 c ℓᶜ} where

   private
      _ᵀ = transpose

   ᵀ-∘-contravariant : ∀ {T₁ : 𝓜ᴮ ⊸ 𝓜ᶜ} {T₂ : 𝓜ᴬ ⊸ 𝓜ᴮ} 
       → ( T₁ ∘ T₂ ) ᵀ ≈ T₂ ᵀ ∘ T₁ ᵀ
   ᵀ-∘-contravariant = refl
```

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where

   private
     _ᵀ = transpose

   .ᵀ-+-covariant : ∀ {T₁ T₂ : 𝓜ᴬ ⊸ 𝓜ᴮ} 
       → ( T₁ + T₂ ) ᵀ ≈ (T₁ ᵀ) + (T₂ ᵀ)
   ᵀ-+-covariant {x = g} = _⊸_.⊸-additive g
```

# Transposable

```agda
module Transposable {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ}
                    {∩ᴮ : 𝓜ᴮ ⊸ (𝓜ᴮ *)} 
                    {∪ᴬ : (𝓜ᴬ *) ⊸ 𝓜ᴬ}
                    where

   infix 50 _ᵀ

   _ᵀ : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴮ ⊸ 𝓜ᴬ
   T ᵀ = ∪ᴬ ∘ transpose T ∘ ∩ᴮ
```

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ}  
         {∪ᴮ : ((𝓜ᴮ *) *) ⊸ 𝓜ᴮ} {∩ᴬ : 𝓜ᴬ ⊸ ((𝓜ᴬ *) *)} where


   -- _ᵀᵀ : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ
   -- T ᵀᵀ = ∪ᴮ ∘ (transpose (transpose T)) ∘ ∩ᴬ

-- TODO: prove transpose involutive
   -- ᵀᵀ-involutive : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → T ≈ T ᵀᵀ
   -- ᵀᵀ-involutive = {!   !}
```
