---
title: Properties of Subsemimodules
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (Semiring)
open import Algebra.Module.Bundles using (LeftSemimodule)

module LinearMap.Submodule.Properties {r ℓʳ} {𝓡 : Semiring r ℓʳ} where

open import Level using (Level; _⊔_; 0ℓ ; lift) renaming (suc to ℓsuc)
open import Algebra.Module.Construct.DirectProduct 
  renaming (leftSemimodule to _⊕ₗ_) using ()
open import Algebra.Module.Construct.TensorUnit 
  renaming (leftSemimodule to 𝓡*) using ()
open import Data.Fin using (Fin ; splitAt) ; open Fin
open import Data.Fin.Properties renaming (≡-setoid to fin)
open import Data.Nat as ℕ using (_+_)
open import Data.Product as × using (Σ ; ∃ ; ∃₂; _,_; _×_)
open import Data.Product.Relation.Binary.Pointwise.NonDependent using (_×ₛ_)
open import Data.Sum using ([_,_]) 
open import Data.Unit using (tt)
open import Fold using (fold; ⊙-preserves-∙; ⊙-preserves-++)
open import Function renaming (_∘_ to _∘ᶠ_ ; Surjection to Surjectionᶠ) using ()
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning
open import Relation.Unary 
  using (Pred; _≐_ ; _∈_; U)
  renaming 
    (_∩_ to _∩′_
    ; _∪_ to _∪′_
    ; _⊆_ to _⊆′_
    ; Satisfiable to Satisfiable′)
open import Relation.Binary.PropositionalEquality using (setoid)

open import LinearMap.Core 𝓡 using (_⊸_; mk) renaming (_≈_ to _⊸≈_)
open import LinearMap.Base 𝓡 using (_∘_; ¡ ; ! ; id)
open import LinearMap.Properties 𝓡 using (Injective ; Surjective)
open import LinearMap.Submodule.Core 𝓡

open SubLeftSemimodule

open Semiring 𝓡 
  renaming 
    ( refl to reflᴿ
    ; Carrier to R
    ; setoid to setoidᴿ
    ; _≈_ to _≈ᴿ_
    ; _*_ to _*ᴿ_
    ; _+_ to _+ᴿ_
    ) 
  using (0# ; 1#)

private 
  variable 
    a b c i ℓᵃ ℓᵇ ℓᶜ ℓ : Level
```

</details>

## Image(s)/Preimage(s)

Given a semimodule homomorphism, 
a subsemimodule on the domain (and codomain) induces
a predicate and correspending subsemimodule
on the codomain (resp, domain).

References: 

* Theorem 3.1 in @Blyth2018

```agda
module _ {ℓ} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ}
         (𝓜ᴬ⊸𝓜ᴮ : 𝓜ᴬ ⊸ 𝓜ᴮ) where

  open LeftSemimodule 𝓜ᴬ 
    renaming 
      ( Carrierᴹ to A
      ; _+ᴹ_ to _+ᴬ_
      ; _*ₗ_ to _*ᴬ_
      ; *ₗ-congˡ to *ᴬ-congˡ
      ; 0ᴹ to 0ᴬ
      )
  open LeftSemimodule 𝓜ᴮ 
    renaming 
      ( Carrierᴹ to B
      ; _+ᴹ_ to _+ᴮ_
      ; _*ₗ_ to _*ᴮ_
      ; _≈ᴹ_ to _≈ᴮ_
      ; ≈ᴹ-sym to ≈ᴮ-sym
      ; ≈ᴹ-trans to ≈ᴮ-trans
      ; +ᴹ-cong to +ᴮ-cong
      ; *ₗ-congˡ to *ᴮ-congˡ
      ; 0ᴹ to 0ᴮ
      )

  -- TODO: rename these
  image : SubLeftSemimodule 𝓜ᴬ ℓ → Pred B (a ⊔ ℓᵇ ⊔ ℓ)
  image 𝓜ᴬ∈P b = ∃ λ Pa → f Pa ≈ᴮ b
      where open _⊸_ (𝓜ᴬ⊸𝓜ᴮ ∘ inclusion⊸ 𝓜ᴬ∈P)

  .imageIsSub : SubLeftSemimodule 𝓜ᴬ ℓ → SubLeftSemimodule 𝓜ᴮ (a ⊔ ℓᵇ ⊔ ℓ)
  imageIsSub 𝓜ᴬ∈P =
      mk 
        (image 𝓜ᴬ∈P)
        (mk 
          (λ { {b₁} {b₂} ((a₁ , Pa₁) , fa₁≈b₁) ((a₂ , Pa₂), fa₂≈b₂) 
                → ((a₁ +ᴬ a₂) , +ᴹ-closed 𝓜ᴬ∈P Pa₁ Pa₂) 
                  , ≈ᴮ-trans ⊸-additive (+ᴮ-cong fa₁≈b₁ fa₂≈b₂)} ) 
          (λ {r} ((a , Pa) , fa≈b) 
                → (r *ᴬ a , *ₗ-closed 𝓜ᴬ∈P Pa) 
                  , ≈ᴮ-trans (≈ᴮ-sym ⊸-homogenenous) (*ᴮ-congˡ fa≈b) )
          ((0ᴬ , 0ᴹ∈P 𝓜ᴬ∈P) , ⊸-unital)
        )
        where open _⊸_ (𝓜ᴬ⊸𝓜ᴮ)

  preimage : SubLeftSemimodule 𝓜ᴮ ℓ → Pred A (b ⊔ ℓᵇ ⊔ ℓ)
  preimage 𝓜ᴮ∈P a = ∃ λ b → f a ≈ᴮ g b
      where open _⊸_ 𝓜ᴬ⊸𝓜ᴮ
            open _⊸_ (inclusion⊸ 𝓜ᴮ∈P) renaming (f to g)
  
  .preimageIsSub : SubLeftSemimodule 𝓜ᴮ ℓ → SubLeftSemimodule 𝓜ᴬ (b ⊔ ℓᵇ ⊔ ℓ)
  preimageIsSub 𝓜ᴮ∈P = 
      mk 
        (preimage 𝓜ᴮ∈P) 
        (mk
          (λ ((b₁ , Pb₁) , fb₁≈a₁) ((b₂ , Pb₂), fb₂≈a₂)  
            → (b₁ +ᴮ b₂ , +ᴹ-closed 𝓜ᴮ∈P Pb₁ Pb₂)
              , ≈ᴮ-trans ⊸-additive (+ᴮ-cong fb₁≈a₁ fb₂≈a₂) ) 
          (λ {r} ((b , Pb), fb≈a) 
              → (r *ᴮ b , *ₗ-closed 𝓜ᴮ∈P Pb) 
                , ≈ᴮ-trans (≈ᴮ-sym ⊸-homogenenous) (*ᴮ-congˡ fb≈a)) 
          ((0ᴮ , 0ᴹ∈P 𝓜ᴮ∈P) , ⊸-unital)
        )
      where open _⊸_ 𝓜ᴬ⊸𝓜ᴮ
```

## Examples 

### `Image`

The `Image` of a linear map is a subsemimodule.
The `Image` is the set of all $b : B$ 
such that there exists an $a : A$
for which a linear map $T$ maps $a$ to $b$.

```agda 
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ}
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} 
         (𝓜ᴬ⊸𝓜ᴮ : 𝓜ᴬ ⊸ 𝓜ᴮ) where

  open LeftSemimodule 𝓜ᴮ renaming (Carrierᴹ to B)
  
  Image : Pred B _ 
  Image = image 𝓜ᴬ⊸𝓜ᴮ (trivial 𝓜ᴬ)

  .ImageIsSubsemimodule : SubLeftSemimodule 𝓜ᴮ _ 
  ImageIsSubsemimodule = imageIsSub 𝓜ᴬ⊸𝓜ᴮ (trivial 𝓜ᴬ)
```

### `Kernel`

The `Kernel` of a linear map is a subsemimodule.
The `Kernel` is set of all $a : A$ that a linear map $T$
maps to the $0$ element (in $B$).
That is, the set of solutions to an equation of linear maps.

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ}
         (𝓜ᴬ⊸𝓜ᴮ : 𝓜ᴬ ⊸ 𝓜ᴮ) where
  open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A)
  open LeftSemimodule 𝓜ᴮ 
    renaming 
      ( Carrierᴹ to B
      ; _≈ᴹ_ to _≈ᴮ_
      ; 0ᴹ to 0ᴮ
      ; ≈ᴹ-trans to ≈ᴮ-trans
      ; +ᴹ-identityˡ to +ᴮ-identityˡ
      ; +ᴹ-cong to +ᴮ-cong
      ; *ₗ-zeroʳ to *ᴮ-zeroʳ
      ; *ₗ-congˡ to *ᴮ-congˡ
      ; ≈ᴹ-refl to ≈ᴮ-refl
      )

  -- The submodule of elements equivalent to then singleton 0ᴮ
  0* : SubLeftSemimodule 𝓜ᴮ _
  0* = mk 
        (_≈ᴮ 0ᴮ) 
        (mk 
          (λ a₁≈0 a₂≈0 → ≈ᴮ-trans (+ᴮ-cong a₁≈0 a₂≈0) (+ᴮ-identityˡ 0ᴮ)) 
          (λ {r} a≈0 → ≈ᴮ-trans (*ᴮ-congˡ a≈0) (*ᴮ-zeroʳ r) ) 
          ≈ᴮ-refl)

  -- The set of all elements in A that map *to* 0ᴮ
  Kernel : Pred A _ 
  Kernel = preimage 𝓜ᴬ⊸𝓜ᴮ 0*

  .Kernel′ : SubLeftSemimodule 𝓜ᴬ _ 
  Kernel′ = preimageIsSub 𝓜ᴬ⊸𝓜ᴮ 0*
```

#### Trivial Kernel

A trivial `Kernel` is one that this equivalent to the
singleton identity element in the domain.

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where
  open LeftSemimodule 𝓜ᴬ renaming (0ᴹ to 0ᴬ; _≈ᴹ_ to _≈ª_)
  
  TrivialKernel : 𝓜ᴬ ⊸ 𝓜ᴮ → Set (a ⊔ ℓᵃ ⊔ b ⊔ ℓᵇ)
  TrivialKernel T = Kernel T ≐ (_≈ª 0ᴬ)
```

#### All linear maps have a Kernel

First point of @Aluffi2021 Proposition 6.2: a kernel exists.

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where
  open LeftSemimodule 𝓜ᴬ renaming ( 0ᴹ to 0ᴬ )
  open LeftSemimodule 𝓜ᴮ renaming ( 0ᴹ to 0ᴮ ; ≈ᴹ-refl to reflᴮ )

  .∃Kernel : ∀ (T : 𝓜ᴬ ⊸ 𝓜ᴮ) → ∃ (Kernel T)
  ∃Kernel T = 0ᴬ , (0ᴮ , reflᴮ) , _⊸_.⊸-unital T
```

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where
  open LeftSemimodule 𝓜ᴬ renaming 
    ( 0ᴹ to 0ᴬ 
    ; Carrierᴹ to A
    ; _≈ᴹ_ to _≈ᵃ_
    ; ≈ᴹ-sym to ≈ᵃ-sym
    ; ≈ᴹ-trans to transᴬ
    ; ≈ᴹ-setoid to ≈ᵃ-setoid
    )
  open LeftSemimodule 𝓜ᴮ renaming 
    ( 0ᴹ to 0ᴮ 
    ; _≈ᴹ_ to _≈ᵇ_
    ; ≈ᴹ-refl to reflᴮ
    ; ≈ᴹ-sym to symᴮ
    ; ≈ᴹ-trans to transᴮ
    ; ≈ᴹ-setoid to ≈ᵇ-setoid
    )

  .inj→trivialker : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → Injective T → TrivialKernel T 
  inj→trivialker {T} inj = 
    (λ ((b , b≈0ᴮ), fx≈b) → inj (transᴮ (transᴮ fx≈b b≈0ᴮ) (symᴮ ⊸-unital))) 
    , λ a≈0ᴬ → (0ᴮ , reflᴮ) , transᴮ (⊸-cong a≈0ᴬ) ⊸-unital
      where open _⊸_ T
  
  --- STUCK 
  -- .trivialker→inj : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → TrivialKernel T → Injective T 
  -- trivialker→inj {T} (Tx≈0ᵇ→0ᵃ≈x , 0ᵃ≈x→Tx≈0ᵇ) {x = a₁} {y = a₂} Ta₁≈Ta₂ = 
  --     transᴬ 
  --       (Tx≈0ᵇ→0ᵃ≈x {x = a₁} {!   !})
  --       {!   !}
  --       where open _⊸_ T
```

## Submodule generated by predicate

```agda
module Generation {ℓ} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where
  open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A)
  open import LinearMap.Submodule.Algebra using (_⊆_; ⋂)

  -- The indexed collection of submodules that contain S.
  ⊆-collection : ∀ {i} {I : Set i} → Pred A ℓ → Set _ 
  ⊆-collection {I = I} S = 
    Σ (I → SubLeftSemimodule 𝓜ᴬ ℓ) 
      λ f → ∀ {i : I} → S ⊆′ P (f i)

  HasCollection : ∀ {i} → Set i → Set _ 
  HasCollection {i} I = Σ (Pred A ℓ) (⊆-collection {i = i} {I = I})

  -- The submodule generated by an indexed collection
  -- of all subsemimodules which contain S.
  ⟨_⟩ : ∀ {I : Set i} → HasCollection I → SubLeftSemimodule 𝓜ᴬ _
  ⟨ (_ , f , _) ⟩ = ⋂ f

  -- Set of all submodules that generate a subset for an index set
  _IsGeneratedBy_ : SubLeftSemimodule 𝓜ᴬ (i ⊔ ℓ) → Set i → Set _ 
  S IsGeneratedBy I = ∃ (λ f → S ≈ ⟨_⟩ {I = I} f)
```

## Linear Combinations

* Definition 2.2 in @Blyth2018

A linear combination is a (finite) sum of products 
of scalars (elements of the semiring)
and elements *of a given SubLeftSemimodule*.
First, we define the computational formula for a linear combination.

```agda
module _ {ℓ} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
  (S : SubLeftSemimodule 𝓜ᴬ ℓ) where
  
  open LeftSemimodule 𝓜ᴬ 
    renaming (Carrierᴹ to A; _≈ᴹ_ to  _≈ᴬ_ ; +ᴹ-monoid to monoidᴬ) 
    using (_*ₗ_)
  open LeftSemimodule (𝓡* ⊕ₗ subLeftSemimodule S) 
    renaming (Carrierᴹ to R×∃P ; ≈ᴹ-setoid to R×∃P-setoid )
    using ()

  ∑ : ∀ {n} → (Fin n → R×∃P) → A
  ∑ xs = fold R×∃P-setoid ⦃ monoidᴬ ⦄ xs λ (r , (a , _)) → r *ₗ a
```

Then, we can define the predicate identifing linear combinations
and the `SubLeftSemimodule` of linear combinations generated by 
a given `SubLeftSemimodule`.

```agda
module LC {ℓ} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where
  open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A; ≈ᴹ-refl to reflᴬ ; ≈ᴹ-setoid to setoidᴬ) 
  
  LinearCombination : SubLeftSemimodule 𝓜ᴬ ℓ → Pred A (r ⊔ a ⊔ ℓᵃ ⊔ ℓ)
  LinearCombination S a = ∃₂ λ n as → a ≈ᴹ ∑ S {n = n} as

  LC : SubLeftSemimodule 𝓜ᴬ ℓ → SubLeftSemimodule 𝓜ᴬ (r ⊔ a ⊔ ℓᵃ ⊔ ℓ)
  LC S = 
    mk 
      (LinearCombination S) 
      (mk 
        (λ (n₁ , f₁ , a₁≈∑f₁ ) (n₂ , f₂ , a₂≈∑f₂) 
          →   n₁ + n₂ 
            , [ f₁ , f₂ ] ∘ᶠ (splitAt n₁)
            , ≈ᴹ-trans 
                (+ᴹ-cong a₁≈∑f₁ a₂≈∑f₂) 
                (⊙-preserves-++ ≈ᴹ-setoid ⦃ +ᴹ-commutativeMonoid ⦄
                  f₁ f₂ (λ (r , (a , _)) → r *ₗ a) 
                  (×.uncurry *ₗ-cong)
                  )
        ) 
        (λ { {r} (n , f , a≈∑f) 
          →  n 
            , ×.map₁ (r *ᴿ_) ∘ᶠ f
            , ≈ᴹ-trans (*ₗ-congˡ a≈∑f) (∑-preserves-⋆ₗ f r)
            }) 
        (0 , (λ ()) , reflᴬ)
      )
      where open LeftSemimodule (𝓡* ⊕ₗ subLeftSemimodule S) using (≈ᴹ-setoid)
            -- TODO: find a more appropriate location for this theorem:
            ∑-preserves-⋆ₗ : ∀ {n} → (f : Fin n → R × ∃ (P S))
              → ∀ (r : R) → r *ₗ (∑ S f) ≈ᴹ ∑ S λ x → ×.map₁ (r *ᴿ_) (f x)
            ∑-preserves-⋆ₗ {n = ℕ.zero} _ r = *ₗ-zeroʳ r
            ∑-preserves-⋆ₗ {n = ℕ.suc n} f r =
              begin 
                r *ₗ ∑ S f
              ≡⟨⟩
                r *ₗ ((×.uncurry (λ r₁ ( a , _) → r₁ *ₗ a) (f zero)) 
                        +ᴹ ∑ S {n = n} (f ∘ᶠ suc))
              ≈⟨ *ₗ-distribˡ r 
                     (×.uncurry (λ r₁ ( a , _) → r₁ *ₗ a) (f zero))
                     (∑ S {n = n} ( f ∘ᶠ suc) ) 
                ⟩
                  r *ₗ (×.uncurry (λ r₁ ( a , _) → r₁ *ₗ a) (f zero)) 
                +ᴹ r *ₗ ∑ S {n = n} (f ∘ᶠ suc)
              ≈⟨ +ᴹ-congˡ (∑-preserves-⋆ₗ {n = n} (f ∘ᶠ suc) r) ⟩
                 r *ₗ (×.uncurry (λ r₁ ( a , _) → r₁ *ₗ a) (f zero)) 
                +ᴹ ∑ S (λ x → ×.map₁ (_*ᴿ_ r) (f (suc x)))
              ≈⟨ +ᴹ-congʳ (≈ᴹ-sym (*ₗ-assoc r (×.proj₁ (f zero)) 
                          (×.proj₁ (×.proj₂ (f zero))))) ⟩
                (∑ S λ x → ×.map₁ (r *ᴿ_) (f x))
              ∎
                where open ≈-Reasoning setoidᴬ
```

The predicate of the submodule generated by a predicate `S` 
(denoted $\langle S \rangle$)
is equivalent to the singleton set $0$ if $S = \emptyset$
and the set of all linear combinations ($LC(S)$) 
if $S$ is satisfiable.

* Theorem 2.2 in @Blyth2018

```agda 
module _ {i} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where
  open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A)

  open Generation {ℓ = i ⊔ r ⊔ a ⊔ ℓᵃ} {𝓜ᴬ = 𝓜ᴬ} 
  open LC {ℓ = i ⊔ r ⊔ a ⊔ ℓᵃ} {𝓜ᴬ = 𝓜ᴬ}
  open import LinearMap.Submodule.Algebra

  S⊆LC : ∀ {S : SubLeftSemimodule 𝓜ᴬ (i ⊔ r ⊔ a ⊔ ℓᵃ)}
      → S ⊆ LC S
  S⊆LC {x = a} a∈S = 
      1 
    , (λ _ → 1# , a , a∈S) 
    , ≈ᴹ-trans 
        (≈ᴹ-sym (*ₗ-identityˡ a))
        (≈ᴹ-sym (+ᴹ-identityʳ (1# *ₗ a)))
  
  -- TODO: complete the following
  --       though perhaps the propositions aren't stated correctly (?)
  
  -- ⟨S⟩⊆S : ∀ {I : Set i} 
  --     → {S : SubLeftSemimodule 𝓜ᴬ (i ⊔ r ⊔ a ⊔ ℓᵃ)}
  --     → {S* : HasCollection I}
  --     -- → NonEmpty S
  --     → ⟨ S* ⟩  ⊆  S
  -- ⟨S⟩⊆S {x = a} a∈⟨S⟩ = {!  a∈⟨S⟩ !}

  -- ⟨S⟩⊆LC : ∀ {I : Set i} 
  --     → {S : SubLeftSemimodule 𝓜ᴬ (i ⊔ r ⊔ a ⊔ ℓᵃ)}
  --     → {S* : HasCollection I}
  --     -- → NonEmpty S
  --     → ⟨ S* ⟩  ⊆  LC S
  -- ⟨S⟩⊆LC {S = S} {S*} = S⊆LC {S = S} ∘ᶠ ⟨S⟩⊆S {S = S} {S* = S*}

  -- LC⊆⟨S⟩ : ∀ {I : Set i}
  --     → {S : SubLeftSemimodule 𝓜ᴬ (i ⊔ r ⊔ a ⊔ ℓᵃ)}
  --     → {S* : HasCollection I}
  --     → NonEmpty S
  --     → LC S ⊆ ⟨ S* ⟩
  -- LC⊆⟨S⟩ {S* = (S , f , S→Pfi) } s {a} (n , xs , a≈∑xs) i = S→Pfi {!    !}
```

## Finitely Generated Submodules/modules

```agda
module _  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where
  open import LinearMap.Submodule.Algebra {𝓡 = 𝓡} {𝓜ᴬ}

  IsSubFinitelyGenerated : ∀ {ℓ} → SubLeftSemimodule 𝓜ᴬ ℓ → Set _ 
  IsSubFinitelyGenerated {ℓ} S =
    ∃ λ n → 
      Σ (Surjectionᶠ (fin n) (setoid (SubLeftSemimodule 𝓜ᴬ ℓ))) 
        λ f → S ≈ ⋂ (Surjectionᶠ.to f)
```

```agda
FinitelyGenerated : LeftSemimodule 𝓡 a ℓᵃ → Set (r ⊔ a ⊔ ℓsuc ℓᵃ)
FinitelyGenerated 𝓜ᴬ = IsSubFinitelyGenerated (trivial 𝓜ᴬ)
```

## Sequences of Submodules

```agda
module _  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
          {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ}
          {𝓜ᶜ : LeftSemimodule 𝓡 c ℓᶜ} where

  Exact : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴮ ⊸ 𝓜ᶜ → Set _
  Exact T₁ T₂ = Image T₁ ≐ Kernel T₂

  SemiExact : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴮ ⊸ 𝓜ᶜ → Set _
  SemiExact T₁ T₂ = Image T₁ ⊆′ Kernel T₂
```

* Theorem 3.6 in @Blyth2018 (1)

```agda
module _  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
          {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ}
          {T : 𝓜ᴬ ⊸ 𝓜ᴮ} where

  open LeftSemimodule 𝓜ᴬ 
    renaming 
      ( Carrierᴹ to A
      ; _≈ᴹ_ to _≈ᴬ_
      ; ≈ᴹ-setoid to setoidᴬ
      ; 0ᴹ to 0ᴬ
      ; ≈ᴹ-refl to reflᴬ
      ; ≈ᴹ-trans to transᴬ
      ; ≈ᴹ-sym to symᴬ)
  open LeftSemimodule 𝓜ᴮ 
    renaming (
        Carrierᴹ to B 
      ; 0ᴹ to 0ᴮ
      ; _≈ᴹ_ to _≈ᴮ_ 
      ; ≈ᴹ-refl to reflᴮ
      ; ≈ᴹ-trans to transᴮ
      ; ≈ᴹ-sym to symᴮ)
  open _⊸_ T

  -- TODO: struggling with this proof.
  --       feels like I'm running in circles.
  -- .exact⇒injective : Exact ¡ T → Injective T
  -- exact⇒injective (im⇒ker , ker⇒im) {a₁} {a₂} fa₁≈fa₂ = 
  --   begin 
  --     a₁
  --   ≈⟨ symᴬ (×.proj₂ (ker⇒im ((f 0ᴬ , ⊸-unital) , {!  !}))) ⟩
  --     0ᴬ
  --   ≈⟨ (×.proj₂ (ker⇒im ((f 0ᴬ , ⊸-unital) , transᴮ (symᴮ fa₁≈fa₂) {!   !}))) ⟩ 
  --     a₂
  --   ∎
  --    where open ≈-Reasoning setoidᴬ
  --          open _⊸_ (inclusion⊸ (0* T)) renaming (f to f*) using ()


  .injective⇒exact : Injective T → Exact ¡ T
  injective⇒exact inj = 
     (λ {a} (_ , 0≈a) → (f a , transᴮ (symᴮ (⊸-cong 0≈a)) ⊸-unital) , reflᴮ) 
    , λ ((_ , b≈0) , fa≈b) 
      → (lift tt , lift tt) , inj (transᴮ ⊸-unital (transᴮ (symᴮ b≈0) (symᴮ fa≈b)))
```
 
* Theorem 3.6 in @Blyth2018 (2)

```agda
module _  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
          {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ}
          {T : 𝓜ᴬ ⊸ 𝓜ᴮ} where

  open LeftSemimodule 𝓜ᴬ renaming ( ≈ᴹ-refl to reflᴬ )
  open LeftSemimodule 𝓜ᴮ renaming (≈ᴹ-trans to transᴮ)
  open _⊸_ T

  .exact⇒surjective : Exact T ! → Surjective T
  exact⇒surjective (im⇒ker , ker⇒im) b = 
     let v = ker⇒im {b} ((lift tt , lift tt) , (lift tt))
     in 
       ×.proj₁ (×.proj₁ v) , λ x → transᴮ (⊸-cong x) (×.proj₂ v)

  .surjective⇒exact : Surjective T → Exact T !
  surjective⇒exact sur = 
      (λ _ → (lift tt , lift tt) , (lift tt)) 
    , λ {b} _ → ( ×.proj₁ (sur b) , lift tt) , ×.proj₂ (sur b) reflᴬ
```

### Short Exact Sequence

```agda
module _  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
          {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ}
          {𝓜ᶜ : LeftSemimodule 𝓡 c ℓᶜ}
          where
    
  ShortExact : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴮ ⊸ 𝓜ᶜ → Set _ 
  ShortExact T₁ T₂ = Exact ¡ T₁ × Exact T₁ T₂ × Exact T₂ !
```
