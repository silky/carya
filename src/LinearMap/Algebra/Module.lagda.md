---
title: Algebraic Module Constructs on Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (
  CommutativeSemiring ; Semiring ; CommutativeMonoid)
open import Algebra.Module.Bundles using (LeftSemimodule)

module LinearMap.Algebra.Module
    {a b r ℓᵃ ℓᵇ ℓʳ : Level} 
    (C𝓡 : CommutativeSemiring r ℓʳ) 
    (𝓜ᴬ : LeftSemimodule (CommutativeSemiring.semiring C𝓡) a ℓᵃ) 
    (𝓜ᴮ : LeftSemimodule (CommutativeSemiring.semiring C𝓡) b ℓᵇ) where

private
  𝓡 = CommutativeSemiring.semiring C𝓡

open import LinearMap.Core 𝓡
open import LinearMap.Base 𝓡
open import LinearMap.Algebra 𝓡

open LeftSemimodule 𝓜ᴬ
  renaming (
    Carrierᴹ to A
  ; _+ᴹ_ to _+ᴬ_
  ; _*ₗ_ to _*ᴬ_  
  ) using ()

open LeftSemimodule 𝓜ᴮ
  renaming (
    Carrierᴹ to B
  ; ≈ᴹ-refl to reflᴮ
  ; _≈ᴹ_ to _≈ᴮ_
  ; _*ₗ_ to _*ᴮ_
  ; _+ᴹ_ to _+ᴮ_
  )

open Semiring 𝓡
  renaming 
  ( refl to reflᴿ 
  ; Carrier to R
  ; _*_ to _*ᴿ_
  )
  using ()
```

</details>

## Algebra Bundles with `_+_` and `_*ₗ_`

```agda
leftSemimodule : LeftSemimodule 𝓡 (a ⊔ b ⊔ r ⊔ ℓᵃ ⊔ ℓᵇ ⊔ ℓʳ) (a ⊔ ℓᵇ)
leftSemimodule = record
  { _*ₗ_ = _*ₗ_ {C𝓡 = CommutativeSemiring.isCommutativeSemiring C𝓡}
  ; isLeftSemimodule = record { 
      +ᴹ-isCommutativeMonoid = isCommutativeMonoid
    ; isPreleftSemimodule = record
      { *ₗ-cong =  λ x y → *ₗ-cong  x y
      ; *ₗ-zeroˡ = λ f {a} → *ₗ-zeroˡ (f ·ᵥ a)
      ; *ₗ-distribʳ = λ f r₁ r₂ {a} → *ₗ-distribʳ (f ·ᵥ a) r₁ r₂
      ; *ₗ-identityˡ = λ f {a} → *ₗ-identityˡ (f ·ᵥ a)
      ; *ₗ-assoc = λ r₁ r₂ f {a} → *ₗ-assoc r₁ r₂ (f ·ᵥ a)
      ; *ₗ-zeroʳ = λ r → *ₗ-zeroʳ r
      ; *ₗ-distribˡ = λ r f g {a} → *ₗ-distribˡ r (f ·ᵥ a) (g ·ᵥ a)
      }
    }
  }
  where open CommutativeMonoid (0-+-commutativeMonoid 𝓜ᴬ 𝓜ᴮ)
```
 