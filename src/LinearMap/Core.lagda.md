---
title: Linear Maps with Bundles as Objects
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (Semiring; CommutativeMonoid)

module LinearMap.Core {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

open import Algebra.Construct.Function using (commutativeMonoid)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Function.Definitions using (Congruent)
open import Relation.Binary.Structures using (IsEquivalence)

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open Semiring 𝓡
    renaming 
    ( Carrier to R
    ; _≈_ to _≈ᴿ_
    ; _+_ to _+ᴿ_
    ; refl to reflᴿ
    ) 
    using (
      1#
    ; 0#
    )
```

</details>

## Linear Map

```agda
record _⊸_ {a b ℓᵃ ℓᵇ} 
    (𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ)
    (𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ) : Set (a ⊔ b ⊔ r ⊔ ℓᵃ ⊔ ℓʳ ⊔ ℓᵇ) where

  constructor mk 

  open LeftSemimodule 𝓜ᴬ renaming 
      ( Carrierᴹ to A
      ; _+ᴹ_ to _+ᴬ_
      ; _*ₗ_ to _*ᴬ_
      ;  _≈ᴹ_ to _≈ᴬ_
      ; *ₗ-zeroˡ to *ᴬ-zeroˡ
      ; ≈ᴹ-sym to symᴬ
      ; 0ᴹ to 0ᴬ
      )
      using ()
  open LeftSemimodule 𝓜ᴮ renaming 
      ( Carrierᴹ to B
      ; _+ᴹ_ to _+ᴮ_
      ; _*ₗ_ to _*ᴮ_
      ; _≈ᴹ_ to _≈ᴮ_
      ; 0ᴹ to 0ᴮ
      ; ≈ᴹ-setoid to setoidᴮ
      )
  
  field
    f : A → B
    .isCongruent : Congruent _≈ᴬ_ _≈ᴮ_ f
    .isAdditive : ∀ {a₁ a₂ : A} → f (a₁ +ᴬ a₂) ≈ᴮ f a₁ +ᴮ f a₂
    .isHomogeneous : ∀ {r : R} {a : A} → r *ᴮ f a ≈ᴮ f (r *ᴬ a)

  .⊸-cong : Congruent _≈ᴬ_ _≈ᴮ_ f
  ⊸-cong = isCongruent

  .⊸-homogenenous : ∀ {r : R} {a : A} → r *ᴮ f a ≈ᴮ f (r *ᴬ a)
  ⊸-homogenenous = isHomogeneous

  .⊸-additive : ∀ {a₁ a₂ : A} → f (a₁ +ᴬ a₂) ≈ᴮ f a₁ +ᴮ f a₂
  ⊸-additive = isAdditive

  .⊸-unital : f 0ᴬ ≈ᴮ 0ᴮ 
  ⊸-unital = 
    begin 
      f 0ᴬ
    ≈⟨ ⊸-cong (symᴬ (*ᴬ-zeroˡ 0ᴬ)) ⟩
      f (0#  *ᴬ 0ᴬ)
    ≈˘⟨ ⊸-homogenenous ⟩
      (0# *ᴮ (f 0ᴬ))
    ≈⟨ *ₗ-zeroˡ (f 0ᴬ) ⟩ 
      0ᴮ 
    ∎
    where open ≈-Reasoning setoidᴮ

  -- Since linear maps are valued in a LeftSemimodule,
  -- the maps themselves are LeftSemimodules.
  ⊸-leftSemimodule : LeftSemimodule 𝓡 (a ⊔ b) (a ⊔ ℓᵇ)
  ⊸-leftSemimodule = record {
        isLeftSemimodule = record { 
          +ᴹ-isCommutativeMonoid = 
              CommutativeMonoid.isCommutativeMonoid 
                (commutativeMonoid A +ᴹ-commutativeMonoid)
        ; isPreleftSemimodule = record
              { *ₗ-cong = λ a fa≈ᴹga → *ₗ-cong a fa≈ᴹga
              ; *ₗ-zeroˡ = λ f → λ {a} → *ₗ-zeroˡ (f a)
              ; *ₗ-distribʳ = λ f r₁ r₂ → λ {a} → *ₗ-distribʳ (f a) r₁ r₂
              ; *ₗ-identityˡ = λ f → λ {a} → *ₗ-identityˡ (f a)
              ; *ₗ-assoc = λ r₁ r₂ f → λ {a} → *ₗ-assoc r₁ r₂ (f a)
              ; *ₗ-zeroʳ = λ r → *ₗ-zeroʳ r
              ; *ₗ-distribˡ = λ r f g → λ {a} → *ₗ-distribˡ r (f a) (g a)
              } 
        }
    }
```

## Equivalence

```agda
module _ {a b ℓᵃ ℓᵇ} 
         {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where
  open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A) using ()
  open LeftSemimodule 𝓜ᴮ renaming (Carrierᴹ to B) 
                          using (≈ᴹ-refl ; ≈ᴹ-sym; ≈ᴹ-trans)

  ⟦_⟧ : 𝓜ᴬ ⊸ 𝓜ᴮ → (A → B)
  ⟦ mk f _ _ _ ⟧ = f

  infix 4 _≈_

  _≈_ : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ → Set _
  T₁ ≈ T₂ = ⟦ T₁ ⟧ ≈ᴹ ⟦ T₂ ⟧
      where open LeftSemimodule (_⊸_.⊸-leftSemimodule T₁)

  isEquivalence : IsEquivalence _≈_
  isEquivalence = record 
    { refl = ≈ᴹ-refl 
    ; sym =  λ x → ≈ᴹ-sym x 
    ; trans = λ x y → ≈ᴹ-trans x y 
    }
```

## Application

```agda
module _ {a b ℓᵃ ℓᵇ}
         {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where
  open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A) using ()
  open LeftSemimodule 𝓜ᴮ renaming (Carrierᴹ to B) using ()

  infixr -1 _·ᵥ_
  
  _·ᵥ_ : 𝓜ᴬ ⊸ 𝓜ᴮ → A → B
  T ·ᵥ a = ⟦ T ⟧ a
```
