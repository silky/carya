---
title: Dual spaces and Linear Functionals
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (CommutativeSemiring ; Semiring)

module LinearMap.Dual {r ℓʳ} (C𝓡 : CommutativeSemiring r ℓʳ) where

open import Level using (Level; _⊔_ ; 0ℓ ; lift)
open import Algebra.Definitions using (Involutive)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.TensorUnit
   renaming (
     leftSemimodule to TU-leftSemimodule
   )
open import Function renaming (_∘_ to _∘ᶠ_) using ()

private
   𝓡 = CommutativeSemiring.semiring C𝓡

open import LinearMap.Core 𝓡
open import LinearMap.Base 𝓡

private 
   variable 
      a b c ℓᵃ ℓᵇ ℓᶜ : Level

open Semiring 𝓡
    renaming 
    ( refl to reflᴿ
    ; Carrier to R
    ; _≈_ to _≈ᴿ_
    ; _+_ to _+ᴿ_
    ; _*_ to _*ᴿ_
    )
```

</details>

## Dual space of a LeftSemimodule

`Dual` defines the [dual space](https://en.wikipedia.org/wiki/Dual_space)
the set of all linear functionals for a `LeftSemimodule`. 
A [linear functional](https://en.wikipedia.org/wiki/Linear_form)
is a linear map from a `LeftSemimodule` (or module or vector)
to its scalar space.
These are also referred to as linear forms or covectors.

```agda
Dual : ∀ {a ℓᵃ} → LeftSemimodule 𝓡 a ℓᵃ → Set (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) 
Dual M = M ⊸ TU-leftSemimodule
```

The function `_*` creates the `LeftSemimodule` in a Dual space.

```agda
_* :  ∀ {a ℓᵃ} → LeftSemimodule 𝓡 a ℓᵃ → LeftSemimodule 𝓡 _ _
M * = leftSemimodule 
 where open import LinearMap.Algebra.Module C𝓡 M TU-leftSemimodule
```

## Double Dual space of a LeftSemimodule

```agda
_** :  ∀ {a ℓᵃ} → LeftSemimodule 𝓡 a ℓᵃ → LeftSemimodule 𝓡 _ _
M ** = (M *) *
```

## Bilinear Properties of the "canonical" dual map

```agda
module CanonicalIsBilinear {a ℓᵃ} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where
   open LeftSemimodule 𝓜ᴬ 
      renaming (
        Carrierᴹ to A
      ; _+ᴹ_ to _+ᴬ_
      ; _*ₗ_ to _*ᴬ_
      ) 
      using ()
   
   open LeftSemimodule (𝓜ᴬ *)
      renaming (
         Carrierᴹ to Aᴰ
      ; _*ₗ_ to _*ᵈ_
      ) using ()

   ⟨_,_⟩ : A → Aᴰ → R
   ⟨ a , aᴰ ⟩ = aᴰ ·ᵥ a

   .linearₗ : ∀ {a₁ a₂ : A} {aᴰ : Aᴰ} 
       → ⟨ a₁ +ᴬ a₂ , aᴰ ⟩ ≈ᴿ ⟨ a₁ , aᴰ ⟩ +ᴿ ⟨ a₂ , aᴰ ⟩ 
   linearₗ {aᴰ = aᴰ} = _⊸_.⊸-additive aᴰ

   linearᵣ : ∀ {a : A} {a₁ᴰ a₂ᴰ : Aᴰ} 
       → ⟨ a , a₁ᴰ + a₂ᴰ ⟩ ≈ᴿ ⟨ a , a₁ᴰ ⟩ +ᴿ ⟨ a , a₂ᴰ ⟩ 
   linearᵣ {a₁ᴰ = a₁ᴰ} = reflᴿ

   .scaleₗ :  ∀ {r : R} {a : A} {aᴰ : Aᴰ} 
       → r *ᴿ ⟨ a , aᴰ ⟩ ≈ᴿ ⟨ r *ᴬ a , aᴰ ⟩
   scaleₗ {aᴰ = aᴰ} = _⊸_.⊸-homogenenous aᴰ
   
   scaleᵣ :  ∀ {r : R} {a : A} {aᴰ : Aᴰ}
       → ⟨ a , r *ᵈ aᴰ ⟩ ≈ᴿ ⟨ a , aᴰ ⟩ *ᴿ r
   scaleᵣ {r = r} {a = a} {aᴰ = aᴰ} = CommutativeSemiring.*-comm C𝓡 r (aᴰ ·ᵥ a)
```
