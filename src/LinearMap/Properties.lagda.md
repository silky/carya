---
title: Properties of Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (Semiring)

module LinearMap.Properties {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.DirectProduct 
  renaming (leftSemimodule to _⊕ₗₛ_) using ()
open import Data.Product as × using (_,_ ; _×_ ; ∃; Σ)
open import Function.Definitions as F using ()
open import Relation.Binary using (Rel; REL)
open import Relation.Unary using (_≐_)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open import LinearMap.Core 𝓡
open import LinearMap.Base 𝓡 

private
  variable
    a b c d ℓᵃ ℓᵇ ℓᶜ ℓᵈ : Level

open Semiring 𝓡 renaming 
  ( Carrier to Cᴿ
  ; _≈_ to _≈ᴿ_
  ; refl to reflᴿ
  ; zeroʳ to zeroʳᴿ
  ) 
  using (
    0#
  )
```

</details>

```agda
-- TODO: is this the standard name? 
--       should this go here since it's about the Semimodule not a map?
module _ {a ℓᵃ} (ℳ : LeftSemimodule 𝓡 a ℓᵃ) where
  open LeftSemimodule ℳ

  IsUnit : Carrierᴹ → Set (a ⊔ ℓᵃ)
  IsUnit m = m ≉ᴹ 0ᴹ → ∀ (m′ : Carrierᴹ) → m ≉ᴹ m′ → m′ ≈ᴹ 0ᴹ
```

## Injection/Surjection/Bijection 

```agda 
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where

  open LeftSemimodule 𝓜ᴬ renaming (_≈ᴹ_ to _≈ᴬ_)
  open LeftSemimodule 𝓜ᴮ renaming (_≈ᴹ_ to _≈ᴮ_)

  Injective : 𝓜ᴬ ⊸ 𝓜ᴮ → Set (a ⊔ ℓᵃ ⊔ ℓᵇ) 
  Injective T = F.Injective _≈ᴬ_ _≈ᴮ_ (T ·ᵥ_)

  Surjective : 𝓜ᴬ ⊸ 𝓜ᴮ → Set (a ⊔ ℓᵃ ⊔ b ⊔ ℓᵇ)
  Surjective T = F.Surjective _≈ᴬ_ _≈ᴮ_ (T ·ᵥ_)
  
  Bijective : 𝓜ᴬ ⊸ 𝓜ᴮ → Set (a ⊔ ℓᵃ ⊔ b ⊔ ℓᵇ)
  Bijective T = Injective T × Surjective T
```
## Monomorphic/Epimorphic/Isomorphic Linear maps

```agda 
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where

  Monomorphic : ∀ {c ℓᶜ} → 𝓜ᴬ ⊸ 𝓜ᴮ → Set _
  Monomorphic {c} {ℓᶜ} T = 
    ∀ {𝓜ᶜ : LeftSemimodule 𝓡 c ℓᶜ} {S₁ S₂ : 𝓜ᶜ ⊸ 𝓜ᴬ} 
    → T ∘ S₁ ≈ T ∘ S₂
    → S₁ ≈ S₂

  Epimorphic : ∀ {c ℓᶜ} → 𝓜ᴬ ⊸ 𝓜ᴮ → Set _
  Epimorphic {c} {ℓᶜ} T = 
    ∀ {𝓜ᶜ : LeftSemimodule 𝓡 c ℓᶜ} {S₁ S₂ : 𝓜ᴮ ⊸ 𝓜ᶜ} 
    → S₁ ∘ T ≈ S₂ ∘ T
    → S₁ ≈ S₂

  Isomorphic : ∀ {c₁ c₂ ℓᶜ₁ ℓᶜ₂} → 𝓜ᴬ ⊸ 𝓜ᴮ → Set _
  Isomorphic {c₁ = c₁} {c₂} {ℓᶜ₁} {ℓᶜ₂} T = 
    Monomorphic {c = c₁} {ℓᶜ₁} T × Epimorphic {c = c₂} {ℓᶜ₂} T
```

### Equivalence of Injective/Monomorphic

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where
  
  open LeftSemimodule 𝓜ᴬ -- renaming (_≈ᴹ_ to _≈ᴮ_)

  Inj→Mono : ∀ {c ℓᶜ} {T : 𝓜ᴬ ⊸ 𝓜ᴮ} 
      → Injective T 
      → Monomorphic {c = c} {ℓᶜ = ℓᶜ} T 
  Inj→Mono inj x = inj x
  
  -- TODO: stuck on the following.
  -- Mono→Inj : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} 
  --     → Monomorphic {c = {!   !}} {ℓᶜ = {!   !}} T 
  --     → Injective T
  -- Mono→Inj {T = T} mono {x = x} {y = y} Tx≈Ty =
  --     mono 
  --       {𝓜ᶜ = 𝟘-leftSemimodule } 
  --       {S₁ = mk {!   !} {!   !} {!   !} {!   !}} 
  --       {S₂ = mk {!   !} {!   !} {!   !} {!   !}} 
  --       {!   !} -- Tx≈Ty
  --      where open _⊸_ T
```

### Equivalence of Surjective/Epimorphic

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where
  
  open LeftSemimodule 𝓜ᴬ renaming (≈ᴹ-refl to ≈ᵃ-refl) using ()

  .Sur→Epi : ∀ {c ℓᶜ} {T : 𝓜ᴬ ⊸ 𝓜ᴮ} 
      → Surjective T 
      → Epimorphic {c = c} {ℓᶜ = ℓᶜ} T 
  Sur→Epi surjection {𝓜ᶜ = 𝓜ᶜ} {S₁ = S₁} {S₂ = S₂} s₁fx≈s₂fx {x = x} = 
   let (a , surject) = surjection x
       Ta≈x = surject {z = a} ≈ᵃ-refl
   in ≈ᴹ-trans 
        (≈ᴹ-trans (≈ᴹ-sym (s₁-cong Ta≈x)) s₁fx≈s₂fx) 
        (s₂-cong Ta≈x)
    where open _⊸_ S₁ renaming (f to s₁; ⊸-cong to s₁-cong) using ()
          open _⊸_ S₂ renaming (f to s₂; ⊸-cong to s₂-cong) using ()
          open LeftSemimodule 𝓜ᶜ
```

## Invertible

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where

  _IsRightInverseOf_ : REL (𝓜ᴮ ⊸ 𝓜ᴬ) (𝓜ᴬ ⊸ 𝓜ᴮ) _ 
  T⁻¹ IsRightInverseOf T = T ∘ T⁻¹ ≈ id
   
  RightInvertible : 𝓜ᴬ ⊸ 𝓜ᴮ → Set _
  RightInvertible T = ∃ (_IsRightInverseOf T)

  _IsLeftInverseOf_ : REL (𝓜ᴮ ⊸ 𝓜ᴬ) (𝓜ᴬ ⊸ 𝓜ᴮ) _ 
  T⁻¹ IsLeftInverseOf T = T⁻¹ ∘ T ≈ id

  LeftInvertible : 𝓜ᴬ ⊸ 𝓜ᴮ → Set _
  LeftInvertible T = ∃ (_IsLeftInverseOf T)
```

# Properties of Composition

## Bilinearity

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where

  -- eq 4
  ∘-identityˡ : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → id ∘ T ≈ T
  ∘-identityˡ {T} = ≈ᴹ-refl {x = ⟦ T ⟧}
    where open LeftSemimodule (_⊸_.⊸-leftSemimodule T)

  -- eq 4
  ∘-identityʳ : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → T ∘ id ≈ T
  ∘-identityʳ {T} = ≈ᴹ-refl {x = ⟦ T ⟧}
    where open LeftSemimodule (_⊸_.⊸-leftSemimodule T)

  -- eq 7 
  +-identityˡ : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → zero {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ = 𝓜ᴮ} + T ≈ T
  +-identityˡ {T} {x} = +ᴹ-identityˡ (T ·ᵥ x)
    where open LeftSemimodule 𝓜ᴮ

  -- eq 7 
  +-identityʳ : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → T + zero {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ = 𝓜ᴮ} ≈ T
  +-identityʳ {T} {x} = +ᴹ-identityʳ (T ·ᵥ x)
    where open LeftSemimodule 𝓜ᴮ

  -- eq 8
  ∘-zeroˡ : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → zero {𝓜ᴬ = 𝓜ᴮ} {𝓜ᴮ = 𝓜ᴬ} ∘ T ≈ zero
  ∘-zeroˡ = ≈ᴹ-refl
    where open LeftSemimodule 𝓜ᴬ

  -- eq 8
  .∘-zeroʳ : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → T ∘ (zero {𝓜ᴬ = 𝓜ᴮ} {𝓜ᴮ = 𝓜ᴬ}) ≈ zero
  ∘-zeroʳ {T = T} {x = x} =
    begin 
      (T ·ᵥ 0ᴬ) 
    ≈⟨ _⊸_.⊸-cong T (symᵃ ( *ₗ-zeroᵃ 0ᴬ)) ⟩
      (T ·ᵥ ( 0# *ₐ 0ᴬ))
    ≈˘⟨ _⊸_.⊸-homogenenous T {r = 0#} {a = 0ᴬ} ⟩
      0# *ᴮ (T ·ᵥ 0ᴬ)
    ≈⟨ *ₗ-zeroˡ (T ·ᵥ 0ᴬ) ⟩
      0ᴹ 
    ∎
     where open LeftSemimodule 𝓜ᴬ 
                renaming (
                  0ᴹ to 0ᴬ
                ; _*ₗ_ to _*ₐ_
                ; ≈ᴹ-sym to symᵃ
                ; *ₗ-zeroˡ to *ₗ-zeroᵃ
                ) 
                using ()
           open LeftSemimodule 𝓜ᴮ 
                 renaming (_*ₗ_ to _*ᴮ_)

           open ≈-Reasoning ≈ᴹ-setoid
```

## Disributivity of Composition over Addition

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ}
         {Mᶜ : LeftSemimodule 𝓡 c ℓᶜ} where

  -- eq 9
  .distribˡ : ∀ (T : 𝓜ᴮ ⊸ Mᶜ) {U V : 𝓜ᴬ ⊸ 𝓜ᴮ} → T ∘ (U + V) ≈ (T ∘ U) + (T ∘ V)
  distribˡ T = _⊸_.⊸-additive T

  -- eq 10
  distribʳ : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} {U V : 𝓜ᴮ ⊸ Mᶜ} → (U + V) ∘ T ≈ (U ∘ T) + (V ∘ T)
  distribʳ = ≈ᴹ-refl
      where open LeftSemimodule Mᶜ
```

# Properties of `proj`/`inj`

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} where

  open LeftSemimodule 𝓜ᴬ renaming (+ᴹ-identityʳ to 𝓜ᴬ-identityʳ)
  open LeftSemimodule 𝓜ᴮ 
    renaming (
      +ᴹ-identityˡ to 𝓜ᴮ-identityˡ
    ; +ᴹ-identityʳ to 𝓜ᴮ-identityʳ)
  
  private
    π₁ : (𝓜ᴬ ⊕ₗₛ 𝓜ᴮ) ⊸ 𝓜ᴬ
    π₁ = proj₁

    π₂ : (𝓜ᴬ ⊕ₗₛ 𝓜ᴮ) ⊸ 𝓜ᴮ
    π₂ = proj₂

    ι₁ : 𝓜ᴬ ⊸ (𝓜ᴬ ⊕ₗₛ 𝓜ᴮ)
    ι₁ = inj₁

    ι₂ : 𝓜ᴮ ⊸ (𝓜ᴬ ⊕ₗₛ 𝓜ᴮ)
    ι₂ = inj₂
  
  -- eq 11 
  π₁∘ι₁≡id : π₁ ∘ ι₁ ≈ id {𝓜ᴬ = 𝓜ᴬ} 
  π₁∘ι₁≡id {x} = 𝓜ᴬ-identityʳ x

  -- eq 12
  π₂∘ι₂≡id : π₂ ∘ ι₂ ≈ id {𝓜ᴬ = 𝓜ᴮ}
  π₂∘ι₂≡id {x} = 𝓜ᴮ-identityˡ x
  
  -- Orthogonality
  -- eq 14
  π₁∘ι₂≡zero : π₁ ∘ ι₂ ≈ zero {𝓜ᴬ = 𝓜ᴮ} {𝓜ᴮ = 𝓜ᴬ}
  π₁∘ι₂≡zero {x} = 𝓜ᴬ-identityʳ (⟦ zero {𝓜ᴬ = 𝓜ᴮ} {𝓜ᴮ = 𝓜ᴬ} ⟧ x)

  -- eq 15
  π₂∘ι₁≡zero : π₂ ∘ ι₁ ≈ zero {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ = 𝓜ᴮ}
  π₂∘ι₁≡zero {x} = 𝓜ᴮ-identityʳ (⟦ zero {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ = 𝓜ᴮ} ⟧ x)

  -- eq 18
  ι₁▿ι₂≡id : ι₁ ▿ ι₂ ≈ id {𝓜ᴬ = 𝓜ᴬ ⊕ₗₛ 𝓜ᴮ}
  ι₁▿ι₂≡id {(a , b)} = 𝓜ᴬ-identityʳ a , 𝓜ᴮ-identityˡ b

  -- eq 19
  π₁▵π₂≡id : π₁ ▵ π₂ ≈ id {𝓜ᴬ = 𝓜ᴬ ⊕ₗₛ 𝓜ᴮ} 
  π₁▵π₂≡id {(a , b)} = 𝓜ᴬ-identityʳ a , 𝓜ᴮ-identityˡ b
```

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} 
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓᵇ} 
         {Mᶜ : LeftSemimodule 𝓡 c ℓᶜ} where
  
  open LeftSemimodule 𝓜ᴬ 
    renaming (
      0ᴹ to 0ᴬ
    ; _+ᴹ_ to _+ᴬ_
    ; +ᴹ-identityʳ to A-identityʳ
    ; +ᴹ-identityˡ to A-identityˡ
    ; ≈ᴹ-sym to symᴬ
    ; ≈ᴹ-trans to transᴬ
    ; _≈ᴹ_ to _≈ᴬ_
    )
    using ()
  open LeftSemimodule 𝓜ᴮ 
    renaming (
      0ᴹ to 0ᴮ
    ; _+ᴹ_ to _+ᴮ_
    ; +ᴹ-identityʳ to B-identityʳ
    ; +ᴹ-identityˡ to B-identityˡ
    ; ≈ᴹ-sym to symᴮ
    ; ≈ᴹ-trans to transᴮ
    ) 
    using ()
  open LeftSemimodule Mᶜ 
    renaming (
      Carrierᴹ to C
    ; _+ᴹ_ to _+ᶜ_
    ;  ≈ᴹ-setoid to C-setoid
    ; _≈ᴹ_ to _≈ᶜ_
    )
  open LeftSemimodule (𝓜ᴬ ⊕ₗₛ 𝓜ᴮ)
    renaming (
      _≈ᴹ_ to _≈ᴬᴮ_
    ) using ()

  private
    π₁ : (𝓜ᴬ ⊕ₗₛ 𝓜ᴮ) ⊸ 𝓜ᴬ
    π₁ = proj₁

    π₂ : (𝓜ᴬ ⊕ₗₛ 𝓜ᴮ) ⊸ 𝓜ᴮ
    π₂ = proj₂  
  
  -- eq 13
  ι₁∘π₁+ι₂∘π₂≡id : (inj₁ ∘ π₁) + (inj₂ ∘ π₂) ≈ id {𝓜ᴬ = 𝓜ᴬ ⊕ₗₛ 𝓜ᴮ}
  ι₁∘π₁+ι₂∘π₂≡id {x = (a , b)} = 
      transᴬ (A-identityʳ (a +ᴬ 0ᴬ)) (A-identityʳ a)
    , transᴮ (B-identityˡ (0ᴮ +ᴮ b)) (B-identityˡ b)

  -- @macedo2013 eq 16
  .[T|U]≡T∘π₁+U∘π₂ : ∀ {T : 𝓜ᴬ ⊸ Mᶜ} {U : 𝓜ᴮ ⊸ Mᶜ} 
      → T ▿ U ≈ (T ∘ proj₁) + (U ∘ proj₂)
  [T|U]≡T∘π₁+U∘π₂ {T = T} {U = U} {x = (a , b)} = 
     +ᴹ-cong 
      (_⊸_.⊸-cong T (symᴬ (A-identityʳ a))) 
      (_⊸_.⊸-cong U (symᴮ (B-identityˡ b)))

  -- eq 17
  [T-U]≡ι₁∘T+ι₂∘U : ∀ {T : Mᶜ ⊸ 𝓜ᴬ} {U : Mᶜ ⊸ 𝓜ᴮ}
      → T ▵ U  ≈ (inj₁ ∘ T) + (inj₂ ∘ U)
  [T-U]≡ι₁∘T+ι₂∘U {T = T} {U = U} {x = x} = 
        (symᴬ (A-identityʳ (T ·ᵥ x))) 
      , (symᴮ (B-identityˡ (U ·ᵥ x)))
```

# Blocked Linear Algebra

The following laws are shown in Section 5 of @macedo2013.

## Fusion Laws

```agda
module _ {m : LeftSemimodule 𝓡 a ℓᵃ} 
         {n : LeftSemimodule 𝓡 b ℓᵇ} 
         {o : LeftSemimodule 𝓡 c ℓᶜ}
         {p : LeftSemimodule 𝓡 d ℓᵈ} where

  open LeftSemimodule n renaming (≈ᴹ-refl to n-refl) using ()
  open LeftSemimodule o renaming (≈ᴹ-refl to o-refl) using ()

  -- eq 26
  ▵-fusion : ∀ {T₁ : m ⊸ n} {T₂ : m ⊸ o} {T₃ : p ⊸ m} 
    → (T₃ · (T₁ ▵ T₂)) ≈ (T₃ · T₁) ▵ (T₃ · T₂)
  ▵-fusion = n-refl , o-refl
```

```agda
module _ {m : LeftSemimodule 𝓡 a ℓᵃ} 
         {n : LeftSemimodule 𝓡 b ℓᵇ} 
         {o : LeftSemimodule 𝓡 c ℓᶜ}
         {p : LeftSemimodule 𝓡 d ℓᵈ} where
  
  -- eq 27
  .▿-fusion : ∀ {T₁ : n ⊸ m} {T₂ : o ⊸ m} {T₃ : m ⊸ p} 
    → ((T₁ ▿ T₂) · T₃) ≈ (T₁ · T₃) ▿ (T₂ · T₃)
  ▿-fusion {T₁ = T₁} {T₂ = T₂} {T₃ = T₃} {x = x} = _⊸_.⊸-additive T₃
```

## Cancellation Laws

```agda
module _ {m : LeftSemimodule 𝓡 a ℓᵃ} 
         {n : LeftSemimodule 𝓡 b ℓᵇ}
         {o : LeftSemimodule 𝓡 c ℓᶜ} where

  open LeftSemimodule m 
    renaming (
      Carrierᴹ to A
    ;  _≈ᴹ_ to _≈ₘ_
    ; 0ᴹ to 0ᴬ
    ; +ᴹ-identityˡ to m-identityˡ
    ; +ᴹ-identityʳ to m-identityʳ
    ; +ᴹ-congʳ to m-congʳ
    ; +ᴹ-congˡ to m-congˡ
    ; ≈ᴹ-trans to transₘ)
  open LeftSemimodule n renaming (
    Carrierᴹ to B
    ; _≈ᴹ_ to _≈ₙ_ 
    ; ≈ᴹ-setoid to setoidₙ
    ; +ᴹ-identityʳ to n-identityʳ
    ; ≈ᴹ-trans to transₙ)
  open LeftSemimodule o renaming (+ᴹ-identityˡ to o-identityˡ)

  -- NOTE: @macedo2013 states these differently. (proj and inj are flipped)
  -- What's up with that?
  -- eqs. 28-29
  proj₁-cancels-▵ : ∀ {T₁ : m ⊸ n} {T₂ : m ⊸ o}
    → ((T₁ ∣ T₂) · proj₁) ≈ T₁
  proj₁-cancels-▵ {T₁ = T₁} {x = x} = n-identityʳ (T₁ ·ᵥ x)

  proj₂-cancels-▵ : ∀ {T₁ : m ⊸ n} {T₂ : m ⊸ o} 
    → ((T₁ ∣ T₂) · proj₂) ≈ T₂
  proj₂-cancels-▵ {T₂ = T₂} {x = x} = o-identityˡ (T₂ ·ᵥ x)
  
  -- TODO: 
  -- .inj₁-cancels-▿ : ∀ {T₁ : n ⊸ m} {T₂ : o ⊸ m}
  --   → (inj₁ · (T₁ — T₂)) ≈ T₁
  -- inj₁-cancels-▿ {T₁ = T₁} {T₂ = T₂} {x = x} = 
  --    transₘ 
  --       (m-congˡ (transₘ {! ∘-zeroʳ  !} ((∘-zeroˡ {𝓜ᴬ = m} {𝓜ᴮ = n} {T = {!   !}})))) 
  --       (m-identityʳ (T₁ ·ᵥ x))

  -- TODO: 
  -- inj₂-cancels-▿ : ∀ {T₁ : n ⊸ m} {T₂ : o ⊸ m} {x}
  --   → ⟦ (inj₂ · (T₁ — T₂)) ⟧ x  ≈ₘ ⟦ T₂ ⟧ x
  -- inj₂-cancels-▿ {T₁ = mk f _ _ _} {T₂ = mk g _ _ _ } {x = x} = 
  --    transₘ (m-congʳ {!   !}) (m-identityˡ (g x))
```

## "Abide" (junc/split exchange) laws

```agda
module _ {m : LeftSemimodule 𝓡 a ℓᵃ} 
         {n : LeftSemimodule 𝓡 b ℓᵇ}
         {o : LeftSemimodule 𝓡 c ℓᶜ}
         {p : LeftSemimodule 𝓡 d ℓᵈ} where
  open LeftSemimodule m renaming (_≈ᴹ_ to _≈ₘ_) using ()
  open LeftSemimodule n renaming (≈ᴹ-refl to reflₙ) using ()
  open LeftSemimodule o renaming (≈ᴹ-refl to reflₒ) using ()
  open LeftSemimodule p renaming (≈ᴹ-refl to reflₚ) using ()

  -- eq 30
  abide₁ : ∀ {T₁ : m ⊸ o} {T₂ : m ⊸ p} {T₃ : n ⊸ o} {T₄ : n ⊸ p}
    → ((T₁ ∣ T₂) — (T₃ ∣ T₄)) ≈ (T₁ — T₃) ∣ (T₂ — T₄)
  abide₁ = reflₒ , reflₚ

  -- TODO: what's the other eqn?
  -- abide₂ : ∀ {T₁ : m ⊸ o} {T₂ : m ⊸ p} {T₃ : n ⊸ o} {T₄ : n ⊸ p}
  --   → (T₁ — T₃) ∣ (T₂ — T₄) ≈ ?
  -- abide₂ = {!   !}
-- 
  -- eq 31
  split-add : ∀ {T₁ : m ⊸ n} {T₂ : m ⊸ o} {T₃ : m ⊸ n} {T₄ : m ⊸ o}
    → ((T₁ ∣ T₂) + (T₃ ∣ T₄)) ≈ (T₁ + T₃) ∣ (T₂ + T₄)
  split-add = reflₙ , reflₒ
```

```agda
module _ {m : LeftSemimodule 𝓡 a ℓᵃ} 
         {n : LeftSemimodule 𝓡 b ℓᵇ}
         {o : LeftSemimodule 𝓡 c ℓᶜ} where
  open LeftSemimodule m renaming (
     _+ᴹ_ to _+ₘ_
    ) using (≈ᴹ-setoid; +ᴹ-comm ; +ᴹ-assoc; +ᴹ-congˡ; +ᴹ-congʳ)

  -- eq 32
  junc-add : ∀ {T₁ : n ⊸ m} {T₂ : o ⊸ m} {T₃ : n ⊸ m} {T₄ : o ⊸ m}
    → ((T₁ — T₂) + (T₃ — T₄)) ≈ (T₁ + T₃) — (T₂ + T₄)
  junc-add {T₁ = T₁} {T₂ = T₂} {T₃ = T₃} {T₄ = T₄} {x = (a , b)} =
    let T₁a = T₁ ·ᵥ a
        T₂b = T₂ ·ᵥ b
        T₃a = T₃ ·ᵥ a
        T₄b = T₄ ·ᵥ b
    in 
      begin
        (T₁a +ₘ T₂b +ₘ (T₃a +ₘ T₄b))
      ≈⟨ +ᴹ-assoc T₁a T₂b (T₃a +ₘ T₄b) ⟩ 
        (T₁a +ₘ (T₂b +ₘ (T₃a +ₘ T₄b)))
      ≈˘⟨ +ᴹ-congˡ (+ᴹ-assoc T₂b  T₃a T₄b) ⟩ 
        T₁a +ₘ ((T₂b +ₘ T₃a) +ₘ T₄b)
      ≈⟨ +ᴹ-congˡ (+ᴹ-congʳ (+ᴹ-comm T₂b T₃a)) ⟩ 
        T₁a +ₘ ((T₃a +ₘ T₂b) +ₘ T₄b)
      ≈⟨ +ᴹ-congˡ (+ᴹ-assoc T₃a T₂b T₄b) ⟩
        T₁a +ₘ (T₃a +ₘ (T₂b +ₘ T₄b))
      ≈˘⟨ +ᴹ-assoc T₁a T₃a (T₂b +ₘ T₄b) ⟩
        (T₁a +ₘ T₃a +ₘ (T₂b +ₘ T₄b))
      ∎
    where open ≈-Reasoning ≈ᴹ-setoid
```

## Divide and Conquer

```agda 
module _ {m : LeftSemimodule 𝓡 a ℓᵃ} 
         {n : LeftSemimodule 𝓡 b ℓᵇ}
         {o : LeftSemimodule 𝓡 c ℓᶜ} 
         {p : LeftSemimodule 𝓡 d ℓᵈ} where
  open LeftSemimodule p using (_≈ᴹ_; ≈ᴹ-refl)

  -- eq 30 
  div&con : ∀ {T₁ : m ⊸ n} {T₂ : m ⊸ o} {T₃ : n ⊸ p} {T₄ : o ⊸ p}
    → ((T₁ ∣ T₂) · (T₃ — T₄)) ≈ (T₁ · T₃) + (T₂ · T₄)
  div&con = ≈ᴹ-refl
```
 