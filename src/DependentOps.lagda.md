---
title: Property Transfer for dependent pairs (`∃` and `∃₂`)
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

module DependentOps where 

open import Data.Product using (∃ ; ∃₂ ; _,_ ; _×_)
open import Level using (_⊔_)
open import Relation.Unary using (Pred)
open import Relation.Binary using (Rel)
```

</details>

::: NOTE

Much of the code and ideas below are borrowed
from the 
[code associated](https://github.com/conal/paper-2021-language-derivatives/blob/fc7fd2b31f8fedc0ee7f4b59c28aa2810c82b2e4/Existential.lagda)
with Conal Elliott's paper 
"Symbolic and Automatic Differentiation of Languages".

::::


## `∃`

```agda
module ∃op {a ℓ} {A : Set a} (P : A → Set ℓ) where
   D₀ : A → Set ℓ
   D₀ a = P a

   D₁ : (A → A) → Set (a ⊔ ℓ)
   D₁ g = ∀ {a} → P a → P (g a)

   D₂ : (A → A → A) → Set (a ⊔ ℓ)
   D₂ h = ∀ {a₁ a₂} → P a₁ → P a₂ → P (h a₁ a₂)
```

```agda
module ∃op' {a b ℓ} {A : Set a} {B  : Set b} (P : A → Set ℓ) where

   D₂' : (B → A → A) → Set (a ⊔ b ⊔ ℓ)
   D₂' h = ∀ {a b} → P a → P (h b a)
```

### Property Transfer 

```agda
module Inj {a ℓ} {A : Set a} {P : A → Set ℓ} where

   open ∃op P
   
   pattern inj p = (_ , p)

   -- Lift a unary operation on A to a unary operation on (∃ P)
   ∃inj₁ : ∀ {∙_ : A → A} (∙′_ : D₁ ∙_) → (∃ P → ∃ P)
   ∃inj₁ ∙′_ (inj x) = inj (∙′ x)

   -- Lift a binary operation on A to a binary operation on (∃ P)
   ∃inj₂ : ∀ {_∙_ : A → A → A} (_∙′_ : D₂ _∙_) → (∃ P → ∃ P → ∃ P)
   ∃inj₂ _∙′_ (inj x) (inj y) = inj (x ∙′ y)

   prop₁ : ∀ {p} {Q : Pred A p} → (∀ a → Q a) → ∀ ((a , _) : ∃ P) → Q a
   prop₁ Q (a , _) = Q a

   prop₂ : ∀ {p} {R : Rel A p} 
         → (∀ a₁ a₂ → R a₁ a₂) 
         → ∀ ((a₁ , _) (a₂ , _) : ∃ P) → R a₁ a₂
   prop₂ R (a₁ , _) (a₂ , _) = R a₁ a₂

   prop₃ : ∀ {p} {S :  A → A → A → Set p} 
          → (∀ a₁ a₂ a₃ → S a₁ a₂ a₃) 
          → ∀ ((a₁ , _) (a₂ , _) (a₃ , _) : ∃ P) 
          → S a₁ a₂ a₃
   prop₃ S (a₁ , _) (a₂ , _) (a₃ , _) = S a₁ a₂ a₃
```

```agda
module Inj' {a b ℓ} {A : Set a} {B : Set b} {P : A → Set ℓ} where

   open ∃op' {B = B} P
   
   pattern inj b = (_ , b)

   ∃inj₂' : ∀ {_∙_ : B → A → A} (_∙′_ : D₂' _∙_) → (B → ∃ P → ∃ P)
   ∃inj₂' _∙′_ b (inj x) = inj (_∙′_ {b = b} x)
```

## `∃₂`

```agda
module ∃₂op {a b ℓ} {A : Set a} {B : Set b} (R : A → B → Set ℓ) where
   R₀ : A → B → Set ℓ
   R₀ a b = R a b

   R₁ : (A → A) → (B → B) → Set (a ⊔ b ⊔ ℓ)
   R₁ f g = ∀ {a b} → R a b → R (f a) (g b)

   R₂ : (A → A → A) → (B → B → B) → Set (a ⊔ b ⊔ ℓ)
   R₂ f g = ∀ {a₁ a₂ b₁ b₂} → R a₁ b₁ → R a₂ b₂ → R (f a₁ a₂) (g b₁ b₂)
```

### Property Transfer

```agda
module Inj₂ {a b ℓ} {A : Set a} {B : Set b} {R : A → B → Set ℓ} where

   open ∃₂op R
   
   pattern inj r = (_ , _ , r)

   -- Lift a unary operation on A and 
   -- a unary operation on B
   -- to a unary operation on (∃ R)
   ∃inj₁ : ∀ {∙₁_ : A → A} {∙₂_ : B → B} 
           → (∙₁′_ : R₁ ∙₁_ ∙₂_ ) 
           → (∃₂ R → ∃₂ R)
   ∃inj₁ ∙′_ (inj x) = inj (∙′ x)

   -- Lift a binary operation on A and 
   -- a binary operation on B
   -- to a binary operation on (∃ R)
   ∃inj₂ : ∀ {_∙₁_ : A → A → A} {_∙₂_ : B → B → B} 
          → (_∙′_ : R₂ _∙₁_ _∙₂_) 
          → (∃₂ R → ∃₂ R → ∃₂ R)
   ∃inj₂ _∙′_ (inj x) (inj y) = inj (x ∙′ y)

   prop₁ : ∀ {pᴬ pᴮ} {Pᴬ : Pred A pᴬ} {Pᴮ : Pred B pᴮ}
          → (∀ a → Pᴬ a)
          → (∀ b → Pᴮ b) 
          → ∀ ((a , b , _) : ∃₂ R)
          → Pᴬ a × Pᴮ b
   prop₁ Pᴬ Pᴮ (a , b , _) = Pᴬ a , Pᴮ b

   prop₂ : ∀ {pᴬ pᴮ} {Rᴬ : Rel A pᴬ} {Rᴮ : Rel B pᴮ} 
         → (∀ a₁ a₂ → Rᴬ a₁ a₂)
         → (∀ b₁ b₂ → Rᴮ b₁ b₂)
         → ∀ ((a₁ , b₁ , _) (a₂ , b₂ , _) : ∃₂ R)
         → Rᴬ a₁ a₂ × Rᴮ b₁ b₂
   prop₂ Rᴬ Rᴮ (a₁ , b₁ , _) (a₂ , b₂ , _) = Rᴬ a₁ a₂ , Rᴮ b₁ b₂

   prop₃ : ∀ {pᴬ pᴮ} {Sᴬ :  A → A → A → Set pᴬ} {Sᴮ : B → B → B → Set pᴮ}
          → (∀ a₁ a₂ a₃ → Sᴬ a₁ a₂ a₃)
          → (∀ b₁ b₂ b₃ → Sᴮ b₁ b₂ b₃)  
          → ∀ ((a₁ , b₁ , _) (a₂ , b₂ , _) (a₃ , b₃ , _) : ∃₂ R) 
          → Sᴬ a₁ a₂ a₃ × Sᴮ b₁ b₂ b₃
   prop₃ Sᴬ Sᴮ (a₁ , b₁ , _) (a₂ , b₂ , _) (a₃ , b₃ , _) = 
       Sᴬ a₁ a₂ a₃ 
     , Sᴮ b₁ b₂ b₃ 
```
