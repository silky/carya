---
title: Algebra on Relations
---

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level ; suc ; _⊔_)

module Algebra.Instances.Relation {a b ℓ : Level} (A : Set a) (B : Set b) where

open import Algebra.Bundles
open import Algebra.Construct.Function {c = a ⊔ suc ℓ} {ℓ = a ⊔ ℓ} B as F using ()
open import Algebra.Instances.Predicate {ℓ = ℓ} A as P using ()

```

## Bundles on `_∪_`

Where:

```
-- _∪_ : REL B A → REL B A → REL B A 
-- R₁ ∪ R₂ = λ b a → R₁ b a ⊎ R₂ b a 
```

```agda
∪-magma : Magma (b ⊔ a ⊔ suc ℓ) (b ⊔ a ⊔ ℓ) 
∪-magma = F.magma P.∪-magma

∪-semigroup : Semigroup (b ⊔ a ⊔ suc ℓ) (b ⊔ a ⊔ ℓ) 
∪-semigroup = F.semigroup P.∪-semigroup

∪-monoid : Monoid (b ⊔ a ⊔ suc ℓ) (b ⊔ a ⊔ ℓ)  
∪-monoid = F.monoid P.∪-monoid

∪-commutativeMonoid : CommutativeMonoid (b ⊔ a ⊔ suc ℓ) (b ⊔ a ⊔ ℓ) 
∪-commutativeMonoid = F.commutativeMonoid P.∪-commutativeMonoid
```

## Bundles on `_∩_`


Where: 

```
-- _∩_ : REL B A → REL B A → REL B A 
-- R₁ ∩ R₂ = λ a b → R₁ b a × R₂ b a
```

```agda
∩-magma : Magma (b ⊔ a ⊔ suc ℓ) (b ⊔ a ⊔ ℓ) 
∩-magma = F.magma P.∩-magma

∩-semigroup : Semigroup (b ⊔ a ⊔ suc ℓ) (b ⊔ a ⊔ ℓ) 
∩-semigroup = F.semigroup P.∩-semigroup

∩-monoid : Monoid (b ⊔ a ⊔ suc ℓ) (b ⊔ a ⊔ ℓ) 
∩-monoid = F.monoid P.∩-monoid

∩-commutativeMonoid : CommutativeMonoid (b ⊔ a ⊔ suc ℓ) (b ⊔ a ⊔ ℓ) 
∩-commutativeMonoid = F.commutativeMonoid P.∩-commutativeMonoid
```

## Bundles on `_∪_` and `_∩_`

```agda
∩-∪-semiring : Semiring (b ⊔ a ⊔ suc ℓ) (b ⊔ a ⊔ ℓ) 
∩-∪-semiring = F.semiring P.∩-∪-semiring

∩-∪-commutativeSemiring : CommutativeSemiring (b ⊔ a ⊔ suc ℓ) (b ⊔ a ⊔ ℓ) 
∩-∪-commutativeSemiring = F.commutativeSemiring P.∩-∪-commutativeSemiring
```
