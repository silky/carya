---
title: Algebra on Predicates
---

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level ; suc; _⊔_)

module Algebra.Instances.Predicate {a ℓ : Level} (A : Set a) where

open import Algebra.Construct.Function {c = suc ℓ} {ℓ = ℓ} A as F using ()
open import Algebra.Bundles
open import Data.Product.Algebra as ×
open import Data.Sum.Algebra as ⊎
```

## Bundles on `_∪_`

```agda
∪-magma : Magma (a ⊔ suc ℓ) (a ⊔ ℓ)
∪-magma = F.magma (⊎-magma ℓ)

∪-semigroup : Semigroup( a ⊔ suc ℓ) (a ⊔ ℓ)
∪-semigroup = F.semigroup (⊎-semigroup ℓ)

∪-monoid : Monoid (a ⊔ suc ℓ) (a ⊔ ℓ) 
∪-monoid = F.monoid (⊎-monoid ℓ)

∪-commutativeMonoid : CommutativeMonoid (a ⊔ suc ℓ) (a ⊔ ℓ)
∪-commutativeMonoid = F.commutativeMonoid (⊎-commutativeMonoid ℓ)
```

## Bundles on `_∩_`

```agda
∩-magma : Magma (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-magma = F.magma (×-magma ℓ)

∩-semigroup : Semigroup (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-semigroup = F.semigroup (×-semigroup ℓ)

∩-monoid : Monoid (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-monoid = F.monoid (×-monoid ℓ)

∩-commutativeMonoid : CommutativeMonoid (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-commutativeMonoid = F.commutativeMonoid (×-commutativeMonoid ℓ)
```

## Bundles on `_∪_` and `_∩_`

```agda
-- Semiring bundle not ℓurrently defined in Data.Product.Algebra ¯\_(ツ)_/¯
∩-∪-semiring : Semiring (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-∪-semiring = F.semiring  (record { isSemiring = ⊎-×-isSemiring ℓ }) 

∩-∪-commutativeSemiring : CommutativeSemiring (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-∪-commutativeSemiring = F.commutativeSemiring (×-⊎-commutativeSemiring ℓ)
```
