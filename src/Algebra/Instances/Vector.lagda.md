---
title: Algebra Bundles for `Vector A n`s 
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level)
open import Data.Nat using (ℕ)

module Algebra.Instances.Vector {c ℓ : Level} (n : ℕ) where

open import Data.Fin using (Fin)
```

</details>

```agda
open import Algebra.Construct.Function {c = c} {ℓ = ℓ} (Fin n) public
```

## Examples 

See the
[`Algebra.Instances.Vector.Examples`](src/Algebra/Instances/Vector/Examples.lagda.md)
module for examples.
