---
title: Module instances of `Vector A n`
---

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level)
open import Data.Nat using (ℕ)

module Algebra.Module.Instances.Vector {c ℓ} (n : ℕ) where

open import Data.Fin using (Fin)
```

```agda
open import Algebra.Module.Construct.Function {r = c} {ℓʳ = ℓ} (Fin n) public
  renaming (leftSemimodule to vector)
```
