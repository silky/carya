---
title: Inner Product
---

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level; _⊔_ ; suc ; 0ℓ)
open import Algebra.Bundles using (Semiring)

module Algebra.Module.InnerProduct {r ℓʳ} {R : Semiring r ℓʳ} where

open import Algebra.Core using (Op₂)
open import Algebra.Definitions using (Involutive)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Structures using (IsSemiring)
open import Data.Product using (∃; _×_; proj₁ ; _,_)
open import Function using (flip)
open import Relation.Binary.Bundles using (Setoid; Poset)
open import Relation.Binary using (Rel)
open import Relation.Binary.Structures using (IsPartialOrder)
```

## Inner Product 

Mostly using the definition
from [nlab](https://ncatlab.org/nlab/show/inner+product+space).

```agda
record InnerProduct {a ℓᵃ} (M : LeftSemimodule R a ℓᵃ) : Set (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) where
    
    open Semiring R renaming (Carrier to C; _≈_ to _≈ᶜ_)
    open LeftSemimodule M renaming (Carrierᴹ to V)

    field
      ⟨_,_⟩ : V → V → C
      _ᵒ : C → C
      .involutive : Involutive _≈ᶜ_ _ᵒ
      .zero : ∀ x → ⟨ 0ᴹ , x ⟩ ≈ᶜ 0#
                  × ⟨ x , 0ᴹ ⟩ ≈ᶜ 0#
      .additive : ∀ x y z → ⟨ x +ᴹ y , z ⟩ ≈ᶜ ⟨ x , z ⟩ + ⟨ y , z ⟩ 
                          × ⟨ x , y +ᴹ z ⟩ ≈ᶜ ⟨ x , y ⟩ + ⟨ x , z ⟩
      .scalable : ∀ c → ∀ x y → ⟨ c *ₗ x , y ⟩ ≈ᶜ c * ⟨ x , y ⟩
                              × ⟨ x , c *ₗ y ⟩ ≈ᶜ ⟨ x , y ⟩ * c
      .conjugateSymmetric : ∀ x y → ⟨ x , y ⟩ ≈ᶜ ⟨ y , x ⟩ ᵒ

    -- norm
    ∥_∥² : V → C
    ∥ x ∥² = ⟨ x , x ⟩
```

## Definiteness

```agda
module _ {a ℓᵃ} {M : LeftSemimodule R a ℓᵃ} where

  open Semiring R renaming (Carrier to C; _≈_ to _≈ᶜ_)
  open LeftSemimodule M renaming (Carrierᴹ to V)
  
  SemiDefinite : InnerProduct M → Set _
  SemiDefinite i = ∀ (x y : V) → ∥ x ∥² ≈ᶜ 0# → ∥ y ∥² ≈ᶜ 0# → ∥ x +ᴹ y ∥² ≈ᶜ 0#
    where open InnerProduct i

  NonDegenerate : InnerProduct M → Set _
  NonDegenerate i = ∀ (x y : V) → ⟨ x , y ⟩ ≈ᶜ 0# → x ≈ᴹ 0ᴹ
    where open InnerProduct i

  Definite : InnerProduct M → Set _
  Definite i = ∀ (x : V) → ∥ x ∥² ≈ᶜ 0# → x ≈ᴹ 0ᴹ
    where open InnerProduct i
```

-- ```agda
-- record ≤-Semiring (a ℓ₁ ℓ₂ : Level) : Set (suc (a ⊔ ℓ₁ ⊔ ℓ₂)) where 
--    field
--      Carrier : Set a
--      _≈_ : Rel Carrier ℓ₁
--      _+_ : Op₂ Carrier
--      _*_ : Op₂ Carrier
--      0#  : Carrier
--      1#  : Carrier
--      _≤_  : Rel Carrier ℓ₂
--      isPartialOrder : IsPartialOrder _≈_ _≤_
--      isSemiring : IsSemiring _≈_ _+_ _*_ 0# 1#

--    semiring : Semiring a ℓ₁ 
--    semiring = record { isSemiring = isSemiring }

--    poset : Poset a ℓ₁ ℓ₂
--    poset = record { isPartialOrder = isPartialOrder }
  
-- module _ {a r ℓ₁ ℓ₂ ℓ₃} 
--     {A : Setoid a ℓ₁} 
--     {R : ≤-Semiring r ℓ₂ ℓ₃}
--     where

--   open ≤-Semiring R renaming (Carrier to C; _≈_ to _≈ᶜ_)
--   open SemiringFunction A semiring
--   open LeftSemimodule leftSemimodule renaming (Carrierᴹ to V)

--   Positive : InnerProduct A semiring → Set _
--   Positive i = ∀ (x : V) → 0# ≤ ∥ x ∥²
--      where open InnerProduct i

--   PositiveDefinite : InnerProduct A semiring → Set _
--   PositiveDefinite i = Positive i × Definite i
-- ```
