---
title: LeftSemimodule of Monomial terms
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_; 0ℓ)
open import Algebra.Bundles using (CommutativeSemiring)

module Algebra.Module.Construct.Monomial 
  {r ℓʳ : Level} (R : CommutativeSemiring r ℓʳ) where 

open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Properties.Semiring.Exp 
  (CommutativeSemiring.semiring R)
open import Algebra.Properties.CommutativeSemigroup 
  (CommutativeSemiring.*-commutativeSemigroup R)
open import Data.Nat using (ℕ) renaming (_*_ to _ℕ*_)
open import Data.Nat.Properties 
  using (+-*-semiring)
  renaming (*-comm to ℕ*-comm)
open import Function using (flip)

open CommutativeSemiring R
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning
open ≈-Reasoning setoid
```

</details>

Create a `LeftSemimodule` from a `CommutativeSemiring`
over the semiring of Natural numbers, 
thus giving the resulting semimodule the semantics of
[monomials](https://en.wikipedia.org/wiki/Monomial).
See also [monomial basis](https://en.wikipedia.org/wiki/Monomial_basis).

```agda
1^n≈1 : ∀ (n : ℕ) → 1# ^ n ≈ 1#
1^n≈1 ℕ.zero = refl
1^n≈1 (ℕ.suc n) = trans (*-identityˡ (1# ^ n)) (1^n≈1 n)

^-comm : ∀ (x : Carrier) → (n₁ n₂ : ℕ) → (x ^ n₁) ^ n₂ ≈ (x ^ n₂) ^ n₁
^-comm x n₁ n₂ =
  begin
    (x ^ n₁) ^ n₂
  ≈⟨ ^-assocʳ x n₁ n₂ ⟩
    x ^ (n₁ ℕ* n₂)
  ≈⟨ ^-congʳ x (ℕ*-comm n₁ n₂) ⟩
    x ^ (n₂ ℕ* n₁)
  ≈⟨ sym (^-assocʳ x n₂  n₁) ⟩
    (x ^ n₂) ^  n₁
  ∎

^-distrib-* : ∀ (x₁ x₂ : Carrier) → (n : ℕ) → (x₁ * x₂) ^ n ≈ x₁ ^ n * x₂ ^ n
^-distrib-* x₁ x₂ ℕ.zero = sym (*-identityˡ 1#)
^-distrib-* x₁ x₂ (ℕ.suc n) =
  begin
    (x₁ * x₂) ^ ℕ.suc n
  ≈⟨ ^-homo-* (x₁ * x₂) 1 n ⟩
    (x₁ * x₂) ^ 1 * ((x₁ * x₂) ^ n)
  ≈⟨ *-congˡ (^-distrib-* x₁ x₂ n) ⟩
    (x₁ * x₂) ^ 1 * (x₁ ^ n * x₂ ^ n)
  ≈⟨ *-congʳ (*-identityʳ (x₁ * x₂)) ⟩
    (x₁ * x₂) * (x₁ ^ n * x₂ ^ n)
  ≈⟨ interchange x₁ x₂ (x₁ ^ n) (x₂ ^ n) ⟩
   (x₁ * x₁ ^ n) * (x₂ * x₂ ^ n)
  ≈⟨ *-cong (*-congʳ (sym (*-identityʳ x₁))) (*-congʳ (sym (*-identityʳ x₂))) ⟩
    (x₁ ^ 1 * x₁ ^ n) * (x₂ ^ 1 * x₂ ^ n)
  ≈⟨ *-cong (sym (^-homo-* x₁ 1 n)) ((sym (^-homo-* x₂ 1 n))) ⟩
    x₁ ^ ℕ.suc n * x₂ ^ ℕ.suc n
  ∎

leftSemimodule : LeftSemimodule +-*-semiring r ℓʳ
leftSemimodule = record
  { _*ₗ_ = flip _^_
  ; isLeftSemimodule = record 
    { +ᴹ-isCommutativeMonoid = *-isCommutativeMonoid 
    ; isPreleftSemimodule = record
        { *ₗ-cong = flip ^-cong 
        ; *ₗ-zeroˡ = λ _ → refl
        ; *ₗ-distribʳ = ^-homo-*
        ; *ₗ-identityˡ = *-identityʳ
        ; *ₗ-assoc = λ n₁ n₂ x → trans (sym (^-assocʳ x n₁ n₂)) (^-comm x n₁ n₂)
        ; *ₗ-zeroʳ = λ n → 1^n≈1 n
        ; *ₗ-distribˡ = λ n x₁ x₂ → ^-distrib-* x₁ x₂ n
        }
    }
  }
```
 