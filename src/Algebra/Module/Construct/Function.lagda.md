---
title: Lifting (Module) Algebraic Structures to functions to Algebraic Structures
bibliography: resources/references.bib
header-includes: 
  - <link rel="stylesheet" href="../resources/style.css">
---

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_; 0ℓ)
open import Algebra.Bundles using (Semiring; CommutativeMonoid)

module Algebra.Module.Construct.Function 
  {a r ℓʳ : Level} (A : Set a) (R : Semiring r ℓʳ) where 

open import Algebra.Construct.Function using (commutativeMonoid; semiring)
open import Algebra.Module.Bundles using (LeftSemimodule)
```

```agda
leftSemimodule : ∀ {b ℓᵇ} 
   → LeftSemimodule R b ℓᵇ
   → LeftSemimodule R (a ⊔ b) (a ⊔ ℓᵇ)
leftSemimodule M = record 
  { isLeftSemimodule = record 
     { +ᴹ-isCommutativeMonoid = 
          CommutativeMonoid.isCommutativeMonoid 
            (commutativeMonoid A +ᴹ-commutativeMonoid)
     ; isPreleftSemimodule = record
          { *ₗ-cong = λ x fx≈ᴹgx → *ₗ-cong x fx≈ᴹgx
          ; *ₗ-zeroˡ = λ f → λ {x} → *ₗ-zeroˡ (f x)
          ; *ₗ-distribʳ = λ f m n → λ {x} → *ₗ-distribʳ (f x) m n
          ; *ₗ-identityˡ = λ f → λ {x} → *ₗ-identityˡ (f x)
          ; *ₗ-assoc = λ m n f → λ {x} → *ₗ-assoc m n (f x)
          ; *ₗ-zeroʳ = λ m → *ₗ-zeroʳ m
          ; *ₗ-distribˡ = λ m f g → λ {x} → *ₗ-distribˡ m (f x) (g x)
          } 
     }
 }
 where open LeftSemimodule M
```
