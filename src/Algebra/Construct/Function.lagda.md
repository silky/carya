---
title: Lifting Algebraic Structures to functions to Algebraic Structures
bibliography: resources/references.bib
header-includes: 
  - <link rel="stylesheet" href="../resources/style.css">
---

```agda
{-# OPTIONS --cubical-compatible --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_; 0ℓ)

module Algebra.Construct.Function {a c ℓ : Level} (A : Set a) where 

open import Algebra.Bundles
open import Data.Product using (_,_ ; proj₁ ; proj₂)
```

```agda
magma : Magma c ℓ → Magma (a ⊔ c) (a ⊔ ℓ)
magma M = record
  {  Carrier = A → Carrier
  ; _≈_ = λ f g →  ∀ {x : A} → f x ≈ g x
  ; _∙_ = λ f g x → f x ∙ g x 
  ; isMagma = record { 
       isEquivalence = record { 
            refl = λ {f} {x} → refl {x = f x} 
          ; sym = λ x → sym x
          ; trans = λ gx≈hx fx≈gx → trans gx≈hx fx≈gx } 
       ; ∙-cong = λ x y → ∙-cong x y } 
 }
 where open Magma M

semigroup : Semigroup c ℓ → Semigroup (a ⊔ c) (a ⊔ ℓ)
semigroup S = record
   { isSemigroup = record { 
        isMagma = isMagma 
      ; assoc = λ f g h → λ {x} → Semigroup.assoc S (f x) (g x) (h x) 
      } 
   }
   where open Magma (magma (Semigroup.magma S))

monoid : Monoid c ℓ → Monoid (a ⊔ c) (a ⊔ ℓ)
monoid M = record 
  { isMonoid = record { 
          isSemigroup = isSemigroup
        ; identity = (λ f → λ {x} → proj₁ identity (f x)) 
                    , λ f → λ {x} → proj₂ identity (f x) 
        } 
   }
   where open Semigroup (semigroup (Monoid.semigroup M))
         identity = Monoid.identity M

commutativeMonoid : CommutativeMonoid c ℓ → CommutativeMonoid (a ⊔ c) (a ⊔ ℓ)
commutativeMonoid CM = record
   { isCommutativeMonoid = record { 
          isMonoid = isMonoid 
        ; comm = λ f g → λ {x} → CommutativeMonoid.comm CM (f x) (g x) }
  }
  where open Monoid (monoid (CommutativeMonoid.monoid CM))

idempotentCommutativeMonoid : IdempotentCommutativeMonoid c ℓ 
    → IdempotentCommutativeMonoid (a ⊔ c) (a ⊔ ℓ)
idempotentCommutativeMonoid ICM = record
   { isIdempotentCommutativeMonoid = record 
        { isCommutativeMonoid = isCommutativeMonoid 
        ; idem = λ f {x} → idem (f x) 
        }
  }
  where open CommutativeMonoid 
               (commutativeMonoid 
                  (IdempotentCommutativeMonoid.commutativeMonoid ICM))
        open IdempotentCommutativeMonoid ICM using (idem)

semiringWithoutAnnihilatingZero : 
    SemiringWithoutAnnihilatingZero c ℓ 
  → SemiringWithoutAnnihilatingZero (a ⊔ c) (a ⊔ ℓ)
semiringWithoutAnnihilatingZero S = record
   { isSemiringWithoutAnnihilatingZero = record
        { +-isCommutativeMonoid = CommutativeMonoid.isCommutativeMonoid M+
        ; *-cong = Monoid.∙-cong M*
        ; *-assoc = Monoid.assoc M*
        ; *-identity = Monoid.identity M*
        ; distrib = (λ f g h → λ {x} → proj₁ distrib (f x) (g x) (h x)) 
                   , λ f g h → λ {x} → proj₂ distrib (f x) (g x) (h x)
        }
   }
   where open SemiringWithoutAnnihilatingZero S
         M+ = commutativeMonoid +-commutativeMonoid
         M* = monoid *-monoid

semiring : Semiring c ℓ → Semiring (a ⊔ c) (a ⊔ ℓ)
semiring S = record
  { isSemiring = record { 
         isSemiringWithoutAnnihilatingZero = 
            isSemiringWithoutAnnihilatingZero
       ; zero = (λ f → λ {x} → proj₁ zero (f x)) 
               , λ f → λ {x} → proj₂ zero (f x) 
               }
  }
  where open SemiringWithoutAnnihilatingZero (
              semiringWithoutAnnihilatingZero 
                (Semiring.semiringWithoutAnnihilatingZero S))
        zero = Semiring.zero S

commutativeSemiring : CommutativeSemiring c ℓ 
                    → CommutativeSemiring (a ⊔ c) (a ⊔ ℓ)
commutativeSemiring S = record
  { isCommutativeSemiring = record { 
       isSemiring = isSemiring 
      ; *-comm = λ f g {x} → CommutativeSemiring.*-comm S (f x) (g x)
      }
  }
  where open Semiring (semiring (CommutativeSemiring.semiring S))
```
