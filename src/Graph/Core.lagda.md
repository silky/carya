---
title: Graph
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-} 

module Graph.Core where

open import Data.Product as × using (∃ ; _,_)
open import Function using (_↔_ ; Inverse; _-⟨_⟩-_ ; id; _∘_)
open import Function.Properties.Inverse using (↔-sym ; ↔-refl; ↔-trans)
open import Function.Properties.Inverse.HalfAdjointEquivalence 
  using (↔⇒≃) renaming (_≃_ to ≃ʰ)
open import Relation.Binary 
  using (Reflexive ; Symmetric; Transitive; IsEquivalence)
open import Relation.Binary.PropositionalEquality using (sym ; subst₂)

open import Relation.Core using (_∼_ ; _≈_)
```

</details>

```agda
Graph : Set₁
Graph = ∃ λ (Node : Set) → Node ∼ Node

-- vertices
∣_∣ : Graph → Set
∣_∣ = ×.proj₁

vertices = ∣_∣

-- edges
⟦_⟧ : ( (A , _) : Graph) → (A ∼ A)
⟦_⟧ = ×.proj₂

edges = ⟦_⟧
```

## Equivalence of `Graph`s (Graph Isomorphism)

See the definition of 
[graph isomorphism](https://en.wikipedia.org/wiki/Graph_isomorphism).
NOTE: 
following the definition specifies an isomorphism of vertex sets,
rather than a bijection.
This is because `Bijection` is two steps removed from 
[`HalfAdjointEquivalence`](https://agda.github.io/agda-stdlib/master/Function.Properties.Inverse.HalfAdjointEquivalence.html#696),
which makes the substitutions simpler in the proof of symmetry.
However, one could use 
[`Bijection⇒Inverse`](https://agda.github.io/agda-stdlib/Function.Properties.Bijection.html#1898)
to get to `HalfAdjointEquivalence` from a `Bijection`.

```agda
infix 4 _≃_ 
_≃_ : Graph → Graph → Set _
(A , Rᴬ) ≃ (B , Rᴮ) = ∃ λ (f : A ↔ B) → Rᴬ ≈ (to f -⟨ Rᴮ ⟩- to f)
  where open Inverse

≃-refl : Reflexive _≃_
≃-refl = ↔-refl , id , id

≃-sym : Symmetric _≃_
≃-sym {y = (_ , Rᴮ)} (f , prf) =
    ↔-sym f
   , λ {a = b₁} {b = b₂} → 
        (×.proj₂ prf ∘
              (subst₂ Rᴮ 
                (sym (≃ʰ.right-inverse-of (↔⇒≃ f) b₁)) 
                (sym (≃ʰ.right-inverse-of (↔⇒≃ f) b₂)))) 
      , subst₂ Rᴮ 
            (≃ʰ.right-inverse-of (↔⇒≃ f) b₁) 
            (≃ʰ.right-inverse-of (↔⇒≃ f) b₂) ∘ (×.proj₁ prf)

≃-trans : Transitive _≃_
≃-trans  (f , prfᶠ) (g , prfᵍ) = 
    ↔-trans f g 
  , ×.proj₁ prfᵍ ∘ ×.proj₁ prfᶠ
  , ×.proj₂ prfᶠ ∘ ×.proj₂ prfᵍ

≃-isEquivalence : IsEquivalence _≃_
≃-isEquivalence = record 
  { refl = ≃-refl
  ; sym =  ≃-sym
  ; trans = λ {i j k} → ≃-trans {i} {j} {k}
  }
```
 