---
title: Graph Algebra
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-} 

module Graph.Algebra where

open import Algebra.Bundles
open import Algebra.Definitions
open import Algebra.Structures
open import Data.Product as ×
open import Data.Product.Algebra as ×
open import Data.Product.Properties as ×
open import Data.Sum as ⊎
open import Data.Sum.Algebra as ⊎
open import Data.Sum.Properties as ⊎
open import Data.Unit.Polymorphic using (tt ; ⊤)
open import Level using (0ℓ ; suc)
open import Function 
  using (Inverse ; Injection) 
  renaming (id to id′ ; _∘_ to _∘′_)
open import Function.Properties.Inverse using (Inverse⇒Injection)
open import Relation.Binary.PropositionalEquality using (cong; sym; trans; _≡_)

open import Graph.Core
open import Graph.Base
```

</details>

## Properties of `_⊕_`

```agda
⊕-cong : Congruent₂ _≃_ _⊕_
⊕-cong {A , Rᴬ} {B , Rᴮ} {C , Rᶜ} {D , Rᴰ} (f , ff) (g , gg) =
    ⊎-cong f g
    , λ { {(⊎.inj₁ _) } {(⊎.inj₁ _)} → ff
        ; {(⊎.inj₂ _) } {(⊎.inj₁ _)} → (λ ()) , λ ()
        ; {(⊎.inj₁ _) } {(⊎.inj₂ _)} → (λ ()) , λ ()
        ; {(⊎.inj₂ _) } {(⊎.inj₂ _)} → gg
        }

⊕-assoc : Associative _≃_ _⊕_
⊕-assoc (A , Rᴬ) (B , Rᴮ) (C , Rᶜ) =
    (⊎-assoc 0ℓ A B C)
    , λ { {(⊎.inj₁ (⊎.inj₁ _)) } {(⊎.inj₁ (⊎.inj₁ _))} → id′ , id′
        ; {(⊎.inj₁ (⊎.inj₁ _)) } {(⊎.inj₁ (⊎.inj₂ _))} → id′ , id′ 
        ; {(⊎.inj₁ (⊎.inj₂ _)) } {(⊎.inj₁ (⊎.inj₁ _))} → id′ , id′
        ; {(⊎.inj₁ (⊎.inj₂ _)) } {(⊎.inj₁ (⊎.inj₂ _))} → id′ , id′
        ; {(⊎.inj₁ (⊎.inj₂ _)) } {(⊎.inj₂ _)} → id′ , id′ 
        ; {(⊎.inj₂ _) } {(⊎.inj₁ (⊎.inj₁ _))} → (λ ()) , (λ ()) 
        ; {(⊎.inj₂ _) } {(⊎.inj₁ (⊎.inj₂ _))} → (λ ()) , (λ ()) 
        ; {(⊎.inj₁ (⊎.inj₁ _)) } {(⊎.inj₂ _)} → (λ ()) , (λ ())
        ; {(⊎.inj₂ _) } {(⊎.inj₂ _)} → id′ , id′
        }

𝟘-⊕-identityˡ : LeftIdentity _≃_ 𝟘 _⊕_
𝟘-⊕-identityˡ (A , Rᴬ) = 
    (⊎-identityˡ 0ℓ A)
  , λ { {⊎.inj₂ _} {⊎.inj₂ _} → id′ , id′ }

𝟘-⊕-identityʳ : RightIdentity _≃_ 𝟘 _⊕_
𝟘-⊕-identityʳ (A , Rᴬ) = 
    (⊎-identityʳ 0ℓ A)
  , λ { {⊎.inj₁ _} {⊎.inj₁ _} → id′ , id′ }

⊕-comm : Commutative _≃_ _⊕_
⊕-comm (A , Rᴬ) (B , Rᴮ) =
  (⊎-comm A B)
  , (λ { {⊎.inj₁ _} {⊎.inj₁ _} → id′ , id′
       ; {⊎.inj₁ _} {⊎.inj₂ _} → id′ , id′
       ; {⊎.inj₂ _} {⊎.inj₁ _} → id′ , id′
       ; {⊎.inj₂ _} {⊎.inj₂ _} → id′ , id′ 
      } )
```

## Properties of `_⊗_`

```agda
⊗-cong : Congruent₂ _≃_ _⊗_
⊗-cong (f , ff) (g , gg) =
    (×-cong f g)
     , ×.map (×.proj₁ ff) (×.proj₁ gg) 
     , ×.map (×.proj₂ ff) (×.proj₂ gg) 

⊗-assoc : Associative _≃_ _⊗_
⊗-assoc (A , Rᴬ) (B , Rᴮ) (C , Rᶜ) =
    (×-assoc 0ℓ A B C)
  , ×.assocʳ′
  , ×.assocˡ′

𝟙-⊗-identityˡ : LeftIdentity _≃_ 𝟙 _⊗_
𝟙-⊗-identityˡ (A , Rᴬ) = 
    (×-identityˡ 0ℓ A)
  , ×.proj₂ 
  , (tt ,_)

𝟙-⊗-identityʳ : RightIdentity _≃_ 𝟙 _⊗_
𝟙-⊗-identityʳ (A , Rᴬ) = 
    (×-identityʳ 0ℓ A)
    , ×.proj₁ 
    , (_, tt)

⊗-comm : Commutative _≃_ _⊗_
⊗-comm (A , Rᴬ) (B , Rᴮ) = 
    (×-comm A B)
  , ×.swap 
  , ×.swap

𝟘-⊗-zeroˡ : LeftZero _≃_ 𝟘 _⊗_
𝟘-⊗-zeroˡ (A , Rᴬ) = 
    (×-zeroˡ 0ℓ A)
  , (λ { (() , _) }) 
  , λ ()

𝟘-⊗-zeroʳ : RightZero _≃_ 𝟘 _⊗_
𝟘-⊗-zeroʳ (A , Rᴬ) =
   (×-zeroʳ 0ℓ A)
  , (λ ()) 
  , (λ ())

𝟘-⊗-zero : Zero _≃_ 𝟘 _⊗_
𝟘-⊗-zero = 𝟘-⊗-zeroˡ , 𝟘-⊗-zeroʳ
```
## Properties of `_□_`

```agda
□-cong : Congruent₂ _≃_ _□_
□-cong {A , Rᴬ} {B , Rᴮ} {C , Rᶜ} {D , Rᴰ} (f , ff) (g , gg) = 
      (×-cong f g)
    , ⊎.map 
        (×.map (cong (to f)) (×.proj₁ gg)) 
        (×.map (×.proj₁ ff) (cong (to g)))
    , ⊎.map 
        (×.map (Injection.injective (Inverse⇒Injection f)) (×.proj₂ gg))
        (×.map (×.proj₂ ff) (Injection.injective (Inverse⇒Injection g)))
    where open Inverse

□-assoc : Associative _≃_ _□_
□-assoc A B C =
    ×.proj₁ (⊗-assoc A B C)
  ,(λ { (⊎.inj₁ (x , y)) → 
          ⊎.inj₁ (cong ×.proj₁ x , (⊎.inj₁ (cong ×.proj₂ x , y))) 
      ; (⊎.inj₂ (⊎.inj₁ (x , y) , z)) → ⊎.inj₁ (x , ⊎.inj₂ (y , z))
      ; (⊎.inj₂ (⊎.inj₂ (x , y) , z)) → ⊎.inj₂ (x , ×-≡,≡→≡  (y , z))
      }) 
 , λ  { (⊎.inj₁ (x , ⊎.inj₁ (y , z))) → ⊎.inj₁ (×-≡,≡→≡ (x , y) , z)
      ; (⊎.inj₁ (x , ⊎.inj₂ (y , z))) → ⊎.inj₂ (⊎.inj₁ (x , y) , z)
      ; (⊎.inj₂ (x , y)) → 
          ⊎.inj₂ ((⊎.inj₂ (x , (×.proj₁ (×-≡,≡←≡ y)))) , (×.proj₂ (×-≡,≡←≡ y)))
      }

□-comm : Commutative _≃_ _□_
□-comm (A , Rᴬ) (B , Rᴮ) =
    ×-comm A B
  , (λ { (⊎.inj₁ x) → ⊎.inj₂ (×.swap x)
       ; (⊎.inj₂ x) → ⊎.inj₁ (×.swap x) 
       })
  , (λ { (⊎.inj₁ x) → ⊎.inj₂ (×.swap x)
       ; (⊎.inj₂ x) → ⊎.inj₁ (×.swap x) 
       })

𝟘-□-zeroˡ : LeftZero _≃_ 𝟘 _□_
𝟘-□-zeroˡ (A , Rᴬ) =
  (×-zeroˡ 0ℓ A)
  , λ { {_} {(() , _ )} }

𝟘-□-zeroʳ : RightZero _≃_ 𝟘 _□_
𝟘-□-zeroʳ (A , Rᴬ) =
   (×-zeroʳ 0ℓ A)
  , λ { {_} {(_ , ())} }
```

## Properties of `_⊙_`

```agda
⊙-cong : Congruent₂ _≃_ _⊙_
⊙-cong (f , ff) (g , gg) = 
      (×-cong f g) 
    , ⊎.map 
        (×.map₁ (×.proj₁ ff)) 
        (×.map (cong (to f)) (×.proj₁ gg)) 
    , ⊎.map 
        (×.map₁ (×.proj₂ ff)) 
        (×.map (Injection.injective (Inverse⇒Injection f)) (×.proj₂ gg))
  where open Inverse

⊙-assoc : Associative _≃_ _⊙_
⊙-assoc A B C =
    ×.proj₁ (⊗-assoc A B C)
  , (λ { (⊎.inj₁ (⊎.inj₁ (x , _) , _)) → ⊎.inj₁ (x , tt)
       ; (⊎.inj₁ (⊎.inj₂ (x , y) , _)) → ⊎.inj₂ (x , ⊎.inj₁ (y , tt))
       ; (⊎.inj₂ (x , y)) → 
              ⊎.inj₂ ((cong ×.proj₁ x) , (⊎.inj₂ (cong ×.proj₂ x , y)))
       }) 
 , λ  { (⊎.inj₁ x) → ⊎.inj₁ (⊎.inj₁ x , tt)
      ; (⊎.inj₂ (x , (⊎.inj₁ (y , _)))) → ⊎.inj₁ (⊎.inj₂ ((x , y)) , tt)
      ; (⊎.inj₂ (x , (⊎.inj₂ (y , z)))) → ⊎.inj₂ (×-≡,≡→≡ (x , y) , z)
      }

-- id-⊙-identityˡ : LeftIdentity _≃_ id _⊙_
-- id-⊙-identityˡ (A , Rᴬ) = 
--     (×-identityˡ 0ℓ A)
--   , (λ { { (x , y) } {(u , v)} → 
--        (λ { (⊎.inj₁ _≡_.refl) → {!   !} --- hmm, nope
--           ; (⊎.inj₂ (_ , x)) → x }) 
--      , λ Rxy → ⊎.inj₂ (_≡_.refl , Rxy) })

𝟘-⊙-zeroˡ : LeftZero _≃_ 𝟘 _⊙_
𝟘-⊙-zeroˡ (A , Rᴬ) =
  (×-zeroˡ 0ℓ A)
  , λ { {_} {(() , _ )} }

𝟘-⊙-zeroʳ : RightZero _≃_ 𝟘 _⊙_
𝟘-⊙-zeroʳ (A , Rᴬ) =
   (×-zeroʳ 0ℓ A)
  , λ { {_} {(_ , ())} }
```

## Properties of `_⊕_` and `_⊗_`

```agda
⊗-distribˡ-⊕ : (_≃_ DistributesOverˡ _⊗_) _⊕_
⊗-distribˡ-⊕ (A , Rᴬ) (B , Rᴮ) (C , Rᶜ) =
     (×-distribˡ-⊎ 0ℓ A B C)
   , λ { {_ , ⊎.inj₁ _}  {_ , ⊎.inj₁ _} → id′ , id′
       ; {_ , ⊎.inj₁ _}  {_ , ⊎.inj₂ _} → (λ { (_ , ()) }) , λ ()
       ; {_ , ⊎.inj₂ _}  {_ , ⊎.inj₁ _} → (λ { (_ , ()) }) , λ ()
       ; {_ , ⊎.inj₂ _}  {_ , ⊎.inj₂ _} → id′ , id′ 
       }

⊗-distribʳ-⊕ : (_≃_ DistributesOverʳ _⊗_) _⊕_
⊗-distribʳ-⊕ (A , Rᴬ) (B , Rᴮ) (C , Rᶜ) = 
    (×-distribʳ-⊎ 0ℓ A B C)
  , λ { {⊎.inj₁ _ , _} {⊎.inj₁ _ , _} →  id′ , id′
      ; {⊎.inj₁ _ , _} {⊎.inj₂ _ , _} → (λ { (() , _)}) , λ ()
      ; {⊎.inj₂ _ , _} {⊎.inj₁ _ , _} → (λ { (() , _)}) , λ ()
      ; {⊎.inj₂ _ , _} {⊎.inj₂ _ , _} → id′ , id′
      }

⊗-distrib-⊕ : (_≃_ DistributesOver _⊗_) _⊕_
⊗-distrib-⊕ = ⊗-distribˡ-⊕ , ⊗-distribʳ-⊕
```

## Properties of `_⊕_` and `_∘_`

```agda
-- ∘-distribˡ-⊕ : (_≃_ DistributesOverˡ _∘_) _⊕_
-- ∘-distribˡ-⊕ (C , Rᶜ) (A , Rᴬ) (B , Rᴮ) =
--     (×-distribˡ-⊎ 0ℓ C A B)
--   , λ { {c , ⊎.inj₁ a } {c₂ , ⊎.inj₁ b} → id′ , id′
--       ; {c , ⊎.inj₁ a } {c₂ , ⊎.inj₂ b} → {!   !} , λ () -- Nope
--       ; {c , ⊎.inj₂ a } {c₂ , ⊎.inj₁ b} → {!   !} , λ () -- Nope
--       ; {c , ⊎.inj₂ a } {c₂ , ⊎.inj₂ b} → id′ , id′ 
--       }

⊙-distribʳ-⊕ : (_≃_ DistributesOverʳ _⊙_) _⊕_
⊙-distribʳ-⊕ (C , Rᶜ) (A , Rᴬ) (B , Rᴮ) =
    (×-distribʳ-⊎ 0ℓ C A B)
  , λ { {⊎.inj₁ a , c } {⊎.inj₁ b , c' } → 
           (⊎.map id′ (×.map₁ ⊎.inj₁-injective))
          , ⊎.map id′ (×.map₁ (cong ⊎.inj₁))
      ;  {⊎.inj₁ a , c } {⊎.inj₂ b , c' } → 
           (λ { (⊎.inj₁ ())
              ; (⊎.inj₂ (() , _))
              })
          , λ ()
      ;  {⊎.inj₂ a , c } {⊎.inj₁ b , c' } →
           (λ { (⊎.inj₁ ())
              ; (⊎.inj₂ (() , _))
              })
          , λ ()
      ;  {⊎.inj₂ a , c } {⊎.inj₂ b , c' } → 
            (⊎.map id′ (×.map₁ ⊎.inj₂-injective)) 
          , (⊎.map id′ (×.map₁ (cong ⊎.inj₂)))
  
      }
```

## Algebra of `_⊕_` for `Graph`

```agda
⊕-isMagma : IsMagma _≃_ _⊕_
⊕-isMagma = record 
    { isEquivalence = ≃-isEquivalence
    ; ∙-cong = λ {x y u v} → ⊕-cong {x} {y} {u} {v}
    } 

⊕-isSemigroup : IsSemigroup _≃_ _⊕_
⊕-isSemigroup = record 
  { isMagma = ⊕-isMagma 
  ; assoc = ⊕-assoc
  }

⊕-isMonoid : IsMonoid _≃_ _⊕_ 𝟘
⊕-isMonoid = record 
  { isSemigroup = ⊕-isSemigroup
  ; identity = 𝟘-⊕-identityˡ , 𝟘-⊕-identityʳ
  }

⊕-isCommutativeMonoid : IsCommutativeMonoid _≃_ _⊕_ 𝟘
⊕-isCommutativeMonoid = record 
  { isMonoid = ⊕-isMonoid
  ; comm = ⊕-comm
  }

⊕-magma : Magma (suc 0ℓ) 0ℓ
⊕-magma = record { isMagma = ⊕-isMagma }

⊕-semigroup : Semigroup _ _
⊕-semigroup = record { isSemigroup = ⊕-isSemigroup }

⊕-monoid : Monoid _ _
⊕-monoid = record { isMonoid = ⊕-isMonoid }

⊕-commutativeMonoid : CommutativeMonoid _ _
⊕-commutativeMonoid = record { isCommutativeMonoid = ⊕-isCommutativeMonoid }
```

## Algebra of `_⊗_` for `Graph`

```agda
⊗-isMagma : IsMagma _≃_ _⊗_
⊗-isMagma = record 
    { isEquivalence = ≃-isEquivalence
    ; ∙-cong = λ {x y u v} → ⊗-cong {x} {y} {u} {v}
    } 

⊗-isSemigroup : IsSemigroup _≃_ _⊗_
⊗-isSemigroup = record 
  { isMagma = ⊗-isMagma 
  ; assoc = ⊗-assoc
  }

⊗-isMonoid : IsMonoid _≃_ _⊗_ 𝟙
⊗-isMonoid = record 
  { isSemigroup = ⊗-isSemigroup
  ; identity = 𝟙-⊗-identityˡ , 𝟙-⊗-identityʳ
  }

⊗-isCommutativeMonoid : IsCommutativeMonoid _≃_ _⊗_ 𝟙
⊗-isCommutativeMonoid = record 
  { isMonoid = ⊗-isMonoid
  ; comm = ⊗-comm
  }

⊗-magma : Magma (suc 0ℓ) 0ℓ
⊗-magma = record { isMagma = ⊗-isMagma }

⊗-semigroup : Semigroup _ _
⊗-semigroup = record {isSemigroup = ⊗-isSemigroup }

⊗-monoid : Monoid _ _
⊗-monoid = record { isMonoid = ⊗-isMonoid }

⊗-commutativeMonoid : CommutativeMonoid _ _
⊗-commutativeMonoid = record { isCommutativeMonoid = ⊗-isCommutativeMonoid }
```

## Algebra of `_□_` for `Graph`

```agda
□-isMagma : IsMagma _≃_ _□_
□-isMagma = record 
    { isEquivalence = ≃-isEquivalence
    ; ∙-cong = λ {x y u v} → □-cong {x} {y} {u} {v}
    }

□-isSemigroup : IsSemigroup _≃_ _□_
□-isSemigroup = record 
    { isMagma = □-isMagma
    ; assoc = □-assoc
    }

□-isCommutativeSemigroup : IsCommutativeSemigroup _≃_ _□_
□-isCommutativeSemigroup = record 
    { isSemigroup = □-isSemigroup
    ; comm = □-comm
    }
```

## Algebra of `_⊙_` for `Graph`

```agda
⊙-isMagma : IsMagma _≃_ _⊙_
⊙-isMagma = record 
    { isEquivalence = ≃-isEquivalence
    ; ∙-cong = λ {x y u v} → ⊙-cong {x} {y} {u} {v}
    }

⊙-isSemigroup : IsSemigroup _≃_ _⊙_
⊙-isSemigroup = record 
    { isMagma = ⊙-isMagma
    ; assoc = ⊙-assoc
    }
```

## Semirings of `⊕` `⊗` for `Graph`

```agda
⊕-⊗-isSemiringWithoutAnnihilatingZero : 
  IsSemiringWithoutAnnihilatingZero _≃_ _⊕_ _⊗_ 𝟘 𝟙
⊕-⊗-isSemiringWithoutAnnihilatingZero = record
  { +-isCommutativeMonoid = ⊕-isCommutativeMonoid
  ; *-cong = λ {x y u v} → ⊗-cong {x} {y} {u} {v}
  ; *-assoc = ⊗-assoc
  ; *-identity = 𝟙-⊗-identityˡ , 𝟙-⊗-identityʳ
  ; distrib = ⊗-distrib-⊕
  }

⊕-⊗-isSemiring : IsSemiring _≃_ _⊕_ _⊗_ 𝟘 𝟙
⊕-⊗-isSemiring = record 
  { isSemiringWithoutAnnihilatingZero = ⊕-⊗-isSemiringWithoutAnnihilatingZero 
  ; zero = 𝟘-⊗-zero 
  }

⊕-⊗-isCommutativeSemiring : IsCommutativeSemiring _≃_ _⊕_ _⊗_ 𝟘 𝟙
⊕-⊗-isCommutativeSemiring = record 
  { isSemiring = ⊕-⊗-isSemiring
  ; *-comm = ⊗-comm 
  }

⊕-⊗-semiring : Semiring _ _
⊕-⊗-semiring = record { isSemiring = ⊕-⊗-isSemiring }

⊕-⊗-commutativeSemiring : CommutativeSemiring _ _
⊕-⊗-commutativeSemiring = 
  record { isCommutativeSemiring = ⊕-⊗-isCommutativeSemiring }
```

## (Near)Semiring of `⊕` `⊙` for `Graph`

```agda
⊕-⊙-isNearSemiring : IsNearSemiring _≃_ _⊕_ _⊙_ 𝟘
⊕-⊙-isNearSemiring = record
    { +-isMonoid = ⊕-isMonoid
    ; *-cong = λ { x y u v} → ⊙-cong {x} {y} {u} {v}
    ; *-assoc = ⊙-assoc
    ; distribʳ = ⊙-distribʳ-⊕
    ; zeroˡ = 𝟘-⊙-zeroˡ
    }

⊕-⊙-NearSemiring : NearSemiring _ _
⊕-⊙-NearSemiring = record { isNearSemiring = ⊕-⊙-isNearSemiring }
```
