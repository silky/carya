---
title: Graph
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-} 

module Graph.Base where

open import Data.Empty.Polymorphic using (⊥)
open import Data.Unit.Polymorphic using (⊤)
open import Data.Product as × using (_×_; _,_)
open import Data.Sum as ⊎ using (_⊎_)
open import Function using () renaming (id to id′)

open import Relation.Binary.PropositionalEquality using (_≡_)

open import Graph.Core
open import Relation.Core using (_∼_  ; _≈_)
open import Relation.Base as R 
  using ( one ; zero )
  renaming 
    (_⊕_ to _⊕ᴿ_; _⊗_ to _⊗ᴿ_; _⊙_ to _⊙ᴿ_; _□_ to _□ᴿ_
    ; pure to pureᴿ; id to idᴿ)
```

</details>

```agda
𝟙 : Graph
𝟙 = ⊤ , one

⟦𝟙⟧ : ⟦ 𝟙 ⟧ ≈ one
⟦𝟙⟧ = id′ , id′

𝟘 : Graph
𝟘  = ⊥ , zero

⟦𝟘⟧ : ⟦ 𝟘 ⟧ ≈ zero
⟦𝟘⟧ = id′ , id′
```

```agda
infixl 6 _⊕_
_⊕_ : Graph → Graph → Graph 
(_ , R₁) ⊕ (_ , R₂) = _ , R₁ ⊕ᴿ R₂

⟦⊕⟧ : ∀ {x y : Graph} → ⟦ x ⊕ y ⟧ ≈ ⟦ x ⟧ ⊕ᴿ ⟦ y ⟧
⟦⊕⟧ = id′ , id′
```

```agda
infixr 9 _⊗_
_⊗_ : Graph → Graph → Graph
(_ , R₁) ⊗ (_ , R₂) = _ , R₁ ⊗ᴿ R₂

⟦⊗⟧ : ∀ {x y : Graph} → ⟦ x ⊗ y ⟧ ≈ ⟦ x ⟧ ⊗ᴿ ⟦ y ⟧
⟦⊗⟧ = id′ , id′
```

```agda
infixr 9 _⊙_
_⊙_ : Graph → Graph → Graph
(_ , R₁) ⊙ (_ , R₂) = _ , R₁ ⊙ᴿ R₂

⟦⊙⟧ : ∀ {x y : Graph} → ⟦ x ⊙ y ⟧ ≈ ⟦ x ⟧ ⊙ᴿ ⟦ y ⟧
⟦⊙⟧ = id′ , id′
```

```agda
infixr 9 _□_
_□_ : Graph → Graph → Graph
(_ , R₁) □ (_ , R₂) = _ , R₁ □ᴿ R₂

⟦□⟧ : ∀ {x y : Graph} → ⟦ x □ y ⟧ ≈ ⟦ x ⟧ □ᴿ ⟦ y ⟧
⟦□⟧ = id′ , id′
```

## ETC

```agda
pure : ∀ {A : Set} → A → Graph
pure a = _ , pureᴿ a

⟦pure⟧ : ∀ {A} {a : A} → ⟦ pure a ⟧ ≈ pureᴿ a
⟦pure⟧ = id′ , id′

id : ∀ {A : Set} → Graph
id {A = A} = A , idᴿ

⟦id⟧ : ∀ {A : Set} → ⟦ id {A} ⟧ ≈ idᴿ 
⟦id⟧ = id′ , id′
```
