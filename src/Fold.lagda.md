---
title: Folding over Vectors
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level)
open import Relation.Binary using (Setoid)

module Fold {a c ℓᵃ ℓ} (𝓐 : Setoid a ℓᵃ) where 

open import Algebra.Bundles using (Monoid; CommutativeMonoid; Semiring) 
open import Data.Fin using (Fin ; splitAt); open Fin
open import Data.Nat using (ℕ)
open import Data.Sum using (_⊎_ ; [_,_] ; map; inj₁ ; inj₂)
open import Data.Vec.Functional using (_++_)
open import Data.Vec.Functional.Relation.Binary.Equality.Setoid 𝓐
    using (tail⁺ ; head⁺ ; _≋_)
open import Function using ( id ; Congruent ; Inverse; _∘_)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open Setoid 𝓐 renaming 
  ( Carrier to A
  ; _≈_ to _≈ᴬ_
  ; refl to reflᴬ
  ; sym to symᴬ
  ; trans to transᴬ
  ) using ()

private 
  variable
    n n₁ n₂ : ℕ
```

</details>

```agda
module _ ⦃ M : Monoid c ℓ ⦄ where 
  
  open Monoid M renaming (Carrier to C)

  fold : (Fin n → A) → (A → C) → C 
  fold {n = ℕ.zero }  _ _  = ε
  fold {n = ℕ.suc x} #A g = g (#A zero) ∙ fold (#A ∘ suc) g
```

```agda
module _ ⦃ M : CommutativeMonoid c ℓ ⦄ where
  
  open CommutativeMonoid M renaming (Carrier to C)

  ⊙ : (Fin n → A) → (A → C) → C
  ⊙ = fold ⦃ M = CommutativeMonoid.monoid M  ⦄
```

## Properties of `⊙`

`⊙` preserves the identity of the underlying monoid.

```agda
module _ ⦃ M : CommutativeMonoid c ℓ ⦄ where
  
  open CommutativeMonoid M renaming (Carrier to C)

  ⊙ᴹ = ⊙ ⦃ M ⦄

  ⊙-identityʳ : (# : Fin n → A) → (f : A → C) → (⊙ᴹ # f) ∙ ε ≈ ⊙ᴹ # f
  ⊙-identityʳ # = identityʳ ∘ ⊙ᴹ #

  ⊙-identityˡ : (# : Fin n → A) → (f : A → C) →  ε ∙ (⊙ᴹ # f) ≈ ⊙ᴹ # f
  ⊙-identityˡ # = identityˡ ∘ ⊙ᴹ #
```

```agda
  ⊙-cong : {f : A → C} 
    → {f-cong : Congruent _≈ᴬ_ _≈_ f} 
    → (#₁ #₂ : Fin n → A)
    → (#₁ ≋ #₂)
    → ⊙ᴹ #₁ f ≈ ⊙ᴹ #₂ f
  ⊙-cong {n = ℕ.zero} _ _ _ = refl
  ⊙-cong {n = ℕ.suc n} {f = f} {f-cong} #₁ #₂ #₁≋#₂  =
     begin
        ⊙ᴹ #₁ f
      ≡⟨⟩
        f (#₁ zero) ∙ ⊙ᴹ (#₁ ∘ suc) f
      ≈⟨ ∙-congˡ 
            (⊙-cong {f-cong = f-cong} (#₁ ∘ suc) (#₂ ∘ suc) (tail⁺ _≈ᴬ_ #₁≋#₂))
       ⟩
        f (#₁ zero) ∙ (⊙ᴹ (#₂ ∘ suc) f)
      ≈⟨ ∙-congʳ (f-cong (head⁺ _≈ᴬ_ #₁≋#₂)) ⟩
        f (#₂ zero) ∙ (⊙ᴹ (#₂ ∘ suc) f)
      ≡⟨⟩
       ⊙ᴹ #₂ f
      ∎
    where open ≈-Reasoning setoid
```

```agda
  ⊙-preserves-++ : (#₁ : Fin n₁ → A) → (#₂ : Fin n₂ → A)
     → (f : A → C) 
     → Congruent _≈ᴬ_ _≈_ f
     → ⊙ᴹ #₁ f ∙ ⊙ᴹ #₂ f ≈ ⊙ᴹ (#₁ ++ #₂) f
  ⊙-preserves-++ {n₁ = ℕ.zero} {n₂ = ℕ.zero} _ _ _ _ = identityˡ ε
  ⊙-preserves-++ {n₁ = ℕ.zero} _ #₂ f _ = identityˡ (⊙ᴹ #₂ f)
  ⊙-preserves-++ {n₁ = ℕ.suc n} {n₂ = m} #₁ #₂ f f-cong =
    begin
      (⊙ᴹ #₁ f ∙ ⊙ᴹ #₂ f)
    ≡⟨⟩
      f (#₁ zero) ∙ ⊙ᴹ (#₁ ∘ suc) f ∙ ⊙ᴹ #₂ f
    ≈⟨ assoc (f (#₁ zero)) (⊙ᴹ (#₁ ∘ suc) f) (⊙ᴹ #₂ f) ⟩
      f (#₁ zero) ∙ (⊙ᴹ (#₁ ∘ suc) f ∙ ⊙ᴹ #₂ f)
    ≈⟨ ∙-congˡ (⊙-preserves-++  (#₁ ∘ suc) #₂ f f-cong) ⟩
      f (#₁ zero) ∙ (⊙ᴹ ((#₁ ∘ suc) ++ #₂) f)
    ≈⟨ ∙-congˡ (⊙-cong {f-cong = f-cong}
            ([ #₁ ∘ suc , #₂ ] ∘ splitAt n) 
            ([ #₁ , #₂ ] ∘ [ inj₁ ∘ suc , inj₂ ] ∘ splitAt n) 
            (λ i → symᴬ ([,]-map {#₁ = #₁} {#₂} {i = splitAt n i} ))
            )
     ⟩ 
      ⊙ᴹ (#₁ ++ #₂) f
    ∎
    where open ≈-Reasoning setoid
          -- TODO: the following is basically [,]-map from the stdlib,
          -- but this does not use prop equality.
          [,]-map : ∀ {n₁ n₂} {#₁ : Fin (ℕ.suc n₁) → A} {#₂ : Fin n₂ → A} → 
              (∀ {i : (Fin n₁) ⊎ (Fin n₂)} → ([ #₁  , #₂ ] ∘ map suc id) i
                    ≈ᴬ [ #₁ ∘ suc ,  #₂ ∘ id ] i)
          [,]-map {i = inj₁ _} = reflᴬ
          [,]-map {i = inj₂ _} = reflᴬ
```

`⊙` is congruent with respect to the binary operation 
of the commutative monoid.

```agda
  ⊙-preserves-∙ : (# : Fin n → A) → (f g : A → C) 
     → (⊙ᴹ # (λ x → f x ∙ g x)) ≈ ⊙ᴹ # f ∙ ⊙ᴹ # g
  ⊙-preserves-∙ {n = ℕ.zero} _ _ _  = sym (identityˡ ε) 
  ⊙-preserves-∙ {n = ℕ.suc n} # f g = 
    begin 
      (⊙ᴹ # λ x → f x ∙ g x) 
      ≡⟨⟩
        (f (# zero) ∙ g (# zero)) ∙ ⊙ (# ∘ suc) (λ ω → f ω ∙ g ω)
      ≈⟨ ∙-congˡ (⊙-preserves-∙ {n = n} (# ∘ suc) f g) ⟩
        (f (# zero) ∙ g (# zero)) ∙ (⊙ (# ∘ suc) f ∙ ⊙ (# ∘ suc) g)
      ≈⟨ assoc (f (# zero)) (g (# zero)) (⊙ (# ∘ suc) f ∙ ⊙ (# ∘ suc) g) ⟩
        f (# zero) ∙ (g (# zero) ∙ (⊙ (# ∘ suc) f ∙ ⊙ (# ∘ suc) g))
      ≈⟨ ∙-congˡ (sym (assoc (g (# zero)) (⊙ (# ∘ suc) f) (⊙ (# ∘ suc) g))) ⟩
        f (# zero) ∙ ((g (# zero) ∙ ⊙ (# ∘ suc) f) ∙ ⊙ (# ∘ suc) g)
      ≈⟨ ∙-congˡ (∙-congʳ (comm  (g (# zero)) (⊙ (# ∘ suc) f))) ⟩
        f (# zero) ∙ ((⊙ (# ∘ suc) f ∙ g (# zero)) ∙ ⊙ (# ∘ suc) g)
      ≈⟨ ∙-congˡ (assoc (⊙ (# ∘ suc) f) (g (# zero)) (⊙ (# ∘ suc) g)) ⟩
        f (# zero) ∙ (⊙ (# ∘ suc) f ∙ (g (# zero) ∙ ⊙ (# ∘ suc) g))
      ≈˘⟨ assoc (f (# zero)) (⊙ (# ∘ suc) f) ((g (# zero) ∙ ⊙ (# ∘ suc) g)) ⟩
        (f (# zero) ∙ ⊙ (# ∘ suc) f) ∙ (g (# zero) ∙ ⊙ (# ∘ suc) g)
      ≡⟨⟩
        ⊙ # f ∙ ⊙ # g
    ∎
    where open ≈-Reasoning setoid
```

`⊙` is commutative with respect to functions `f` and `g`
that map *to* the underlying commutative monoid.

```agda
  ⊙-comm : (# : Fin n → A) → (f g : A → C)
      → (⊙ᴹ # f ∙ ⊙ᴹ # g) ≈ (⊙ᴹ # g ∙ ⊙ᴹ # f)
  ⊙-comm  # f g = comm (⊙ # f) (⊙ # g)
```

## Specialized cases

```agda
module SemiringFold ⦃ S : Semiring c ℓ ⦄ where
  open Semiring S renaming (Carrier to C ; refl to reflˢ)

  ∑[_]_ : (Fin n → A) → (A → C) → C
  ∑[ i ] f = fold ⦃ M = +-monoid ⦄ i f

  ∏[_]_ : (Fin n → A) → (A → C) → C
  ∏[ i ] f =  fold ⦃ M = *-monoid ⦄ i f

  -- Properties of Folds over semirings
  ∑0#≡0# : ∀ {# : Fin n → A} → ∑[ # ] (λ _ → 0#) ≈ 0#
  ∑0#≡0# {n = ℕ.zero} = reflˢ
  ∑0#≡0# {n = ℕ.suc n} {# = #} =
      begin 
        ∑[ # ] (λ _ → 0#)
      ≡⟨⟩
        fold ⦃ +-monoid ⦄ # (λ _ → 0#)
      ≈⟨ +-congˡ (∑0#≡0# {n = n} {# = # ∘ suc}) ⟩
        0# + 0#
      ≈⟨ +-identityˡ 0# ⟩
        0#
    ∎
    where open ≈-Reasoning (record {isEquivalence = isEquivalence})

  ∏1#≡1# : ∀ {# : Fin n → A} → ∏[ # ] (λ _ → 1#) ≈ 1#
  ∏1#≡1# {n = ℕ.zero} = reflˢ
  ∏1#≡1# {n = ℕ.suc n} {# = #} = 
      begin 
        ∏[ # ] (λ _ → 1#)
      ≡⟨⟩
        fold ⦃ *-monoid ⦄ # (λ _ → 1#)
      ≈⟨ *-congˡ (∏1#≡1# {n = n} {# = # ∘ suc}) ⟩
        1# * 1#
      ≈⟨ *-identityˡ 1# ⟩
        1#
    ∎
    where open ≈-Reasoning (record {isEquivalence = isEquivalence})
```

```agda
-- module NatFold where 
--   open import Data.Nat.Properties
--   open SemiringFold ⦃ S = +-*-semiring ⦄
--   open Semiring +-*-semiring renaming (refl to reflˢ)

--   ∑1#≡n : ∀ {# : Fin n → A} → ∑[ # ] (λ _ → 1#) ≈ n
--   ∑1#≡n {n = ℕ.zero} = reflˢ
--   ∑1#≡n {n = ℕ.suc n} {# = #} = +-congˡ (∑1#≡n {n = n} {# = # ∘ suc}) 
```
