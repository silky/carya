---
title: Basic Operations on Relations
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_ ; suc; 0ℓ)

module Relation.Base where

open import Data.Product as × using (∃ ; _×_; _,_)
open import Data.Sum as ⊎ using (_⊎_)
open import Data.Unit.Polymorphic using (⊤ ; tt)
open import Data.Empty.Polymorphic using (⊥)
open import Function renaming (id to id′) using ()
open import Relation.Binary.PropositionalEquality using (_≡_)

open import Relation.Core

private
  variable
    A B C D : Set
```

</details>

## Special Relations

```agda
-- "Always" construct in stdlib
one : A ∼ B
one _ _ = ⊤

-- "Never" construct in stdlib
zero : A ∼ B
zero _ _ = ⊥

id : A ∼ A
id a₁ a₂ = a₁ ≡ a₂

pure : A → A ∼ A
pure a a₁ a₂ = a ≡ a₁ × a ≡ a₂
```

## Standard Binary Operations on Relations

### Union

See 
[Relation.Binary.Construct.Union](https://agda.github.io/agda-stdlib/v2.0/Relation.Binary.Construct.Union.html)

```agda
infixl 6 _+_
_+_ : A ∼ B → A ∼ B → A ∼ B
R₁ + R₂ = λ a b → R₁ a b ⊎ R₂ a b
```

### Intersection

See 
[Relation.Binary.Construct.Intersection](https://agda.github.io/agda-stdlib/v2.0/Relation.Binary.Construct.Intersection.html)

```agda
infixl 7 _*_
_*_ : A ∼ B → A ∼ B → A ∼ B
R₁ * R₂ = λ a b → R₁ a b × R₂ a b
```

### Composition

See 
[Relation.Binary.Construct.Composition](https://agda.github.io/agda-stdlib/v2.0/Relation.Binary.Construct.Composition.html)

```agda
infixr 9 _∘_
_∘_ : B ∼ C → A ∼ B → A ∼ C
_R₂_ ∘ _R₁_ = λ a c → ∃ λ b → (a R₁ b) × (b R₂ c)
```

### Disjoint Union

See also:

* [Disjoint Union of Graphs](https://en.wikipedia.org/wiki/Disjoint_union_of_graphs)

```agda
_⊕_ : A ∼ C → B ∼ D → (A ⊎ B) ∼ (C ⊎ D)
R₁ ⊕ R₂ = 
  λ { (⊎.inj₁ a) (⊎.inj₁ c) → R₁ a c
    ; (⊎.inj₁ _) (⊎.inj₂ _) → ⊥
    ; (⊎.inj₂ _) (⊎.inj₁ _) → ⊥
    ; (⊎.inj₂ b) (⊎.inj₂ d) → R₂ b d 
    }
```

### Tensor Product

See also:

* [Graph tensor product](https://en.wikipedia.org/wiki/Tensor_product_of_graphs)

```agda
_⊗_ : A ∼ C → B ∼ D → (A × B) ∼ (C × D)
R₁ ⊗ R₂ = λ (a , b) (c , d) → R₁ a c × R₂ b d
```

### How to classify the following (?) (bi-product (?))

```agda
×-copy : A ∼ (A × A)
×-copy a (a₁ , a₂) = id a a₁ × id a a₂

_▵'_ : A ∼ B → A ∼ C → A ∼ (B × C)
R₁ ▵' R₂ = (R₁ ⊗ R₂) ∘ ×-copy

×-reduce : (A × A) ∼ A 
×-reduce (a₁ , a₂) a = ×-copy a (a₁ , a₂)

_▿'_ : A ∼ C → B ∼ C → (A × B) ∼ C
R₁ ▿' R₂ = ×-reduce ∘ (R₁ ⊗ R₂)

×-proj₁ : (A × B) ∼ A
×-proj₁ (a₁ , _) a = id a₁ a

×-proj₂ : (A × B) ∼ B
×-proj₂ (_ , b₁) b = id b₁ b

×-inj₁ : A ∼ (A × B)
×-inj₁ a (a₁ , _) = id a a₁

×-inj₂ : B ∼ (A × B)
×-inj₂ b (_ , b₁) = id b b₁
```

```agda
copy : A ∼ (A ⊎ A)
copy a₁ (⊎.inj₁ a₂) = id a₁ a₂
copy a₁ (⊎.inj₂ a₂) = id a₁ a₂

_▵_ : A ∼ B → A ∼ C → A ∼ (B ⊎ C)
R₁ ▵ R₂ = (R₁ ⊕ R₂) ∘ copy

reduce : (A ⊎ A) ∼ A 
reduce (⊎.inj₁ a₁) a₂ = id a₁ a₂
reduce (⊎.inj₂ a₁) a₂ = id a₁ a₂

_▿_ : A ∼ C → B ∼ C → (A ⊎ B) ∼ C
R₁ ▿ R₂ = reduce ∘ (R₁ ⊕ R₂)

swap : (A ⊎ B) ∼ (B ⊎ A)
swap (⊎.inj₁ _) (⊎.inj₁ _) = ⊥
swap (⊎.inj₁ a₁) (⊎.inj₂ a₂) = id a₁ a₂
swap (⊎.inj₂ b₁) (⊎.inj₁ b₂) = id b₁ b₂
swap (⊎.inj₂ _) (⊎.inj₂ _) = ⊥

proj₁ : (A ⊎ B) ∼ A 
proj₁ (⊎.inj₁ a₁) a₂ = id a₁ a₂
proj₁ (⊎.inj₂ _) _ = ⊥

proj₂ : (A ⊎ B) ∼ B
proj₂ (⊎.inj₂ b₁) b₂ = id b₁ b₂
proj₂ (⊎.inj₁ _) _ = ⊥

inj₁ : A ∼ (A ⊎ B)
inj₁ a₁ (⊎.inj₁ a₂) = id a₁ a₂
inj₁ _ (⊎.inj₂ _) = ⊥

inj₂ : B ∼ (A ⊎ B)
inj₂ b₁ (⊎.inj₂ b₂) = id b₁ b₂
inj₂ _ (⊎.inj₁ _) = ⊥
```

## Graph-like Operations

TODO: better label than "graph-like" (?)

### Cartesian Product

See: 

* [Graph Cartesian product](https://en.wikipedia.org/wiki/Cartesian_product_of_graphs)

```agda
_□_ : A ∼ A → B ∼ B → (A × B) ∼ (A × B)
R₁ □ R₂ = (id ⊗ R₂) + (R₁ ⊗ id)

_ : ∀ {R₁ : A ∼ A} {R₂ : B ∼ B} → 
    R₁ □ R₂ ≈ λ (a₁ , b₁) (a₂ , b₂) → a₁ ≡ a₂ × R₂ b₁ b₂ ⊎ b₁ ≡ b₂ × R₁ a₁ a₂ 
_ = (⊎.map id′ ×.swap) , ⊎.map id′ ×.swap
```

### Lexographic product (graph composition)

TODO: what to call this?
The relation operation corresponds 
to the lexographic product (composition) of graphs.

```agda
_⊙_ : A ∼ A → B ∼ B → (A × B) ∼ (A × B)
R₁ ⊙ R₂ = (R₁ ⊗ one) + (id ⊗ R₂)

_ : ∀ {R₁ : A ∼ A} {R₂ : B ∼ B} → 
    R₁ ⊙ R₂ ≈ λ (a₁ , b₁) (a₂ , b₂) → R₁ a₁ a₂ ⊎ (a₁ ≡ a₂ × R₂ b₁ b₂)
_ = (⊎.map ×.proj₁ id′) , (⊎.map (_, tt) id′)
```

### Strong product 

See also:

* [Graph strong product](https://en.wikipedia.org/wiki/Strong_product_of_graphs)

```agda
_⊠_ : A ∼ A → B ∼ B → (A × B) ∼ (A × B)
R₁ ⊠ R₂ = R₁ □ R₂ + (R₁ ⊗ R₂)

_ : ∀ {R₁ : A ∼ A} {R₂ : B ∼ B} → 
    R₁ ⊠ R₂ ≈ 
     λ (a₁ , b₁) (a₂ , b₂) → 
        (a₁ ≡ a₂ × R₂ b₁ b₂ ⊎ b₁ ≡ b₂ × R₁ a₁ a₂) ⊎ R₁ a₁ a₂ × R₂ b₁ b₂
_ =  (⊎.map (⊎.map id′ ×.swap) id′) , ⊎.map (⊎.map id′ ×.swap)  id′
```
