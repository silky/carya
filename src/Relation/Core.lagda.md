---
title: Relations
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

module Relation.Core where

open import Data.Product as × using (_×_; _,_)
open import Function using (id ; _∘_) 
open import Level using (Level ; _⊔_ ; suc; 0ℓ)
open import Relation.Binary using (REL ; IsEquivalence)
```

</details>

```agda
_∼_ : Set → Set → Set₁
A ∼ B = REL A B 0ℓ

infix 4 _≈_
_≈_ : {A B : Set} → A ∼ B → A ∼ B → Set
_R₁_ ≈ _R₂_ = ∀ {a b} → ((a R₁ b → a R₂ b) × (a R₂ b → a R₁ b))

isEquivalence : ∀ {A B : Set} → IsEquivalence (_≈_ {A = A} {B}) 
isEquivalence = record 
  { refl = id , id
  ; sym = λ f → ×.swap f 
  ; trans = λ f g → ×.proj₁ g ∘ ×.proj₁ f , ×.proj₂ f ∘ ×.proj₂ g 
  }
```
 