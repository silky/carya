---
title: Biproduct Category of Relations
---

<details>
<summary>Imports</summary>

```agda
-- {-# OPTIONS --safe --cubical-compatible #-}
{-# OPTIONS --safe --without-K #-} -- proof of π₁∘ι₂≡zero gives cubical-compatible warning  

open import Level using (Level ; _⊔_ ; suc; 0ℓ)

module Relation.Category.Biproduct  where

open import Data.Product as × using (_,_)
open import Data.Sum as ⊎ using (_⊎_)
open import Data.Sum.Properties as ⊎
open import Relation.Binary.PropositionalEquality 
  using (_≡_; trans; refl ; cong)

open import Relation.Core
open import Relation.Base

private
  variable
    A B C D : Set
```


```agda
π₁∘ι₁≡id : proj₁ {A = A} {B} ∘ inj₁ ≈ id 
π₁∘ι₁≡id {a = a₁} {a₂} = 
    (λ { (⊎.inj₁ a , a₁≡a , a≡a₂) → trans a₁≡a a≡a₂ }) 
    , λ x → ⊎.inj₁ a₁ , refl , x

π₂∘ι₂≡id : proj₂ {A = A} {B} ∘ inj₂ ≈ id
π₂∘ι₂≡id {a = b₁} {b₂} = 
    (λ { (⊎.inj₂ b , b₁≡b , b≡b₂) → trans b₁≡b b≡b₂ }) 
  , λ x → ⊎.inj₂ b₁ , refl , x

ι₁∘π₁+ι₂∘π₂≡id : (inj₁ ∘ proj₁ {A = A} {B} ) + (inj₂ ∘ proj₂) ≈ id
ι₁∘π₁+ι₂∘π₂≡id {a = ⊎.inj₁ a₁} {⊎.inj₁ a₂} = 
      (λ {(⊎.inj₁ (a₃ , a₁≡a₃ , a₃≡a₂ )) → cong ⊎.inj₁ (trans a₁≡a₃ a₃≡a₂) 
      ; (⊎.inj₂ (_ , (), _)) } ) 
    , λ x → ⊎.inj₁ (a₁ , refl , ⊎.inj₁-injective x)
ι₁∘π₁+ι₂∘π₂≡id {a = ⊎.inj₁ _} {⊎.inj₂ _} = 
      (λ {(⊎.inj₁ (_ , _ , ())) ; (⊎.inj₂ (_ , (), _)) } ) 
    , λ ()
ι₁∘π₁+ι₂∘π₂≡id {a = ⊎.inj₂ _} {⊎.inj₁ _} = 
    (λ {(⊎.inj₁ (_ , () , _)) ; (⊎.inj₂ (_ , _ , ())) } ) 
    , λ ()
ι₁∘π₁+ι₂∘π₂≡id {a = ⊎.inj₂ b₁} {⊎.inj₂ b₂} =
    (λ {(⊎.inj₂ (b₃ , b₁≡b₃ , b₃≡b₂ )) → cong ⊎.inj₂ (trans b₁≡b₃ b₃≡b₂) 
      ; (⊎.inj₁ (_ , (), _)) } ) 
    , λ x → ⊎.inj₂ (b₁ , refl , ⊎.inj₂-injective x)
```

## Orthogonality

```agda
π₁∘ι₂≡zero : proj₁ {A = A} {B} ∘ inj₂ ≈ zero
π₁∘ι₂≡zero  = 
    (λ { ((⊎.inj₁ _) , () , _)   })
    , λ ()

π₂∘ι₁≡zero : proj₂ {A = A} {B} ∘ inj₁ ≈ zero
π₂∘ι₁≡zero = ((λ { ((⊎.inj₁ a) , _ , ()) })) , (λ ())

ι₁▿ι₂≡id : inj₁ {A = A} {B} ▿ inj₂ ≈ id
ι₁▿ι₂≡id  {a = ⊎.inj₁ a₁} {⊎.inj₁ a₂} = 
    (λ { (⊎.inj₁ (⊎.inj₁ a₃) , a₃≡a₁ , x) → trans (cong ⊎.inj₁ a₃≡a₁) x
        ; (⊎.inj₁ (⊎.inj₂ _) , () , _)
        ; (⊎.inj₂ (⊎.inj₁ _) , () , _)
        ; (⊎.inj₂ (⊎.inj₂ _) , () , _)
        } ) 
  , λ x → ⊎.inj₁ (⊎.inj₁ a₁) , refl , x
ι₁▿ι₂≡id  {a = ⊎.inj₁ a₁} {⊎.inj₂ b₁} = 
    (λ { (⊎.inj₁ (⊎.inj₁ a₂) , a₁≡a₂ , x) → trans (cong ⊎.inj₁ a₁≡a₂) x
        ; (⊎.inj₁ (⊎.inj₂ _) , () , _)
        ; (⊎.inj₂ (⊎.inj₁ _) , () , _)
        ; (⊎.inj₂ (⊎.inj₂ _) , () , _)
        } ) 
  , λ x → ⊎.inj₁ (⊎.inj₁ a₁) , refl , x
ι₁▿ι₂≡id  {a = ⊎.inj₂ b₁} {⊎.inj₁ _} =
      (λ { (⊎.inj₁ (⊎.inj₁ _) , () , _)
        ; (⊎.inj₁ (⊎.inj₂ _) , () , _)
        ; (⊎.inj₂ (⊎.inj₁ _) , () , _)
        ; (⊎.inj₂ (⊎.inj₂ _) , b₁≡b₂ , x) → trans (cong ⊎.inj₂ b₁≡b₂) x
        } ) 
  , λ x → ⊎.inj₂ (⊎.inj₂ b₁) , refl , x
ι₁▿ι₂≡id  {a = ⊎.inj₂ b₁} {⊎.inj₂ _} =
    (λ { (⊎.inj₁ (⊎.inj₁ _) , () , _)
    ; (⊎.inj₁ (⊎.inj₂ _) , () , _)
    ; (⊎.inj₂ (⊎.inj₁ _) , () , _)
    ; (⊎.inj₂ (⊎.inj₂ _) , b₁≡b₂ , x) → trans (cong ⊎.inj₂ b₁≡b₂) x
    } ) 
  , λ x → ⊎.inj₂ (⊎.inj₂ b₁) , refl , x
```
