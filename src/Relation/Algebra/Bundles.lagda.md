---
title: Algebraic Bundles on Relations
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

module Relation.Algebra.Bundles where

open import Algebra.Bundles
open import Relation.Algebra.Structures
```

</details>

## Bundles with Union

```agda
+-magma : ∀ {A B : Set} → Magma _ _
+-magma {A} {B} = record { isMagma = +-isMagma {A = A} {B}}

+-semigroup : ∀ {A B : Set} → Semigroup _ _
+-semigroup {A} {B} = record { isSemigroup = +-isSemigroup {A = A} {B} }

+-monoid : ∀ {A B : Set} → Monoid _ _
+-monoid {A} {B} = record { isMonoid = +-isMonoid {A = A} {B} }

+-commutativeMonoid : ∀ {A B : Set} → CommutativeMonoid _ _
+-commutativeMonoid {A} {B} = record 
  { isCommutativeMonoid = +-isCommutativeMonoid {A = A} {B} }
```

## Bundles with Intersection

```agda
*-magma : ∀ {A B : Set} → Magma _ _
*-magma {A} {B} = record { isMagma = *-isMagma {A = A} {B}}

*-semigroup : ∀ {A B : Set} → Semigroup _ _
*-semigroup {A} {B} = record { isSemigroup = *-isSemigroup {A = A} {B} }

*-monoid : ∀ {A B : Set} → Monoid _ _
*-monoid {A} {B} = record { isMonoid = *-isMonoid {A = A} {B} }

*-commutativeMonoid : ∀ {A B : Set} → CommutativeMonoid _ _
*-commutativeMonoid {A} {B} = record 
  { isCommutativeMonoid = *-isCommutativeMonoid {A = A} {B} }
```

## Bundles with Union and Intersection

```agda
+-*-semiringWithoutAnnihilatingZero : ∀ {A : Set} 
   → SemiringWithoutAnnihilatingZero _ _
+-*-semiringWithoutAnnihilatingZero {A} = record 
   { isSemiringWithoutAnnihilatingZero = 
        +-*-isSemiringWithoutAnnihilatingZero {A} 
   }

+-*-semiring : ∀ {A : Set} → Semiring _ _
+-*-semiring {A} = record { isSemiring = +-*-isSemiring {A} }

+-*-commutativeSemiring : ∀ {A : Set} → CommutativeSemiring _ _
+-*-commutativeSemiring {A} = record 
 { isCommutativeSemiring = +-*-isCommutativeSemiring {A} }
```

## Bundles with Union and Composition

```agda
+-∘-semiringWithoutAnnihilatingZero : ∀ {A : Set} 
   → SemiringWithoutAnnihilatingZero _ _
+-∘-semiringWithoutAnnihilatingZero {A} = record 
   { isSemiringWithoutAnnihilatingZero = 
        +-∘-isSemiringWithoutAnnihilatingZero {A} 
   }

+-∘-semiring : ∀ {A : Set} → Semiring _ _
+-∘-semiring {A} = record { isSemiring = +-∘-isSemiring {A} }
```
