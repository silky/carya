---
title: Algebraic Structures on Relations
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

module Relation.Algebra.Structures where

open import Algebra.Structures
open import Data.Product as × using (_,_)

open import Relation.Core
open import Relation.Base
open import Relation.Algebra.Properties

private
  variable
    A B : Set
```

</details>

## Structures with union

```agda
+-isMagma : IsMagma (_≈_ {A = A} {B}) _+_ 
+-isMagma = record 
  { isEquivalence = isEquivalence 
  ; ∙-cong = +-cong 
  }

+-isSemigroup : IsSemigroup (_≈_ {A = A} {B}) _+_
+-isSemigroup = record 
  { isMagma = +-isMagma 
  ; assoc = +-assoc 
  }

+-isMonoid : IsMonoid (_≈_ {A = A} {B}) _+_ zero
+-isMonoid = record 
  { isSemigroup = +-isSemigroup 
  ; identity = +-identityˡ , +-identityʳ 
  }

+-isCommutativeMonoid : IsCommutativeMonoid (_≈_ {A = A} {B}) _+_ zero
+-isCommutativeMonoid = record 
  { isMonoid = +-isMonoid 
  ; comm = +-comm
  }
```

## Structures with Intersection

```agda
*-isMagma : IsMagma (_≈_ {A = A} {B}) _*_ 
*-isMagma = record 
  { isEquivalence = isEquivalence 
  ; ∙-cong = *-cong 
  }

*-isSemigroup : IsSemigroup (_≈_ {A = A} {B}) _*_
*-isSemigroup = record 
  { isMagma = *-isMagma 
  ; assoc = *-assoc 
  }

*-isMonoid : IsMonoid (_≈_ {A = A} {B}) _*_ one
*-isMonoid = record 
  { isSemigroup = *-isSemigroup 
  ; identity = *-identityˡ , *-identityʳ 
  }

*-isCommutativeMonoid : IsCommutativeMonoid (_≈_ {A = A} {B}) _*_ one
*-isCommutativeMonoid = record 
  { isMonoid = *-isMonoid 
  ; comm = *-comm
  }
```

## Structures with Composition

```agda
∘-isMagma : IsMagma (_≈_ {A = A}) _∘_ 
∘-isMagma = record 
  { isEquivalence = isEquivalence 
  ; ∙-cong = ∘-cong 
  }

∘-isSemigroup : IsSemigroup (_≈_ {A = A}) _∘_
∘-isSemigroup = record 
  { isMagma = ∘-isMagma 
  ; assoc = ∘-assoc 
  }

∘-isMonoid : IsMonoid (_≈_ {A = A}) _∘_ id
∘-isMonoid = record 
  { isSemigroup = ∘-isSemigroup 
  ; identity = ∘-identityˡ , ∘-identityʳ 
  }
```

## Structures with Union and Intersection

```agda
+-*-isSemiringWithoutAnnihilatingZero : 
    IsSemiringWithoutAnnihilatingZero (_≈_ {A = A}) _+_ _*_ zero one
+-*-isSemiringWithoutAnnihilatingZero = record
  { +-isCommutativeMonoid = +-isCommutativeMonoid
  ; *-cong = *-cong
  ; *-assoc = *-assoc
  ; *-identity = *-identityˡ , *-identityʳ
  ; distrib = +-*-distribˡ , +-*-distribʳ
  }

+-*-isSemiring : IsSemiring (_≈_ {A = A}) _+_ _*_ zero one
+-*-isSemiring = record 
  { isSemiringWithoutAnnihilatingZero = +-*-isSemiringWithoutAnnihilatingZero 
  ; zero = *-zeroˡ , *-zeroʳ
  }

+-*-isCommutativeSemiring : IsCommutativeSemiring (_≈_ {A = A}) _+_ _*_ zero one
+-*-isCommutativeSemiring = record 
  { isSemiring = +-*-isSemiring
  ; *-comm = *-comm
  }
```

## Structures with Union and Composition

```agda
+-∘-isSemiringWithoutAnnihilatingZero : 
    IsSemiringWithoutAnnihilatingZero (_≈_ {A = A}) _+_ _∘_ zero id
+-∘-isSemiringWithoutAnnihilatingZero = record
  { +-isCommutativeMonoid = +-isCommutativeMonoid
  ; *-cong = ∘-cong
  ; *-assoc = ∘-assoc
  ; *-identity = ∘-identityˡ , ∘-identityʳ
  ; distrib = +-∘-distribˡ , +-∘-distribʳ
  }

+-∘-isSemiring : IsSemiring (_≈_ {A = A}) _+_ _∘_ zero id
+-∘-isSemiring = record 
  { isSemiringWithoutAnnihilatingZero = +-∘-isSemiringWithoutAnnihilatingZero 
  ; zero = ∘-zeroˡ , ∘-zeroʳ
  }
```
