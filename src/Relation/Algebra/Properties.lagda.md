---
title: Algebraic Properties of Operations on Relations
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

module Relation.Algebra.Properties where

open import Algebra.Definitions
open import Data.Unit.Polymorphic using (tt)
open import Data.Product as × using (∃ ; ∃₂ ; _×_; _,_)
open import Data.Sum as ⊎ using (_⊎_)
open import Function using (flip)
open import Relation.Binary using ( _Preserves₂_⟶_⟶_)
open import Relation.Binary.PropositionalEquality 
  using (subst ; sym ; refl)

open import Relation.Core
open import Relation.Base

private
  variable
    A B C D : Set
```

</details>

## Properties of union (`_+_`) 

```agda
+-cong : ∀ {A B : Set} 
   → _+_ Preserves₂ (_≈_ {A = A} {B}) ⟶ _≈_ ⟶ _≈_
+-cong f g = ⊎.map (×.proj₁ f) (×.proj₁ g) , ⊎.map (×.proj₂ f) (×.proj₂ g)

+-identityˡ : ∀ (x : A ∼ B) → zero + x ≈ x
+-identityˡ _ = (λ { (⊎.inj₂ x) → x} ) , ⊎.inj₂

+-identityʳ : ∀ (x : A ∼ B) → x + zero ≈ x
+-identityʳ _ = (λ { (⊎.inj₁ x) → x} ) , ⊎.inj₁

+-assoc : ∀ (x y z : A ∼ B) → x + y + z ≈ x + (y + z)
+-assoc _ _ _ = ⊎.assocʳ , ⊎.assocˡ

+-comm : ∀ (x y : A ∼ B) → x + y ≈ y + x
+-comm _ _ = ⊎.swap , ⊎.swap
```

## Properties of Intersection (`_*_`) 

```agda
*-cong : ∀ {A B : Set} 
   → _*_ Preserves₂ (_≈_ {A = A} {B}) ⟶ _≈_ ⟶ _≈_
*-cong f g = ×.map (×.proj₁ f) (×.proj₁ g) , ×.map (×.proj₂ f) (×.proj₂ g)

*-identityˡ : ∀ (x : A ∼ B) → one * x ≈ x
*-identityˡ _ = ×.proj₂ , (tt ,_)

*-identityʳ : ∀ (x : A ∼ B) → x * one ≈ x
*-identityʳ _ = ×.proj₁ , (_, tt)

*-assoc : ∀ (x y z : A ∼ B) → x * y * z ≈ x * (y * z)
*-assoc _ _ _ = ×.assocʳ , ×.assocˡ

*-comm : ∀ (x y : A ∼ B) → x * y ≈ y * x
*-comm _ _ = ×.swap , ×.swap

*-zeroˡ : ∀ (x : A ∼ B) → (_≈_ {A = A} {B}) (zero * x) zero
*-zeroˡ _ = ×.proj₁ , λ()

*-zeroʳ : ∀ (x : A ∼ B) → (_≈_ {A = A} {B}) (x * zero) zero
*-zeroʳ _ = ×.proj₂ , λ ()
```

## Properties of Composition (`_∘_`)

```agda
∘-identityˡ : ∀ (x : A ∼ B) → id ∘ x ≈ x
∘-identityˡ R {a} {b} = 
    (λ (b′ , aRb , b≡b) → subst (R a) b≡b aRb)
   , λ aRb → b , aRb , refl

∘-identityʳ : ∀ (x : A ∼ B) → x ∘ id ≈ x
∘-identityʳ R {a} {b} = 
    (λ (a' , a≡a' , a′Rb) → subst (flip R b) (sym a≡a') a′Rb) 
  , λ aRb → a , refl , aRb

∘-assoc : ∀ (x : C ∼ D) (y : B ∼ C) (z : A ∼ B) → (x ∘ y) ∘ z ≈ x ∘ (y ∘ z)
∘-assoc R₃ R₂ R₁ {a} {d} = 
   (λ (b , aR₁b , c , bR₂c , cR₃d ) → c , (b , aR₁b , bR₂c) , cR₃d) 
   , λ (c , (b , aR₁b , bR₂c), cR₃d ) → b , aR₁b , c , bR₂c , cR₃d

∘-cong : ∀ {A B C : Set} 
   → _∘_ Preserves₂ (_≈_ {A = B} {C}) ⟶ (_≈_ {A = A} {B}) ⟶ (_≈_ {A = A} {C})
∘-cong f g = 
   (λ (b , S₁ab , R₁bc) → b , ×.proj₁ g S₁ab , ×.proj₁ f R₁bc) 
  , λ (b , S₂ab , R₂bc) → b , ×.proj₂ g S₂ab , ×.proj₂ f R₂bc

∘-zeroˡ : ∀ (x : A ∼ B) → (_≈_ {A = A} {B}) (zero ∘ x) zero
∘-zeroˡ _ = (λ (_ , _ , x) → x) , λ ()

∘-zeroʳ : ∀ (x : A ∼ B) → (_≈_ {A = A} {B}) (x ∘ zero) zero
∘-zeroʳ _ = (λ (_ , x , _) → x) , λ ()
```

## Combined Properties of Union and Intersection

```agda
+-*-distribˡ : ∀ (x y z : A ∼ A) → x * (y + z) ≈ (x * y) + (x * z)
+-*-distribˡ _ _ _ = 
    (λ { (a , ⊎.inj₁ x ) → ⊎.inj₁ (a , x) 
       ; (a , ⊎.inj₂ x ) → ⊎.inj₂ (a , x) 
       }
    )
  , (λ { (⊎.inj₁ (a , x)) → a , ⊎.inj₁ x
       ; (⊎.inj₂ (a , x)) → a , ⊎.inj₂ x
       }
    )

+-*-distribʳ : ∀ (x y z : A ∼ A) → (y + z) * x ≈ (y * x) + (z * x)
+-*-distribʳ _ _ _ = 
    (λ { (⊎.inj₁ x , y) → ⊎.inj₁ (x , y) 
       ; (⊎.inj₂ x , y) → ⊎.inj₂ (x , y) 
       }
    )
 , (λ { (⊎.inj₁ (x , y)) → ⊎.inj₁ x , y
      ; (⊎.inj₂ (x , y)) → ⊎.inj₂ x , y
      }
   )
```

## Combined Properties of Union and Composition

```agda
+-∘-distribˡ : ∀ (x y z : A ∼ A) → (x ∘ (y + z)) ≈ ((x ∘ y) + (x ∘ z))
+-∘-distribˡ _ _ _ = 
    (λ { (a , ⊎.inj₁ x , y) → ⊎.inj₁ (a , x , y) 
       ; (a , ⊎.inj₂ x , y) → ⊎.inj₂ (a , x , y) }
    )
  , (λ { (⊎.inj₁ (a , x , y)) → a , ⊎.inj₁ x , y
       ; (⊎.inj₂ (a , x , y)) → a , ⊎.inj₂ x , y}
    )

+-∘-distribʳ : ∀ (x y z : A ∼ A) → (y + z) ∘ x ≈ (y ∘ x) + (z ∘ x)
+-∘-distribʳ _ _ _ = 
    (λ { (a , y , ⊎.inj₁ x ) → ⊎.inj₁ (a , y , x) 
       ; (a , y , ⊎.inj₂ x ) → ⊎.inj₂ (a , y , x) }
    )
 , (λ { (⊎.inj₁ (a , x , y)) → a , x , ⊎.inj₁ y
       ; (⊎.inj₂ (a , x , y)) → a , x , ⊎.inj₂ y}
    )
```
