# Style Guide

* Algebraic Bundles are denoted using mathematical *bold* script unicode.
* In the case that the carriers need to be distinguished
(as the case with modules)
superscript capital letters are used.
For example,
the `Carrierᴹ` for
the semimodules labelled as `𝓜ᴬ` and `𝓜ᴮ`
should be denoted `A` and `B` respectively.
* Levels of carriers are denoted with lowercase letters
corresponding to the letter used to denote the Carrier.
Levels of the equivalence relation should be `ℓ`
appended with the superscript of the carrier level.
For example,
the semimodule labelled `𝓜ᴬ` should have levels `a` and `ℓᵃ`.
* Certain upper-case letters are not representable as superscript unicode
(e.g. superscript `C`).
In such cases, use lower case.

## Commonly used symbols

```verbatim
𝓜 U+1D4DC \MCM
𝓡 U+1D4E1 \MCR
```
